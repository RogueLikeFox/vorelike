/*

Vorelike Copyright (c) 2013-2014 RogueLikeFox

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

var currentmodindex = 0;
var lastusedobjectindex = 0;

var menuarray = new Array();

//function copyobject(input)
//{
//	var newobj = new baseobject({ name: input.name, isbaseobject: false });

//	var newobjectindex = newobj.objectindex;

//	for(var property in input)
//	{
//		newobj[property] = input[property];
//	}

//	newobj.objectindex = newobjectindex;

//	return newobj;
//}

function checktags(disabletags, swallowmovetags)
{
	var tags = disabletags.split(",");
	for (var tag in tags)
	{
		if (swallowmovetags.indexOf(tag.trim()) >= 0)
		{
			return true;
			// bad!
		}
	}
	return false;
}

function getdataurifromfilename(filename)
{
		// need to work on this - 17/5/2014
		for (var key in FileSystemObj)
		{
			if (filename.indexOf(key) > -1)
			{
				return FileSystemObj[key];
			}
		}
		return false;
}

function getobjectfromind(index)
{
	var i = 0;
	
	for (i = 0; i < objects.length; i++)
	{
		if (objects[i].objectindex == index)
		{
			return objects[i];
		}
	}
	for (i = 0; i < actors.length; i++)
	{
		if (actors[i].swallowed == false)
		{
			if (actors[i].objectindex == index)
			{
				return actors[i];
			}
		}
	}
	return false;
}

function getobjectat(inputx, inputy)
{
	var i = 0;
	
	for (i = 0; i < objects.length; i++)
	{
		if (objects[i].x == inputx && objects[i].y == inputy)
		{
			return objects[i];
		}
	}
	for (i = 0; i < actors.length; i++)
	{
		if (actors[i].swallowed == false && actors[i] != getobjectreference("player"))
		{
			if (actors[i].x == inputx && actors[i].y == inputy)
			{
				return actors[i];
			}
		}
	}
	return false;
}

function getobjectreference(inputreferenceid)
{
	var i = 0;

	log("Searching for: " + inputreferenceid);

	// optimization here...
	
	if (inputreferenceid == "player")
	{
		for (i = 0; i < actors.length; i++)
		{
			if (actors[i].referenceid == inputreferenceid)
			{
				return actors[i];
			}
		}
	}
	
	// and on to the search
	
		for (i = 0; i < textures.length; i++)
		{
			if (textures[i].referenceid == inputreferenceid)
			{
				return textures[i];
			}
		}	
	
		for (i = 0; i < objects.length; i++)
		{
			if (objects[i].referenceid != null)
			{
				log(objects[i].referenceid);
			}
			if (objects[i].referenceid == inputreferenceid)
			{
				return objects[i];
			}
		}

		for (i = 0; i < actors.length; i++)
		{
			if (actors[i].referenceid == inputreferenceid)
			{
				return actors[i];
			}
		}

		for (i = 0; i < textscenes.length; i++)
		{
			if (textscenes[i].referenceid == inputreferenceid)
			{
				return textscenes[i];
			}
		}

		for (i = 0; i < choiceobjects.length; i++)
		{
			if (choiceobjects[i].referenceid == inputreferenceid)
			{
				return choiceobjects[i];
			}
		}

		for (i = 0; i < baseobjects.length; i++)
		{
			if (baseobjects[i].referenceid == inputreferenceid)
			{
				return baseobjects[i];
			}
		}

	return false;
}

function findrandomobjonfloor(input)
{
	var objects = new Array();
	
	if (allfloors[input.floornumber] == undefined || allfloors[input.floornumber].objects == undefined)
	{
		// bad...
		return false;
	}
	
	for(var i = 0; i < allfloors[input.floornumber].objects.length; i++)
	{
		if (allfloors[input.floornumber].objects[i].referenceid == input.referenceid)
		{
			objects.push(allfloors[input.floornumber].objects[i]);
		}
	}
	if (objects.length > 0)
	{
		return getrandomelement(objects);
	} else {
		return false;
	}
}

function soundarray(input)
{
	this.soundcategory = input.category.toString();

	if (sounds[this.soundcategory] == undefined)
	{
		sounds.push(this.soundcategory);
		sounds[this.soundcategory] = [];
	}

	var soundfiles = input.sounds;

	for (i = 0; i < soundfiles.length; i++)
	{
		sounds[this.soundcategory].push(soundfiles[i]);
	}
}

function basicobject(input)
{
	// basic object
	// it's always represented by a character on screen.

	this.mybaseobject = input.mybaseobject ? input.mybaseobject : "none";	
	
	if (this.mybaseobject != undefined && this.mybaseobject != "none" && baseobjects[this.mybaseobject] != undefined)
	{
		var i;
		for(i in baseobjects[this.mybaseobject])
		{
			this[i] = baseobjects[this.mybaseobject][i] ? baseobjects[this.mybaseobject][i] : false;
		}
	}

	this.mybaseobject = input.mybaseobject ? input.mybaseobject : "none";	
	
    this.x = 0;
    this.y = 0;
	this.layer = 1;

	var i;

	this.canactivate = true;
	this.powerlevel = 1; // like leveling

	for (i in input)
	{
		this[i] = input[i];
	}

	this.referenceid = input.referenceid;

	this.choices = input.choices ? input.choices : this.choices;
	this.outcomes = input.outcomes ? input.outcomes : this.outcomes;

	this.level = currentlevel;
    this.char = input.char ? input.char : this.char;
	this.color = input.color ? input.color : this.color;
    this.texture = input.texture ? input.texture : this.texture;
	this.name = input.name ? input.name : this.name;
	this.blocks = input.blocks;
	this.isbaseobject = input.isbaseobject;
	this.minfloor = input.minfloor ? input.minfloor : this.minfloor;
	this.maxfloor = input.maxfloor ? input.maxfloor : this.maxfloor;

	this.attributes = input.attributes ? input.attributes : this.attributes;
	// like hp+5, etc
	this.objecttype = input.objecttype ? input.objecttype : this.objecttype;
	// like "Armor", "Weapon", etc

	this.modindex = parseInt(currentmodindex.toString(),16);
	this.objectindex = parseInt((lastusedobjectindex + 1).toString(),16);
	
	if (this.objectindex == undefined)
	{
		alert(this.name + " has an undefined objectindex?");
	}

	this.render = true;
	this.deleted = false;

	lastusedobjectindex = lastusedobjectindex + 1;

	if (this.isbaseobject == true)
	{
		baseobjects.push(this);
	} else {
		this.isobject = true;
		objects.push(this);
	}
	return this;
}

basicobject.prototype.use = function(activator)
{
	if (this.deleted == true)
	{
		return;
	}
	
	if (activator.objectindex == getobjectreference("player").objectindex)
	{
		var attributes = this.attributes.replace(" ","");
	
		var effect = attributes.split("+");
		
		activator[effect[0]] += Number(effect[1]);
		
		try {
			if (activator[effect[0]] > activator[("total" + effect[0])])
			{
				activator[effect[0]] = activator[("total" + effect[0])];
			}
		}
		catch(err) {
			alert("Can't find a total" + effect[0] + " for effect " + effect[0]);
		}
	}

	var i;	

	for (i = 0; i < activator.backpack.length; i++)
	{
		if (activator.backpack[i].objectindex == this.objectindex)
		{
			activator.backpack.splice(i, 1);
		}
	}

	if (this.render == true)
	{
		this.removefromworld();
	}
};

basicobject.prototype.drop = function(activator)
{
	var i;	

	for (i = 0; i < activator.backpack.length; i++)
	{
		if (activator.backpack[i].objectindex == this.objectindex)
		{
			this.addtoworld(activator);

			activator.backpack.splice(i, 1);

			return true;
		}
	}
	return false;
};

basicobject.prototype.activate = function(activator)
{
	if (this.deleted == true)
	{
		return;
	}

	if (activator.objectindex == getobjectreference("player").objectindex)
	{
		getobjectreference("player").backpack.push(this);
	}
	this.removefromworld();
};

basicobject.prototype.addtoworld = function(activator)
{
	this.render = true;
	this.canactivate = true;

	if (activator)
	{
		this.x = activator.x;
		this.y = activator.y;
	} else {
		this.x = this.prev_x;
		this.y = this.prev_y;
	}

	allfloors[currentlevel].tiles[this.x][this.y].char = this.char;
	allfloors[currentlevel].tiles[this.x][this.y].blocked = this.blocks;
	renderall({handleactors: false});
};

basicobject.prototype.removefromworld = function()
{
	this.render = false;

	this.blocks = false;
	this.canactivate = false;

	allfloors[currentlevel].tiles[this.x][this.y].char = " ";
	allfloors[currentlevel].tiles[this.x][this.y].blocked = false;

	this.prev_x = this.x;
	this.prev_y = this.y;

	this.x = -1;
	this.y = -1;
};

function getimageprops(that, img)
{
	that.imagewidth = img.width;
	that.imageheight = img.height;
	if (that.clipwidth == 0 || that.clipheight == 0)
	{
		that.clipwidth = img.width;
		that.clipheight = img.height;
	}
}

function textureobject(input)
{
	var i;

	this.imagewidth = 0;
	this.imageheight = 0;
	
	this.istexture = true;
	
	this.referenceid = input.referenceid;
	
	var img = document.createElement("img");
	img.onload = function()
	{
		//alert(this.width + ", " + this.height);
		imagesrcs[this.src] = this;
	};
	img.src = input.texturefiles;
	
	var that = this;
	var imgobj = img;
	
	if (img.width == 0 || img.height == 0)
	{
		setInterval(function() { getimageprops(that, imgobj); }, 0.5);
	}
	
	this.imagewidth = img.width;
	this.imageheight = img.height;
	
	this.clipx = 0;
	this.clipy = 0;
	this.clipwidth = img.width;
	this.clipheight = img.height;
	
	this.canvasx = 0;
	this.canvasy = 0;
	this.canvaswidth = TILE_SIZE_GPHS;
	this.canvasheight = TILE_SIZE_GPHS;

	// image, clipx, clipy, clipwidth, clipheight, canvasx, canvasy, canvaswidth, canvasheight

	this.frameindex = 0;

	this.animtype = 0; // 0 = filmstrip, 1 = sprite sheet, 2 = image per frame

	this.isanimation = input.isanimation;

	this.spritesheetpoints = input.spritesheetpoints;

	this.name = input.name;
	this.texturefiles = input.texturefiles;
	this.filename = input.filename;

	this.timeprev = 0;
	this.timenow = 0;
	this.speed = input.speed;

	this.scale = input.scale ? input.scale : false;

	for (i in input)
	{
		this[i] = input[i];
	}
	
	if (this.referenceid.indexOf("ovrl") >= 0)
	{
		//alert("OVRL sprite: " + this.clipwidth + ", " + this.clipheight + ", " + this.imagewidth + ", " + this.imageheight + ", " + this.referenceid);
	}
	
	textures.push(this);	
}

textureobject.prototype.display = function()
{
	var imagename = this.nextframe();

	if (this.isanimation == true)
	{
		this.now = new Date().getTime();

		if ((this.now - this.timeprev) >= this.speed)
		{
			this.timeprev = new Date().getTime();
			this.frameindex = this.frameindex + 1;
		}
	}
	return imagename;
};

textureobject.prototype.nextframe = function()
{
	if (this.animtype == 0)
	{
		// filmstrip
		// so one file
		
		if (this.isanimation == true)
		{
			this.clipx = (Number(this.clipwidth) * this.frameindex);
		
			if ((this.clipx + this.clipwidth) > this.imagewidth)
			{
				this.clipx = 0;
				this.frameindex = 0;
			}
		}
		
		if (getdataurifromfilename(this.texturefiles) == false)
		{
			return this.texturefiles;
		} else {
			return getdataurifromfilename(this.texturefiles);
		}
	} else if (this.animtype == 1)
	{
		// sprite sheet, one file

		// this.clipx, this.clipy, this.clipwidth, this.clipheight

		if (this.frameindex > (this.spritesheetpoints.length-1) || this.isanimation == false)
		{
			this.frameindex = 0;
		}

		var info = this.spritesheetpoints[this.frameindex].split(",");
		
		this.clipx = Number(info[0].trim());
		this.clipy = Number(info[1].trim());
		this.clipwidth = Number(info[2].trim());
		this.clipheight = Number(info[3].trim());

		if (info.indexOf("NaN") > -1)
		{
			alert("Error with spritesheetpoints");
		}
		
		return this.texturefiles;
	} else if (this.animtype == 2)
	{
		// individual images

		if (isArray(this.texturefiles) == false)
		{
			return this.texturefiles;
		} else {
			if (this.frameindex <= (this.texturefiles.length-1))
			{
				return this.texturefiles[this.frameindex];
			} else {
				return this.texturefiles[0];
			}
		}
	}
	return false;
};

function choiceobject(input)
{
	var i;
	
	for (i in input)
	{
		this[i] = input[i];
	}
	
	this.name = input.name;
	this.ischoice = true;
	this.questiontext = input.questiontext?input.questiontext:"";
	this.referenceid = input.referenceid;
	this.choices = input.choices;
	this.outcomes = input.outcomes; 
	// outcomes = textscene names or other choices
	
	this.allowcancel = (input.allowcancel !== undefined)?input.allowcancel:true;
	
	this.currentchoice = this;
	this.chosenchoice = 0;
	this.chosenchoicefinal = -1;
	this.totalchoices = 0;
	this.choices;
	this.choicetext = "";
	this.choicecanvas;
	
	this.nextchoice = input.nextchoice ? input.nextchoice : "";
	
	this.talker = input.talker;

	if (input.istemp != false)
	{
		choiceobjects.push(this);
	}
}

choiceobject.prototype.displaychoice = function(talker)
{
	this.talker = talker;
	currentchoice = this;

	log("Displaychoice: " + talker.name);
	log("Displaychoice: " + currentchoice.talker.name + ", " + talker.name);

	if (isArray(this.choices))
	{
		this.textchoices = this.choices.join("|");
	} else {
		this.textchoices = this.choices;
	}
	
	choicebox(this);
};

function textscene(input)
{
	var i;

	for (i in input)
	{
		this[i] = input[i];
	}

	this.name = input.name;
	this.istextscene = true;
	this.referenceid = input.referenceid;
	this.sounds = input.sounds;
	if (input.scenes == undefined)
	{
		this.scenes = new Array();	
	} else {
		this.scenes = input.scenes;
	}
	this.scenenumber = 0;
	this.totalscenes = input.scenes.length;
	this.talker = input.talker;

	textscenes.push(this);
}

textscene.prototype.doscene = function(talker)
{
	this.talker = talker;
	currentscene = this;
	
	gamestatus = "textscene";

	this.scenenumber = 0;
	this.nextscene();
};

textscene.prototype.nextscene = function()
{
	if (this.scenenumber < this.totalscenes)
	{
		//log(this.scenes[this.scenenumber]);
		if (getobjectreference(this.scenes[this.scenenumber]).ischoice == true)
		{
			removenotification();
			getobjectreference(this.scenes[this.scenenumber]).displaychoice(this.talker);
		} else if (getobjectreference(this.scenes[this.scenenumber]).istextscene == true) {
			getobjectreference(this.scenes[this.scenenumber]).doscene(this.talker);
		} else {
			createnotification({text: this.scenes[this.scenenumber], currentscene: this});
		}
		this.scenenumber = this.scenenumber + 1;
			
		return this.scenenumber;
	} else {
		this.scenenumber = 0;
		currentscene = "";
		removenotification();
		gamestatus = "playing";
	}
};

function swallowmove(input)
{
	this.referenceid = input.referenceid;

	var i;

	for (i in input)
	{
		this[i] = input[i];
	}

	this.isswallowmove = true;
	
	this.name = input.name;
	this.sounds = input.sounds;
	this.finishsounds = input.finishsounds;
	this.hpthreshold = input.hpthreshold; // percentage of target health
	this.actors = input.actors;
	this.gender = input.gender;
	this.doesswallow = input.doesswallow;
	this.nextstage = input.nextstage;
	this.nextstagepercent = input.nextstagepercent;
	this.targetcanmove = input.targetcanmove;
	this.actorcanmove = input.actorcanmove;
	this.status = input.status;
	
	this.tags = input.tags ? input.tags : ""; // string
	
	this.successtext = input.successtext;
	this.succcesstextmale = input.successtextmale?nput.successtextmale:undefined;
	this.succcesstextfemale = input.successtextfemale?input.successtextfemale:undefined;
	this.succcesstextherm = input.successtextherm?input.successtextherm:undefined;	
	
	this.nextstagetext = input.nextstagetext;
	this.nextstagetextmale = input.nextstagetextmale?input.nextstagetextmale:undefined;	
	this.nextstagetextfemale = input.nextstagetextfemale?input.nextstagetextfemale:undefined;
	this.nextstagetextherm = input.nextstagetextherm?input.nextstagetextherm:undefined;	
	
	this.failtext = input.failtext;
	this.failtextmale = input.failtextmale?input.failtextmale:undefined;	
	this.failtextfemale = input.failtextfemale?input.failtextfemale:undefined;	
	this.failtextherm = input.failtextherm?input.failtextherm:undefined;	

	this.rubtext = input.rubtext;
	this.rubtextmale = input.rubtextmale?input.rubtextmale:undefined;
	this.rubtextfemale = input.rubtextfemale?input.rubtextfemale:undefined;
	this.rubtextherm = input.rubtextherm?input.rubtextherm:undefined;
	
	this.movetonextstageimd = input.movetonextstageimd;
	this.currentactor = input.currentactor;
	this.isstart = input.isstart;
	this.pleasurereleasetext = input.pleasurereleasetext;
	this.finishtext = input.finishtext;

	this.canescape = input.canescape ? input.canescape : true; // not sure how to use this yet in gameplay?
	
	for(var i = 0; i < baseobjects.length; i++)
	{
		if (this.actors.indexOf(baseobjects[i].referenceid) >= 0 || this.actors == baseobjects[i].referenceid)
		{
			baseobjects[i].swallowmoves.push(this);
		}
	}
	swallowmovelist.push(this);
}

swallowmove.prototype.getnextstage = function()
{
	if (this.nextstage == "release")
	{
		return "release";
	}
	for(var i = 0; i < swallowmovelist.length; i++)
	{
		if (this.nextstage == swallowmovelist[i].referenceid)
		{
			if (checktags(Disabletags, swallowmovelist[i].tags) == false)
			{
				return swallowmovelist[i];
			} else {
				return swallowmovelist[i].getnextstage();
			}
		}
	}
	return this;
};

swallowmove.prototype.getpreviousstage = function()
{
	if (this.prevstage == "release")
	{
		return;	
	}
	for(var i = 0; i < swallowmovelist.length; i++)
	{
		if (this.prevstage == swallowmovelist[i].referenceid)
		{
			return swallowmovelist[i];
		}
	}
	return this;
};

function chardescription(input)
{
	var i;

	this.ischardesc = true;
	
	for (i in input)
	{
		this[i] = input[i];
	}

	for(var i = 0; i < baseobjects.length; i++)
	{
		if (this.actors.indexOf(baseobjects[i].referenceid) >= 0 || this.actors == baseobjects[i].referenceid)
		{
			baseobjects[i].intros.push(this);
		}
	}
}

function character(input)
{
	this.mybaseobject = input.mybaseobject ? input.mybaseobject : "none";	
	
	if (this.mybaseobject != undefined && this.mybaseobject != "none" && baseobjects[this.mybaseobject] != undefined)
	{
		var i;
		for(i in baseobjects[this.mybaseobject])
		{
			this[i] = baseobjects[this.mybaseobject][i] ? baseobjects[this.mybaseobject][i] : false;
		}
	}

	this.mybaseobject = input.mybaseobject ? input.mybaseobject : "none";
	
	this.referenceid = input.referenceid;

	var i;	
	
	for (i in input)
	{
		this[i] = input[i];
	}

	this.experience = input.experience ? input.experience : 0;
	this.powerlevel = input.powerlevel ? input.powerlevel : 1; // like leveling
	this.level = currentlevel;
    this.char = input.char ? input.char : this.char;
	this.color = input.color ? input.color : this.char;
    this.texture = input.texture ? input.texture : this.texture;
	this.name = input.name ? input.name : this.name;
	this.isbaseobject = input.isbaseobject;
	this.minfloor = input.minfloor ? input.minfloor : this.minfloor;
	this.maxfloor = input.maxfloor ? input.maxfloor : this.maxfloor;
	this.gender = input.gender ? input.gender : this.gender;

	this.tilesizex = input.tilesizex ? input.tilesizex : this.tilesizex;
	if (this.tilesizex == undefined)
	{
		this.tilesizex = 1;
	}
	this.tilesizey = input.tilesizey ? input.tilesizey : this.tilesizey;
	if (this.tilesizey == undefined)
	{
		this.tilesizey = 1;
	}	

	this.talkscenes = input.talkscenes ? input.talkscenes : this.talkscenes; // name of textscene, could be array.

	this.x = 0;
	this.y = 0;
	this.layer = 1;	
	this.prev_x = 0;
	this.prev_y = 0;

	this.modindex = parseInt(currentmodindex.toString(),16);
	this.objectindex = parseInt((lastusedobjectindex + 1).toString(),16);

	this.prevobjectindex = this.objectindex;
	
	if (this.objectindex == undefined)
	{
		alert(this.name + " has an undefined objectindex?");
	}
	
	lastusedobjectindex = lastusedobjectindex + 1;

	this.isactor = true;
	this.backpack = input.backpack ? input.backpack : this.backpack;
	this.totalhp = input.totalhp ? input.totalhp : this.totalhp;
	this.hp = input.hp ? input.hp : this.hp;
	this.totalpleasure = input.totalpleasure ? input.totalpleasure : this.totalpleasure;
	this.pleasure = input.pleasure ? input.pleasure : this.pleasure;
	this.status = input.status ? input.status : this.status;
	this.blocks = true;
	this.target = "";
	this.actortype = input.actortype ? input.actortype : this.actortype;
	this.sightdistance = input.sightdistance ? input.sightdistance : this.sightdistance;	

	this.intros = new Array();

	this.dead = false;
	this.canmove = true;
	this.swallowed = false;
	this.trapped = false;
	this.swallower = "";
	this.currentattack = "";

	this.aggrotype = input.aggrotype ? input.aggrotype : this.aggrotype;
	// 0 = prey, 1 = predator, 2 = apex

	this.strength = input.strength ? input.strength : this.strength;
	this.dexterity = input.dexterity ? input.dexterity : this.dexterity;
	
	this.ai = input.ai ? input.ai : this.ai;

	this.showedintro = false;

	this.friends = new Array();
	this.contents = new Array();

	if (input.attacks == undefined)
	{
		this.attacks = new Array();	
	} else {
		this.attacks = input.attacks ? input.attacks : this.attacks;
	}

	if (input.swallowmoves == undefined)
	{
		this.swallowmoves = new Array();
	} else {
		this.swallowmoves = input.swallowmoves ? input.swallowmoves : this.swallowmoves;
	}

	if (input.isbaseobject == true)
	{
		baseobjects.push(this);
	} else {
		if (input.isactor == true)
		{
			actors.push(this);
		}
	}
	return this;
}

character.prototype.setanim = function(animname)
{
	var theanim = getobjectreference(animname)?getobjectreference(animname):getobjectreference(this.referenceid + "tex");
	if (theanim.referenceid != undefined)
	{
		if (this.texture != theanim.referenceid)
		{
			this.texture = theanim.referenceid;
		}
	} else {
		//printouttext("TextCanvas", false, "Setanim error: " + animname + ", " + this.texture);
		return;
	}
};

character.prototype.switchplaces = function(actor)
{
	this.lastcommand = "switchplaces";
	// switch places with player

	var currentx = this.x;
	var currenty = this.y;

	this.x = actor.x;
	this.y = actor.y;

	actor.x = currentx;
	actor.y = currenty;

	renderall({handleactors: false});
};

character.prototype.chaseactor = function(actor)
{
	this.lastcommand = "chaseactor";
	var tiles = pathFind(this.x, this.y, actor.x, actor.y);
	
	//alert(tiles[0].x + ", " + tiles[0].y);

	if (tiles.length <= 1)
	{
		this.move(0, 0);
		return;
	}

	var tile;

	var tilelength = 0;

	for (tilelength = (tiles.length-1); tilelength >= 0; tilelength--)
	{
		if (this.x != tiles[tilelength].x || this.y != tiles[tilelength].y)
		{
			break;
		}
	}

	tile = tiles[tilelength];

	if (tile == undefined)
	{
		var i;
		var returntext = "Error in chasing (number of returned tiles: " + tiles.length + "): ";
		for (i in tiles)
		{
			returntext += tiles[i].x + ", " + tiles[i].y + "\n";
		}
		alert(returntext);
		return;
	}

	if (actor.objectindex == getobjectreference("player").objectindex)
	{
		//alert("Pathing from " + this.name + " to " + actor.name + " gives: " + (tile.x) + ", " + (tile.y) + ", current: " + this.x + ", " + this.y);
		//alert((this.x - tile.x) + ", " + (tile.y - this.y));
	}
	this.lastcommand = "chaseactor";
	if (this.move(tile.x - this.x, tile.y - this.y) == false)
	{
		// stuck?
		actor.target = "";
	}
};

character.prototype.openinventory = function(opener)
{
	this.lastcommand = "openinventory";
	gamestatus = "inventory";

	if (opener.objectindex == this.objectindex)
	{
		// player is opening their inventory...
		// so we display what is in their backpack.

		if (this.backpack.length <= 0 && this.objectindex == getobjectreference("player").objectindex)
		{
			createnotification({text: "Your inventory is empty."});
			return;
		}

		var i;
		var j;

		var objinv = new Array();
		var objoutcomes = new Array();
		var objindx = new Array();

		var numtypes = 1;

		for (i in this.backpack)
		{	
			if (objinv.length == 0)
			{
				objinv.push("1 " + this.backpack[i].name);
				objoutcomes.push("useitem");
			} else {		
				for (j in objinv)
				{
					if (objinv[j].indexOf(this.backpack[i].name) != -1)
					{
						numtypes = numtypes + 1;
						objinv[j] = (numtypes) + " " + this.backpack[i].name;
						objoutcomes[j] = "useitem";
					} else {
						objinv.push("1 " + this.backpack[i].name);
						objoutcomes.push("useitem");
					}
				}
			}
			objindx.push(this.backpack[i]);
		}

		var choiceinv = new choiceobject( { name: "InvChoice", referenceid: -1, choices: objinv, outcomes: objoutcomes, objindx: objindx } );

		currentchoice = choiceinv;
		
		choiceinv.displaychoice(getobjectreference("player"));
	} else {
		
	}
};

character.prototype.dropobject = function(object)
{
	this.lastcommand = "dropobject";
	var i;

	for (i in this.backpack)
	{
		if (object.objectindex == this.backpack[i].objectindex)
		{
			this.backpack.splice(i,1);
			return true;
		}
	}
	return false;
};

character.prototype.playertalk = function()
{
	this.lastcommand = "playertalk";
	// playe wants to talk to the actor

	var scene = "";

	scene = this.talkscenes;

	if (isArray(this.talkscenes))
	{
		scene = getrandomelement(this.talkscenes);
	}

	if (getobjectreference(scene).istextscene == true)
	{
		getobjectreference(scene).doscene(this);
	} else if (getobjectreference(scene).ischoice == true) {
		getobjectreference(scene).displaychoice(this);
	} else {
		createnotification({text: "The " + this.gender.toLowerCase() + " " + this.name .toLowerCase() + " has nothing to say to you."});
	}
};

character.prototype.canseetarget = function(target)
{
	
};

character.prototype.levelup = function()
{
	this.experience = this.experience + (1/this.powerlevel);

	var powerprev = this.powerlevel;

	while (this.experience >= 1)
	{
		this.powerlevel = this.powerlevel + 1;
		this.experience = this.experience - 1;
	}

	if (this.powerlevel > powerprev)
	{
		this.experience = 0;

		this.totalhp = Math.round(this.totalhp + (this.powerlevel));
		this.totalpleasure = Math.round(this.totalpleasure + (this.powerlevel));
		this.strength = Math.round(this.strength + (this.powerlevel));

		//this.hp = this.totalhp;
		//this.pleasure = 0;
		//printouttext("TextCanvas", false, this.name + " (" + this.objectindex + ") is now level " + this.powerlevel + ".");	
	}
};

character.prototype.attack = function(target)
{
	this.lastcommand = "attack";
	// need to decide if try to swallow or attack

	if (this.ismyfriend(target) == true && this.objectindex != getobjectreference("player").objectindex && target.objectindex != getobjectreference("player").objectindex)
	{
		return;
	}
	
	var random;
	
	if (target.hp <= target.totalhp || target.objectindex != getobjectreference("player").objectindex)
	{	
		random = Math.round(Math.random() * 100);
	} else {
		random = 50;
	}
	
	if (this.intros.length > 0 && target.objectindex == getobjectreference("player").objectindex && this.showedintro == false)
	{
		this.showedintro = true;
		printouttext("TextCanvas", false, getrandomelement(this.intros));
	}

	if (random < 40 && target.swallowed == false && target.trapped == false && this.swallowed == false && this.trapped == false && this.swallowmoves.length > 0)
	{
		// try a swallow
		this.swallow(target, undefined);
	} else {

		if (random >= 50 && (this.swallowed == true || this.trapped == true) && this.objectindex != getobjectreference("player").objectindex)
		{
			var pleasure = this.strength;
			target.pleasure = target.pleasure + pleasure;
			return;
		}

		if ((target.objectindex == getobjectreference("player").objectindex || this.objectindex == getobjectreference("player").objectindex) && target.swallower != undefined && target.swallower.objectindex == this.objectindex && (target.swallowed == true || target.trapped == true))
		{	
			var damage = this.currentswallowmove.basedamange ? this.currentswallowmove.basedamange : this.strength;
			damage = this.currentswallowmove.dicesize ? damage + Math.round(Math.random() * this.currentswallowmove.dicesize) : damage;
			
			if (this.ismyfriend(target) == false && (this.currentswallowmove.canescape == undefined || this.currentswallowmove.canescape == true))
			{
				target.hp = target.hp - damage;
			} else {
				damage = 0;
			}		
			
			if (this.ismyfriend(target) == true && this.objectindex == getobjectreference("player").objectindex)
			{
				log("Attack: " + this.name + " is friends with " + target.name);
			}

			if (this.currentswallowmove == undefined)
			{
				alert(this.name + " has an undefined swallowmove?");
			}
			
			playsound(getrandomsoundfromcategory(this.currentswallowmove.sounds));
			
			var changedtext = "";
			
			if (getobjectreference("player").gender == "male" && this.currentswallowmove.successtextmale != undefined)
			{
				changedtext = replacetext( { intext: getrandomelement(this.currentswallowmove.successtextmale), damage: damage, gender: this.gender, creaturename: this.name, hp: this.hp, totalhp: this.totalhp } );										
			} else if (getobjectreference("player").gender == "female" && this.currentswallowmove.successtextfemale != undefined) {
				changedtext = replacetext( { intext: getrandomelement(this.currentswallowmove.successtextfemale), damage: damage, gender: this.gender, creaturename: this.name, hp: this.hp, totalhp: this.totalhp } );			
			} else if (getobjectreference("player").gender == "herm" && this.currentswallowmove.successtextherm != undefined) {
				changedtext = replacetext( { intext: getrandomelement(this.currentswallowmove.successtextherm), damage: damage, gender: this.gender, creaturename: this.name, hp: this.hp, totalhp: this.totalhp } );			
			} else {
				changedtext = replacetext( { intext: getrandomelement(this.currentswallowmove.successtext), damage: damage, gender: this.gender, creaturename: this.name, hp: this.hp, totalhp: this.totalhp } );			
			}			
			
			if (changedtext == "")
			{
				var alertoutput = "";
				for(text in this.currentswallowmove.successtext)
				{
					alertoutput += (text + ": " + this.currentswallowmove.successtext[text] + "\n");
				}
				alert(alertoutput);
			}
			printouttext("TextCanvas", false, changedtext);

			var randnum = Math.round((Math.random() * 100));
			
			if ((randnum <= this.currentswallowmove.nextstagepercent) || (this.currentswallowmove.hpthreshold >= ((target.hp / target.totalhp) * 100)) || this.currentswallowmove.movetonextstageimd == true)
			{
				if (this.currentswallowmove.getnextstage() == "release")
				{
					this.release(target);
					return;
				}

				if (this.currentswallowmove.getnextstage().referenceid != this.currentswallowmove.referenceid)
				{
					if (this.currentswallowmove.nextstagetext.length > 0)
					{
						if (getobjectreference("player").gender == "male" && this.currentswallowmove.nextstagetextmale != undefined)
						{
							printouttext("TextCanvas", false, getrandomelement(this.currentswallowmove.nextstagetextmale));								
						} else if (getobjectreference("player").gender == "female" && this.currentswallowmove.nextstagetextfemale != undefined) {
							printouttext("TextCanvas", false, getrandomelement(this.currentswallowmove.nextstagetextfemale));					
						} else if (getobjectreference("player").gender == "herm" && this.currentswallowmove.nextstagetextherm != undefined) {
							printouttext("TextCanvas", false, getrandomelement(this.currentswallowmove.nextstagetextherm));						
						} else {
							printouttext("TextCanvas", false, getrandomelement(this.currentswallowmove.nextstagetext));			
						}
					}
				}
			
				var curactor = this.currentswallowmove.currentactor;
				this.currentswallowmove = this.currentswallowmove.getnextstage();
				this.currentswallowmove.currentactor = curactor;
				
				if (target.swallowed == false && this.currentswallowmove.doesswallow == true)
				{
					target.swallower = this;
					this.contents.push(target);
					target.prev_x = target.x;
					target.prev_y = target.y;
					this.setanim(this.currentswallowmove.referenceid + "_swallow");
					playsound(getrandomsoundfromcategory(this.currentswallowmove.sounds));
				}
				
				target.canmove = this.currentswallowmove.targetcanmove;
				this.canmove = this.currentswallowmove.actorcanmove;
				target.swallowed = this.currentswallowmove.doesswallow;
				target.canescape = this.currentswallowmove.canescape ? this.currentswallowmove.canescape : true;
			}
		} else if (target.objectindex == getobjectreference("player").objectindex && target.swallowed == false && this.swallowed == false && this.trapped == false) {
			var number = Math.round(Math.random() * (this.attacks.length-1));

			this.currentattack = this.attacks[number];			
			
			var damage = this.currentattack.basedamange ? this.currentattack.basedamange : this.strength;
			damage = this.currentattack.dicesize ? damage + Math.round(Math.random() * this.currentattack.dicesize) : damage;

			if (this.ismyfriend(target) == false)
			{	
				target.hp = target.hp - damage;
			} else {
				damage = 0;
			}			
			
			//log(this.name + ", Attack #" + number + ", " + this.attacks.length);

			playsound(getrandomsoundfromcategory(this.currentattack.sounds));

			printouttext("TextCanvas", false, replacetext( { intext: getrandomelement(this.currentattack.successtext), damage: damage, gender: this.gender, creaturename: this.name, hp: this.hp, totalhp: this.totalhp } ) );
		}

		if (this.objectindex == getobjectreference("player").objectindex)
		{
			if (target.swallowed == true || target.trapped == true)
			{
				if (target.swallower == getobjectreference("player"))
				{
					var damage = this.currentswallowmove.basedamange ? this.currentswallowmove.basedamange : this.strength;
					damage = this.currentswallowmove.dicesize ? damage + Math.round(Math.random() * this.currentswallowmove.dicesize) : damage;	
					
					if (this.ismyfriend(target) == false)
					{	
						target.hp = target.hp - damage;
					} else {
						damage = 0;
					}							
				}
			} else {
				var number = Math.round(Math.random() * (this.attacks.length-1));

				
				this.currentattack = this.attacks[number] ? this.attacks[number] : undefined;
				
				var damage = 0;
				
				if (this.currentattack == undefined)
				{
					damage = this.strength;
				} else {
					damage = this.currentattack.basedamange ? this.currentattack.basedamange : this.strength;
					damage = this.currentattack.dicesize ? damage + Math.round(Math.random() * this.currentattack.dicesize) : damage;					
				}
					
				if (this.ismyfriend(target) == false)
				{	
					target.hp = target.hp - damage;
				} else {
					damage = 0;
				}											
			}
		}
		
		//log(this.name + " is attacking " + target.name);

		if (target.hp <= 0)
		{
			if (this.currentswallowmove != undefined && this.currentswallowmove.finishsounds != undefined)
			{
				playsound(getrandomsoundfromcategory(this.currentswallowmove.finishsounds));
			}
			for (var i = 0; i < target.contents.length; i++)
			{
				target.release(target.contents[i]);
			}
			if (this.swallower.objectindex == target.objectindex)
			{
				target.release(this);
			}
			target.die(this);
			this.levelup();
		}
	}
};

character.prototype.rest = function()
{
	this.lastcommand = "rest";
	if (this.hp < this.totalhp)
	{
		this.hp = this.hp + 1;
	}
};

character.prototype.die = function(killer)
{	
	this.lastcommand = "die";
	allfloors[currentlevel].tiles[this.x][this.y].char = " ";
	// allfloors[currentlevel].tiles[this.x][this.y].prevchar;
	allfloors[currentlevel].tiles[this.x][this.y].blocked = false;
	allfloors[currentlevel].tiles[this.prev_x][this.prev_y].blocked = false;	

	this.dead = true;
	this.status = "Dead";
	this.hp = 0;
	//log(this.name + " is dead");

	this.target = "";
	killer.target = "";

	//printouttext("TextCanvas", false, this.name + " (" + this.objectindex + ", " + this.isbaseobject + ") was defeated by " + killer.name + " (" + killer.objectindex + ", " + this.isbaseobject + ")");
	
	if (this.objectindex != getobjectreference("player").objectindex && this.swallower.objectindex == getobjectreference("player").objectindex)
	{
		// player digested prey
		try {
		
			if (killer.currentswallowmove.finishtext != undefined && killer.currentswallowmove.finishtext.length > 0)
			{
				printouttext("TextCanvas", false, getrandomelement(killer.currentswallowmove.finishtext));
			} else {
				printouttext("TextCanvas", false, "You have digested your prey.");
			}
		
		}
		catch(err) {
			alert("Error: " + err);
		}
	} else if (this.objectindex == getobjectreference("player").objectindex) {
		if (getobjectreference("player").swallowed == true || getobjectreference("player").trapped == true)
		{
			// enemy digested player...
			if (killer.currentswallowmove.finishtext != undefined && killer.currentswallowmove.finishtext.length > 0)
			{
				printouttext("TextCanvas", false, getrandomelement(killer.currentswallowmove.finishtext));	
			} else {
				printouttext("TextCanvas", false, "You have been slain.");
			}
		} else {
			// enemy just killed player...
			if (killer.currentattack.finishtext != undefined && killer.currentattack.finishtext.length > 0)
			{
				printouttext("TextCanvas", false, getrandomelement(killer.currentattack.finishtext));	
			} else {
				printouttext("TextCanvas", false, "You have been slain.");
			}		
		}
	}
	
	if (this.objectindex == getobjectreference("player").objectindex)
	{
		printouttext("TextCanvas", false, "Press the Enter key to restart the game.");
	}
	
	killer.release(this);
};
 
character.prototype.move = function(dx, dy)
{
	this.lastcommand = "move("+dx + ", " + dy + ")";
	
	var dx = dx;
	var dy = dy;

	if (dx == undefined)
	{
		dx = 0;
	}
	if (dy == undefined)
	{
		dy = 0;
	}
	
	if (this.level != currentlevel)
	{
		if (this.objectindex == getobjectreference("player").objectindex)
		{
			alert("The player is not on the current level: " + getobjectreference("player").level);
		}
		return;
	}

	if (this.target != "" && this.objectindex != getobjectreference("player").objectindex && this.target != undefined)
	{
		if (get_distance(this, this.target).distance <= 1 && get_distance(this, this.target).isdiag == false && this.target.dead == false)
		{
			//  || this.target.swallower.objectindex == this.objectindex
			this.attack(this.target);
			return true;					
		}
	}

	if (this.contents.length > 0 || this.canmove == false || this.trapped == true || this.swallowed == true)
	{
		if (this.objectindex == getobjectreference("player").objectindex)	
		{
			if (this.prev_x == 0 && this.prev_y == 0)
			{
				alert("Player cannot move?");
			}
		}
		return true;
		// creatures who have swallowed or are trapped by something can't move.
	}	
	
	if (this.objectindex == getobjectreference("player").objectindex)
	{
		for(var i = 0; i < allfloors[currentlevel].actors.length; i++)
		{
			if (allfloors[currentlevel].actors[i].level != currentlevel || allfloors[currentlevel].actors[i].swallowed == true)
			{
				continue;
			}
			if (allfloors[currentlevel].actors[i].x == (this.x + dx) && allfloors[currentlevel].actors[i].y == (this.y + dy) && allfloors[currentlevel].actors[i].objectindex != getobjectreference("player").objectindex)
			{
				if (allfloors[currentlevel].actors[i].dead == false)
				{
					// player is trying to move towards an enemy!
					if (this.ismyfriend(allfloors[currentlevel].actors[i]) == false)
					{
						this.attack(allfloors[currentlevel].actors[i]);
					} else {
						// but its a friend!
						allfloors[currentlevel].actors[i].playertalk();
					}
					return true;
				}
			}
		}
	}
	
	var newx = Math.round(this.x + dx);
	var newy = Math.round(this.y + dy);
	
	// now we check
	
	if (this.tilesizex > 1 || this.tilesizey > 1)
	{
		for (var i = 0; i < this.tilesizex; i++)
		{
			for (var j = 0; j < this.tilesizey; j++)
			{
				if (allfloors[currentlevel].tiles[newx + i][newy + j].blocked == false && allfloors[currentlevel].tiles[newx + i][newy + j].walkable == true)
				{
					
				} else {
					return false;
				}
			}
		}
	} else {
		if (allfloors[currentlevel].tiles[newx] != undefined && allfloors[currentlevel].tiles[newx][newy] != undefined && allfloors[currentlevel].tiles[newx][newy].blocked == false && allfloors[currentlevel].tiles[newx][newy].walkable == true)
		{

		} else {
			return false;
		}	
	}
	
	this.setanim(this.referenceid + "_move");

	allfloors[currentlevel].tiles[this.x][this.y].char = " ";
	// allfloors[currentlevel].tiles[this.x][this.y].prevchar;
	allfloors[currentlevel].tiles[this.x][this.y].blocked = false;

	this.prev_x = this.x;
	this.prev_y = this.y;

	this.x += dx;
	this.y += dy;
	allfloors[currentlevel].tiles[this.x][this.y].blocked = true;
		
	if (this.referenceid == getobjectreference("player").referenceid)
	{
		discover_tiles(this.x, this.y, 5);
	}
		
	return true;
};

character.prototype.clear = function()
{
	allfloors[currentlevel].tiles[this.x][this.y].char = allfloors[currentlevel].tiles[this.x][this.y].prevchar;

	var screenoffx = screenx * TILE_SIZE;
	var screenoffy = screeny * TILE_SIZE;

	var c = document.getElementById("MapCanvas");
	var ctx = c.getContext("2d");
	ctx.font = "24px Arial";
	ctx.fillText(this.char,(WIDTH_OFF + this.x*TILE_SIZE)-screenoffx,(HEIGHT_OFF + this.y*TILE_SIZE)-screenoffy);
};

character.prototype.addfriend = function(character)
{
	if (character.objectindex == getobjectreference("player").objectindex)
	{
		this.color = getobjectreference("player").color;
	}
	log("Making " + this.name + " (" + this.objectindex +") a friend of " + character.name + " (" + character.objectindex + ")");
	this.friends.push(character);
	this.target = "";
	character.friends.push(this);
	character.target = "";
};

character.prototype.release = function(actor)
{
	this.lastcommand = "release";
	if (this.swallowed == true && this.swallower != undefined && actor.dead == false)
	{
		// the creature that swallowed actor was also swallowed
		actor.swallower = this.swallower;
		actor.swallower.contents.push(actor);
		this.contents.splice(actor);
	} else {
		actor.blocks = true;

		if (actor.prev_x != undefined && actor.prev_y != undefined)
		{
			actor.x = actor.prev_x;
			actor.y = actor.prev_y;
		}

		actor.canmove = true;
		this.canmove = true;
		actor.swallowed = false;
		actor.trapped = false;

		if (actor.swallowed == false && actor.trapped == false && actor.dead == false)
		{
			actor.status = "Normal";
		} else {
			actor.status = this.status;
		}

		actor.swallower = "";
		this.contents.splice(actor);
		this.pleasure = 0;
		this.target = "";
		actor.target = "";
		if (this.contents.length <= 0)
		{
			this.setanim(this.referenceid + "tex");
		}
	}
	renderall({handleactors: false});
	//playsound("release.ogg");	
};

character.prototype.pleasureactor = function(target)
{
	this.lastcommand = "pleasureactor";
	var pleasure = this.strength;
	target.pleasure = target.pleasure + pleasure;

	if (this.swallowed == true && this.objectindex == getobjectreference("player").objectindex && target.currentswallowmove.rubtext.length > 0)
	{
		// player was swallowed and is trying to pleasure the predator
		this.setanim(target.currentswallowmove.referenceid + "_rub");
		if (target.currentswallowmove.rubtext != "" && target.currentswallowmove.rubtext != undefined)
		{
			if (getobjectreference("player").gender == "male" && target.currentswallowmove.rubtextmale != undefined)
			{
				printouttext("TextCanvas", false, replacetext( { intext: getrandomelement(target.currentswallowmove.rubtextmale), damage: pleasure, gender: this.gender, creaturename: this.name, hp: this.hp, totalhp: this.totalhp } ) );		
			} else if (getobjectreference("player").gender == "female" && target.currentswallowmove.nextstagetextfemale != undefined) {
				printouttext("TextCanvas", false, replacetext( { intext: getrandomelement(target.currentswallowmove.rubtextfemale), damage: pleasure, gender: this.gender, creaturename: this.name, hp: this.hp, totalhp: this.totalhp } ) );						
			} else if (getobjectreference("player").gender == "herm" && target.currentswallowmove.nextstagetextherm != undefined) {
				printouttext("TextCanvas", false, replacetext( { intext: getrandomelement(target.currentswallowmove.rubtextherm), damage: pleasure, gender: this.gender, creaturename: this.name, hp: this.hp, totalhp: this.totalhp } ) );											
			} else {
				printouttext("TextCanvas", false, replacetext( { intext: getrandomelement(target.currentswallowmove.rubtext), damage: pleasure, gender: this.gender, creaturename: this.name, hp: this.hp, totalhp: this.totalhp } ) );		
			}		
		}
	}

	if (target.pleasure > target.totalpleasure)
	{
		if (this.swallowed == true && this.objectindex == getobjectreference("player").objectindex)
		{
			if (target.currentswallowmove.pleasurereleasetext != "" && target.currentswallowmove.pleasurereleasetext != undefined)
			{
				printouttext("TextCanvas", false, replacetext({intext: getrandomelement(target.currentswallowmove.pleasurereleasetext), damage: pleasure, gender: this.gender, creaturename: this.name, hp: this.hp, totalhp: this.totalhp } ) );		
			}
		}
		if (target.ismyfriend(this) == false)
		{
			this.setanim(target.currentswallowmove.referenceid + "_pleasure");
			target.addfriend(this);
			target.release(this);
		} else {
			target.pleasure = 0;
			// don't release unless player hits spacebar.
		}
	}
};

character.prototype.swallow = function(actor, specificmove)
{
	this.lastcommand = "swallow";
	// need a formula here to determine if 
	// successful.

	var specificmove = specificmove?specificmove:undefined;
	
	// actor is actor being swallowed

	var successful = true;

	var swallowmove = this.swallowmoves[0];

	if (this.swallowmoves.length > 0 && specificmove == undefined)
	{
		var random = Math.round(Math.random() * (this.swallowmoves.length-1));

		swallowmove = this.swallowmoves[random];
		
		var tries = 0;
		while (swallowmove.isstart == false && tries <= 20)
		{
			random = Math.round(Math.random() * (this.swallowmoves.length-1));
			swallowmove = this.swallowmoves[random];
			tries = tries + 1;
		}
		if (tries >= 21 || swallowmove.isstart == false)
		{
			for(var i = 0; i < this.swallowmoves.length; i++)
			{
				if (this.swallowmoves[i].isstart == true)
				{
					swallowmove = this.swallowmoves[i];
				}
			}
		}
		//alert(swallowmove.name + ", " + swallowmove.isstart);
	} else if (this.swallowmoves.length > 0 && specificmove != undefined)
	{
		for(var i = 0; i < this.swallowmoves.length; i++)
		{
			if (this.swallowmoves[i].isstart == true && this.swallowmoves[i].referenceid == specificmove)
			{
				swallowmove = this.swallowmoves[i];
			}
		}		
	}

	if (successful)
	{
		actor.blocks = false;	

		allfloors[currentlevel].tiles[actor.x][actor.y].blocked = false;
		allfloors[currentlevel].tiles[actor.x][actor.y].char = " ";

		actor.trapped = true;
		actor.prev_x = actor.x;
		actor.prev_y = actor.y;
		actor.canmove = swallowmove.targetcanmove;
		this.canmove = swallowmove.actorcanmove;
		actor.swallowed = swallowmove.doesswallow;
		actor.canescape = swallowmove.canescape ? swallowmove.canescape : true;
		swallowmove.currentactor = this;
		actor.swallower = this;
		this.currentswallowmove = swallowmove;
		this.currentswallowmove.currentactor = this;

		actor.status = swallowmove.status;
		this.contents.push(actor);
		//this.x = actor.x;
		//this.y = actor.y;
		if (actor.objectindex == getobjectreference("player").objectindex)
		{
			this.setanim(this.currentswallowmove.referenceid + "_swallow");
			playsound(getrandomsoundfromcategory(this.currentswallowmove.sounds));
			
			if (getobjectreference("player").gender == "male" && this.currentswallowmove.successtextmale != undefined)
			{
				printouttext("TextCanvas", false, getrandomelement(this.currentswallowmove.successtextmale));								
			} else if (getobjectreference("player").gender == "female" && this.currentswallowmove.successtextfemale != undefined) {
				printouttext("TextCanvas", false, getrandomelement(this.currentswallowmove.successtextfemale));					
			} else if (getobjectreference("player").gender == "herm" && this.currentswallowmove.successtextherm != undefined) {
				printouttext("TextCanvas", false, getrandomelement(this.currentswallowmove.successtextherm));						
			} else {
				printouttext("TextCanvas", false, getrandomelement(this.currentswallowmove.successtext));			
			}
			
			if (this.currentswallowmove.movetonextstageimd == true)
			{
				if (this.currentswallowmove.nextstagetext != "")
				{
					if (getobjectreference("player").gender == "male" && this.currentswallowmove.nextstagetextmale != undefined)
					{
						printouttext("TextCanvas", false, getrandomelement(this.currentswallowmove.nextstagetextmale));								
					} else if (getobjectreference("player").gender == "female" && this.currentswallowmove.nextstagetextfemale != undefined) {
						printouttext("TextCanvas", false, getrandomelement(this.currentswallowmove.nextstagetextfemale));					
					} else if (getobjectreference("player").gender == "herm" && this.currentswallowmove.nextstagetextherm != undefined) {
						printouttext("TextCanvas", false, getrandomelement(this.currentswallowmove.nextstagetextherm));						
					} else {
						printouttext("TextCanvas", false, getrandomelement(this.currentswallowmove.nextstagetext));			
					}
				}
				this.currentswallowmove = swallowmove.getnextstage();
				this.currentswallowmove.actor = this;
			}
			clearmap();
		} else if (this.objectindex == getobjectreference("player").objectindex) {
			this.setanim(this.currentswallowmove.referenceid + "_swallow");
			playsound(getrandomsoundfromcategory(this.currentswallowmove.sounds));
			
			if (getobjectreference("player").gender == "male" && this.currentswallowmove.successtextmale != undefined)
			{
				printouttext("TextCanvas", false, getrandomelement(this.currentswallowmove.successtextmale));								
			} else if (getobjectreference("player").gender == "female" && this.currentswallowmove.successtextfemale != undefined) {
				printouttext("TextCanvas", false, getrandomelement(this.currentswallowmove.successtextfemale));					
			} else if (getobjectreference("player").gender == "herm" && this.currentswallowmove.successtextherm != undefined) {
				printouttext("TextCanvas", false, getrandomelement(this.currentswallowmove.successtextherm));						
			} else {
				printouttext("TextCanvas", false, getrandomelement(this.currentswallowmove.successtext));			
			}
			
			if (this.currentswallowmove.movetonextstageimd == true)
			{
				if (this.currentswallowmove.nextstagetext != "")
				{
					if (getobjectreference("player").gender == "male" && this.currentswallowmove.nextstagetextmale != undefined)
					{
						printouttext("TextCanvas", false, getrandomelement(this.currentswallowmove.nextstagetextmale));								
					} else if (getobjectreference("player").gender == "female" && this.currentswallowmove.nextstagetextfemale != undefined) {
						printouttext("TextCanvas", false, getrandomelement(this.currentswallowmove.nextstagetextfemale));					
					} else if (getobjectreference("player").gender == "herm" && this.currentswallowmove.nextstagetextherm != undefined) {
						printouttext("TextCanvas", false, getrandomelement(this.currentswallowmove.nextstagetextherm));						
					} else {
						printouttext("TextCanvas", false, getrandomelement(this.currentswallowmove.nextstagetext));			
					}
				}
				this.currentswallowmove = swallowmove.getnextstage();
				this.currentswallowmove.actor = this;
			}
			clearmap();		
		}
	} else {
		if (getobjectreference("player").gender == "male" && this.currentswallowmove.failtextmale != undefined)
		{
			printouttext("TextCanvas", false, getrandomelement(this.currentswallowmove.failtextmale));								
		} else if (getobjectreference("player").gender == "female" && this.currentswallowmove.failtextfemale != undefined) {
			printouttext("TextCanvas", false, getrandomelement(this.currentswallowmove.failtextfemale));					
		} else if (getobjectreference("player").gender == "herm" && this.currentswallowmove.failtextherm != undefined) {
			printouttext("TextCanvas", false, getrandomelement(this.currentswallowmove.failtextherm));						
		} else {
			printouttext("TextCanvas", false, getrandomelement(swallowmove.failtext));
		}	
	}
};

character.prototype.ismyfriend = function(actor)
{
	if (this.friends.indexOf(actor) != -1)
	{
		return true;
	} else {
		return false;
	}
	return false;
};

function get_distance(actor, otheractor)
{
	var target_x = otheractor.x;
	var target_y = otheractor.y;

	var target_tilesizex = otheractor.tilesizex?otheractor.tilesizex:1;
	var target_tilesizey = otheractor.tilesizey?otheractor.tilesizey:1;
	
	var actor_tilesizex = actor.tilesizex?actor.tilesizex:1;
	var actor_tilesizey = actor.tilesizey?actor.tilesizey:1;	
	
	var dx = (target_x + (target_tilesizex/2)) - (actor.x + (actor_tilesizex/2));
	var dy = (target_y + (target_tilesizey/2)) - (actor.y + (actor_tilesizey/2));
	
	var distance = Math.round(Math.sqrt( (dx * dx) + (dy * dy) ));
	var realdistance = Math.sqrt( (dx * dx) + (dy * dy) );

	dx = Math.round(dx / distance);
	var realx = (dx / distance);
	dy = Math.round(dy / distance);
	var realy = (dx / distance);

	var diagonal = true;

	if (dx == 0 || dy == 0)
	{
		diagonal = false;
	}

	var position = {distance: distance, x: dx, y: dy, realx: realx, realy: realy, realdistance: realdistance, isdiag: diagonal};
	
	return position;
}

function ai_target_search(searcher)
{
	var foundtargets = new Array();
	for(var i = 0; i < allfloors[currentlevel].actors.length; i++)
	{
		if (searcher.race == actors[i].race)
		{
			continue;
		}
		if (get_distance(searcher, actors[i]).distance <= searcher.sightdistance && searcher.objectindex != actors[i].objectindex)
		{	
			if (searcher.objectindex != actors[i].objectindex && searcher.level == actors[i].level && (actors[i].swallowed == false || actors[i].swallower == searcher))
			{
				// got a target!
				if (actors[i].objectindex == getobjectreference("player").objectindex)
				{
					//log(searcher.name + " (" + searcher.objectindex + ") found player!");
				}
				if (actors[i].dead == false && searcher.ismyfriend(actors[i]) == false)
				{
					foundtargets.push(actors[i]);
				}
				if (actors[i].dead == false && searcher.ismyfriend(actors[i]) == true && actors[i].swallower.referenceid == searcher.referenceid && (actors[i].trapped == true || actors[i].swallowed == true))
				{
					foundtargets.push(actors[i]);
				}
			}
		}	
	}
	if (foundtargets.length > 0)
	{
		//log("Targets found for (" + searcher.objectindex + "): " + foundtargets.length);
	}
	return foundtargets;
}

function handle_ai(actor)
{	
	var doalert = false;
	if (getobjectreference("player").swallowed == true && getobjectreference("player").swallower == actor)
	{
		doalert = true;
	}

	if (actor.level != currentlevel)
	{
		return;
	}
	
	var i = 0;
	
	var targets = ai_target_search(actor);

	if ((actor.swallowed == true || actor.trapped == true) && actor.swallower.objectindex != undefined)
	{
		if (actor.objectindex != getobjectreference("player").objectindex)
		{
			// not the player
			var random = Math.random();
			if (random > 0.25)
			{
				actor.pleasureactor(actor.swallower);
			} else {
				actor.attack(actor.swallower);
			}
		}
	}

	if (actor.target.objectindex != undefined)
	{
		if (actor.target.dead == true)
		{
			actor.target = "";
			return;
		}

		// actor has a target already.
		if (get_distance(actor, actor.target).distance > actor.sightdistance)
		{
			actor.target = "";
			return;	
		}
		if (actor.aggrotype >= actor.target.aggrotype)
		{
			// attack
			this.lastcommand = "handleai_chaseactor";
			//alert(actor.name + " is chasing: " + actor.target.name);
			if (actor.chaseactor(actor.target) == false)
			{

			}

			//actor.move(get_distance(actor, actor.target).x, get_distance(actor, actor.target).y);		

			if (actor.target.objectindex == player.objectindex)
			{
				//log(actor.name + " chasing player, x: " + get_distance(actor, actor.target).realx + ", y: " + get_distance(actor, actor.target).realy + ", distance: " + get_distance(actor, actor.target).realdistance);
			}
		} else {
			// run away!
			this.lastcommand = "handleai_move";
			actor.move(get_distance(actor, actor.target).x*-1, get_distance(actor, actor.target).y*-1);		
		}
	} else if (targets.length > 0) {
		for (var i = 0; i < targets.length; i++)
		{
			if (actor.aggrotype >= targets[i].aggrotype)
			{
				// attack!
				actor.target = targets[i];
				actor.lastcommand = "handleai_chaseactor";
				actor.chaseactor(actor.target);
				//actor.move(get_distance(actor, actor.target).x, get_distance(actor, actor.target).y);		
			} else {
				// run away!
				actor.target = targets[i];
				actor.lastcommand = "handleai_chaseactor_runaway";
				actor.chaseactor(actor.target);
				//actor.move(get_distance(actor, actor.target).x*-1, get_distance(actor, actor.target).y*-1);		
			}
			break;
		}
	} else {
		// no targets

		var moved = false;

		if (actor.ismyfriend(getobjectreference("player")) == true)
		{
			if (get_distance(actor, getobjectreference("player")).distance <= 2 && getobjectreference("player").swallowed == false)
			{
				return;
			}
		}

		if (actor.hp < actor.totalhp)
		{
			actor.rest();
			return;
		}

		var tries = 0;

		while(moved == false && tries <= 10)
		{
			var choice = Math.random() * 100;
			this.lastcommand = "randommove";
			if (choice <= 25)
			{
				moved = actor.move(1,0);
			} else if (choice <= 50)
			{
				moved = actor.move(-1,0);
			} else if (choice <= 75)
			{
				moved = actor.move(0,1);
			} else if (choice > 75)
			{
				moved = actor.move(0,-1);
			}
			tries = tries + 1;
		}
	}
}

function handle_actors()
{
	// handles actor movement and states.
	
	if (gamestatus != "playing")
	{
		return;
	}
	
	for (var i = 0; i < allfloors[currentlevel].actors.length; i++)
	{
		if (allfloors[currentlevel].actors[i].objectindex == undefined || allfloors[currentlevel].actors[i] == undefined)
		{
			alert(allfloors[currentlevel].actors[i].name + " has an undefined objectindex (was " + allfloors[currentlevel].actors[i].prevobjectindex + ")");
		}
		if (allfloors[currentlevel].actors[i].objectindex != getobjectreference("player").objectindex && allfloors[currentlevel].actors[i].dead == false && allfloors[currentlevel].actors[i].level == currentlevel)
		{
			handle_ai(allfloors[currentlevel].actors[i]);
		}
	}
}

function addobjectat(input)
{
	var myobject = input.object;
	var x = input.x;
	var y = input.y;
}

function populatecurrentfloor()
{
	log("Populating " + currentlevel + " floor...");
	//log("Number of rooms: " + allfloors[currentlevel].rooms.length);
	log("Number of baseobjects: " + baseobjects.length);

	var newobject;
	var tempobj;

	var i = 1;
	var j = 0;

	allfloors[currentlevel].objects = new Array();
	allfloors[currentlevel].actors = new Array();

	getobjectreference("player").x = 0;
	getobjectreference("player").y = 0;
	
	allfloors[currentlevel].actors.push(getobjectreference("player"));
	
	while (allfloors[currentlevel].tiles[getobjectreference("player").x][getobjectreference("player").y] == undefined || allfloors[currentlevel].tiles[getobjectreference("player").x][getobjectreference("player").y].blocked == true)
	{
		getobjectreference("player").x = allfloors[currentlevel].size * Math.random();
		getobjectreference("player").y = allfloors[currentlevel].size * Math.random();
		getobjectreference("player").x = Math.round(getobjectreference("player").x);
		getobjectreference("player").y = Math.round(getobjectreference("player").y);
	}	
	
	discover_tiles(getobjectreference("player").x, getobjectreference("player").y, 5);

	if (allfloors[currentlevel] == undefined || allfloors[currentlevel].rooms == undefined || baseobjects.length == 0)
	{
		return;
	}

	if (currentlevel >= 1)
	{
		tempobj = getobjectreference("stairsup");

		if (tempobj.isbaseobject == true)
		{
			newobject = new basicobject(tempobj);
		} else {
			newobject = new basicobject(tempobj.mybaseobject);
		}

		newobject.x = allfloors[currentlevel].rooms[0].x1 + Math.round(allfloors[currentlevel].rooms[0].w * Math.random());
		newobject.y = allfloors[currentlevel].rooms[0].y1 + Math.round(allfloors[currentlevel].rooms[0].h * Math.random());
		newobject.x = Math.round(newobject.x);
		newobject.y = Math.round(newobject.y);

		while (allfloors[currentlevel].tiles[newobject.x][newobject.y] == undefined || allfloors[currentlevel].tiles[newobject.x][newobject.y].blocked == true)
		{
			newobject.x = allfloors[currentlevel].rooms[0].x1 + Math.round(allfloors[currentlevel].rooms[0].w * Math.random());
			newobject.y = allfloors[currentlevel].rooms[0].y1 + Math.round(allfloors[currentlevel].rooms[0].h * Math.random());
			newobject.x = Math.round(newobject.x);
			newobject.y = Math.round(newobject.y);
		}
		log("Placing up stairs...");
		allfloors[currentlevel].objects.push(newobject);		
	}

	var rounds = 0;

	for(rounds = 0; rounds < 3; rounds++)
	{
		for(i = 1; i < allfloors[currentlevel].rooms.length; i++)
		{

			if (baseobjects.length > 0) 
			{
				j = Math.random() * (baseobjects.length - 1);
				j = Math.round(j);

				if (Math.random() * 100 > 90)
				{
					while (baseobjects[j].referenceid != "stairsdown")
					{
						j = Math.random() * (baseobjects.length - 1);
						j = Math.round(j);					
					}
				}
			} else {
				return;
			}

			if (baseobjects[j].referenceid != getobjectreference("player").referenceid)
			{
				if (currentlevel >= baseobjects[j].minfloor && currentlevel <= baseobjects[j].maxfloor)
				{
					tempobj = baseobjects[j];
	
					tempobj.isbaseobject = false;
					tempobj.mybaseobject = baseobjects[j];
	
					if (baseobjects[j].isactor == true)
					{
						newobject = new character( tempobj );
					} else {
						newobject = new basicobject( tempobj );
					}
	
					if (newobject.objectindex == undefined)
					{
						alert(newobject.name + " has an undefined objectindex");
					}
	
					newobject.x = allfloors[currentlevel].rooms[i].x1 + Math.round(allfloors[currentlevel].rooms[i].w * Math.random());
					newobject.y = allfloors[currentlevel].rooms[i].y1 + Math.round(allfloors[currentlevel].rooms[i].h * Math.random());
					newobject.x = Math.round(newobject.x);
					newobject.y = Math.round(newobject.y);
	
					while (allfloors[currentlevel].tiles[newobject.x][newobject.y] == undefined || allfloors[currentlevel].tiles[newobject.x][newobject.y].blocked == true)
					{
						newobject.x = allfloors[currentlevel].rooms[i].x1 + Math.round(allfloors[currentlevel].rooms[i].w * Math.random());
						newobject.y = allfloors[currentlevel].rooms[i].y1 + Math.round(allfloors[currentlevel].rooms[i].h * Math.random());
						newobject.x = Math.round(newobject.x);
						newobject.y = Math.round(newobject.y);
					}
					
					if (baseobjects[j].isactor == true)
					{
						allfloors[currentlevel].actors.push(newobject);
					} else {
						allfloors[currentlevel].objects.push(newobject);
					}					
				}
			}
		}
	}
}
