/*

Vorelike Copyright (c) 2013-2014 RogueLikeFox

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

function createchoices()
{
	var genderchoice = "";

	var scatchoice = "";
		
	//fileparser({text: genderchoice});
	//fileparser({text: scatchoice});

}

function fileparser(thefile)
{
	var lines = "";
	if (this.responseText != undefined)
	{
		lines = this.responseText;
	} else {
		lines = this.result;
	}
	
	if (thefile.text != undefined)
	{
		alert(thefile.text);
		lines = thefile.text;
	}
	
	if (lines.indexOf("END") >= 0 && lines.indexOf("NEXTSTAGE") >= 0 && lines.indexOf("#") >= 0)
	{
		// OVRL file. So we parse it differently.
		ovrl_fileparser(lines);
		return;
	}
	
	//log(this.responseText);
	
	lines = lines.split("\n");	

	for(var i = 0; i < lines.length;)
	{	
		var output = new Array();
		var j = i;

		for (var j = i; j < lines.length; j++)
		{
			var line = lines[j];
			var trimmedLine = lines[j].trim();
			
			if (trimmedLine.indexOf("//") == 0)
			{
				continue;
			}
			
			// NEW: If a line is indented, it is considered a
			// continuation of the previous line.
			//
			if (j != i && (line[0] != " ") && (line[0] != "\t"))
			{
				break;
			}
			
			if (trimmedLine.indexOf(";base64") > -1)
			{
				trimmedLine = trimmedLine.replace(";base64", "!base64");
				trimmedLine = trimmedLine.replace("data:", "data!");
				var args = trimmedLine.split(";");
			} else {
				var args = trimmedLine.split(";");
			}
			
			var maxArg = args.length - 1;

			for(var k = 0; k <= maxArg; k++)
			{
				var command = args[k].trim();			
				
				if ((command == "") || (command.substring(0,2) == "//"))
				{
					continue;
				}
				
				command = command.split(":");
				
				if (args[k].indexOf("base64") > -1)
				{
					command[1] = command[1].replace("!base64", ";base64");		
					command[1] = command[1].replace("data!", "data:");				
				}								
				
				if (command[0] == undefined || command[1] == undefined)
				{
					alert("There is an error in mod file: " + this.filename + " at line: " + i);
				}

				var key = command[0].trim();
				var value = command[1].trim();
				
				//log("Command: key=\"" + key + "\", value=\"" + value + "\"");
				
				if (value == undefined)
				{
					alert("There is an error in mod file: " + this.filename + " at line: " + i);
				}

				if (value == "true" || value == "false")
				{
					var val = (value == "true");
					//log("Setting \"" + key + "\" to boolean value \"" + val.toString() + "\""); 
					output[key] = (value == "true");
				} 
				else if (isFinite(value) == true) 
				{
					var val = parseFloat(value);
					//log("Setting \"" + key + "\" to float value \"" + val.toString() + "\"");
					output[key] = parseFloat(value);
				}
				else if (value.indexOf("[") == 0) 
				{					
					// array
					output[key] = new Array();
					
					// NEW: Arrays can also be split by newline, and semicolons must be ignored.
					//
					var arrayInLine = value.substring(1).trim();
					var arraytext = "";
					
					while (true)
					{
						var index = arrayInLine.indexOf("]");
						if (index < 0)
						{
							arraytext += arrayInLine;
							if (k < maxArg)
							{
								k++;
								arraytext += "; ";
								arrayInLine = args[k].trim();
								continue;
							}
							else
							{
								j++;
								arraytext += "|";
								arrayInLine = lines[j].trim();
								continue;
							}
						} else {
							arraytext += arrayInLine.substring(0, index);
							break;
						}
					}
					if (arraytext == "")
					{
						
					} else {
						//log("Setting \"" + key + "\" to Array \"" + arraytext + "\"");				
						arraytext = arraytext.split("|");
						for(text in arraytext)
						{
							var item = arraytext[text].trim();
							if ((item != undefined) && (item != ""))
							{							
								//log(key + ": " + item);	
								output[key].push(item);
							}
						}
					}
				} 
				else
				{
					//log("Setting \"" + key + "\" to string \"" + value + "\"");
					output[key] = value.replace("\n","");
				}
			}
		}

		i = j;
		
		if (output.isactor == true)
		{
			if (output.name == "Player")
			{	
				defaultplayer = new character( output );

				var player = getobjectreference("player");
				player.isbaseobject = false;
				defaultplayer.isbaseobject = false;
				player.mybaseobject = getobjectreference("player");
				defaultplayer.mybaseobject = getobjectreference("player");				
				player.level = currentlevel;
				defaultplayer.level = currentlevel;

				player = new character( player );
			} else {
				var newcharacter = new character( output );
			}
		} else if (output.addattack == true) {
			// need to find the base...
			for(var k = 0; k < baseobjects.length; k++)
			{
				if( Object.prototype.toString.call( output.actors ) === '[object Array]' )
				{
					if (output.actors.indexOf(baseobjects[k].referenceid) >= 0)
					{
						if (baseobjects[k].attacks == undefined)
						{
							baseobjects[k].attacks = new Array();
						}
						baseobjects[k].attacks.push(output);
					}
				} else {
					if (baseobjects[k].referenceid == output.actors)
					{
						// got 'em
						if (baseobjects[k].attacks == undefined)
						{
							baseobjects[k].attacks = new Array();
						}
						baseobjects[k].attacks.push(output);
					}
				}
			}
		} else if (output.addswallow == true) {
			// need to find the base...
			new swallowmove(output);
		} else if (output.issound == true) {
			new soundarray( output );
		} else if (output.addchoice == true) {
			new choiceobject( output );
		} else if (output.addtextscene == true) {
			new textscene( output );
		} else if (output.isactor == false) {
			new basicobject( output );
		} else if (output.israndomseed == true) {
			// random seed using Math.seedrandom()
			customseed = output.randomseed;
			if (customseed != false && customseed != undefined)
			{
				gamerandomseed = Math.seedrandom(output.randomseed);
			} else {
				gamerandomseed = Math.seedrandom(new Date().getTime());
			}
		} else if (output.nextfile == true) {
			if (this.responseText == undefined)
			{
				//alert("Loading file: " + output.file);
				loadXMLfiles(output.file);
			} else {
				log("Loading file \"" + output.file + "\"");
				//alert("Loading file: " + output.file);
				loadXMLfiles(output.file);
			}
		} else if (output.addtexture == true) {
			new textureobject( output );
		} else if (output.addintro == true) {
			new chardescription( output );
		} else if (output.tmxmap != undefined && output.tmxmap.length > 0)
		{
			loadTMX(output.tmxmap);
		}
	}
}

function ovrl_fileparser(filetext)
{
	var lines = "";
	lines = filetext;
	
	// supports V3 and V4 of OVRL files
	
	//log(this.responseText);
	
	var textinfo = lines.split("lexicon");

	// textinfo[0] is stuff like: art, artamount, etc
	// textinfo[1] is vore text

	lines = textinfo[0].split("\n");

	var commands = new Array();
	
	var output = new Array();	
	
	var morecommand = 0;
	var lastart = 0;
	
	for(var i = 0; i < lines.length; i++)
	{	
		var trimmedLine = lines[i].trim();
		var command = "";
		command = trimmedLine.split(" ");
		if (commands[command[0]] == undefined)
		{
			commands[command[0]] = command;
			morecommand = 0;
		} else {
			morecommand = morecommand + 1;
			commands[(command[0] + morecommand)] = command;
			lastart = morecommand;
		}
	}

	// art...
	output.addtexture = true;
	output.name = "ovrl" + commands["name"][1] + "tex";
	output.referenceid = "ovrl" + commands["name"][1] + "tex";
	output.isanimation = false;
	output.animtype = 0; // filmstrip
	output.scale = true;
	output.texturefiles = "./" + commands["art"][1];
	output.clipx = 0;
	output.clipy = 0;
	if (commands["art1"] == undefined)
	{
		// V4 uses a filmstrip method
		output.clipwidth = commands["art"][2];
		output.clipheight = 32;
	} else {
		output.animtype = 2;
		// no need here, as V3 uses entire file size
	}
	
	new textureobject( output );
	
	try {
				
		var thetexture = getobjectreference(output.referenceid);
		var oneimg;
		
		if (thetexture.referenceid != undefined)
		{
			if (imagesrcs[thetexture.texturefiles] == undefined)
			{
				oneimg = new Image();
				oneimg.src = thetexture.display();
				imagesrcs[thetexture.texturefiles] = oneimg;
			} else {
				thetexture.display();
				oneimg = imagesrcs[thetexture.texturefiles];
			}
		} else {
			alert("Unable to load image: " + output.name);		
		}
		oneimg = null;
		thetexture = null;		
	}
	catch(err) {
		alert(err);
	}	
	
	output = new Array();
	
	// character
	output.char = commands["name"][1].substring(0,1);
	output.color = "#670000";
	output.texture = "ovrl" + commands["name"][1] + "tex";
	output.name = commands["name"][1];
	output.gender = (commands["gender"])?commands["gender"][1]:"";
	output.race = commands["name"][1];
	output.isactor = true;
	output.isbaseobject = true;
	output.referenceid = "ovrl" + commands["name"][1];
	output.minfloor = (commands["minfloor"])?commands["minfloor"][1]:5 + Math.round(Math.random() * 5);
	output.maxfloor = (commands["maxfloor"])?commands["maxfloor"][1]:999;
	output.hp = (commands["hp"])?commands["hp"][1]:Math.round((Math.random() * 5)+5);
	output.totalhp = output.hp;
	output.backpack = new Array();
	output.status = "Normal";
	output.actortype = "Predator";
	output.aggrotype = 2;
	output.sightdistance = 5;
	output.strength = commands["strength"][1];
	output.dexterity = commands["dexterity"][1];
	output.constitution = commands["endurance"][1];
	output.intelligence = (commands["intelligence"])?commands["intelligence"][1]:1;
	output.wisdom = (commands["wisdom"])?commands["wisdtom"][1]:1;
	output.charisma = (commands["charisma"])?commands["charisma"][1]:1;
	output.totalpleasure = (commands["totalpleasure"])?commands["totalpleasure"][1]:1;
	output.pleasure = 0;
	output.talkscenes = "ovrl" + commands["name"][1] + "text";
	
	new character( output );
	
	// dealing with the Lexicon stuff now.
	
	// textinfo[1] is all the Lexicon stuff
	var lexicon = textinfo[1].split("END");
	
	// lexicon[0] is attacks
	// others are vore attacks
	
	// attacks	
	
	var attackinfo = lexicon[0].split("NEXTSTAGE");
	
	// attackinfo[0] and attackinfo[1] seem to be attacks
	// attackinfo[2] seems to be strength related
	// attackinfo[3] is dodging
	
	var successinfo = new Array(); 
	successinfo = successinfo.concat(attackinfo[0].replace(/(\r\n|\n|\r)/gm,"").split("#"), attackinfo[1].replace(/(\r\n|\n|\r)/gm,"").split("#"));
	
	var textoutput = "";
	
			for(var n = 0; n < successinfo.length; n++)
			{
				if (successinfo[n].length <= 0)
				{
					// bad
					successinfo.splice(n,1);
				} else {
					textoutput += successinfo[n] + "\n";
				}
			}
	
	var failinfo = new Array();
	failinfo = attackinfo[3].replace(/(\r\n|\n|\r)/gm,"").split("#");
	
	textoutput = "";
	
			for(var n = 0; n < failinfo.length; n++)
			{
				if (failinfo[n].length <= 0)
				{
					// bad
					failinfo.splice(n,1);
				} else {
					textoutput += failinfo[n] + "\n";
				}
			}
	
	output = new Array();
	
	output.addattack = true;
	output.name = commands["name"][1];
	output.gender = "";
	output.actors = "ovrl" + commands["name"][1];
	output.successtext = successinfo;
	output.failtext = failinfo;
	output.finishtext = "";
	
			for(var k = 0; k < baseobjects.length; k++)
			{
				if( Object.prototype.toString.call( output.actors ) === '[object Array]' )
				{
					if (output.actors.indexOf(baseobjects[k].referenceid) >= 0)
					{
						if (baseobjects[k].attacks == undefined)
						{
							baseobjects[k].attacks = new Array();
						}
						baseobjects[k].attacks.push(output);
					}
				} else {
					if (baseobjects[k].referenceid == output.actors)
					{
						// got 'em
						if (baseobjects[k].attacks == undefined)
						{
							baseobjects[k].attacks = new Array();
						}
						baseobjects[k].attacks.push(output);
					}
				}
			}	
	
	// swallowmoves
	// dealing with the Lexicon stuff now.
	
	var swallowinfo = new Array();
	
	for(var l = 1; l < lexicon.length; l++)
	{
		swallowinfo = lexicon[l].split("NEXTSTAGE");
		
		// Each swallowinfo set is made up by lines and # seperated by \n, so we need to convert each
		// into different stages, with each stagepercent being 50ish.
		
		if (lexicon[l].length <= 0)
		{
			continue;
		}
		
		for(var m = 0; m < swallowinfo.length; m++)
		{
			output = new Array();		
			
			output.addswallow = true;
			output.referenceid = "ovrl" + commands["name"][1] + "vore" + l + "stage" + m;
			if (m == 0)
			{
				output.isstart = true;
			} else {
				output.isstart = false;
			}
			output.actors = "ovrl" + commands["name"][1];
			output.gender = "";
			if (m == 0)
			{
				output.sounds = "swallow";
			} else {
				output.sounds = "belly";
			}
			if ((m+1) < swallowinfo.length)
			{
				output.nextstage = "ovrl" + commands["name"][1] + "vore" + l + "stage" + (m+1);
			} else {
				output.nextstage = "none";
			}
			output.nextstagepercent = 100;
			if (m == 0)
			{
				output.movetonextstageimd = true;		
			} else {
				output.movetonextstageimd = false;
			}
			output.doesswallow = true;
			output.targetcanmove = false;
			output.actorcanmove = false;
			output.status = "Vored";
			output.successtext = new Array();
			
			if ((m+1) >= swallowinfo.length)
			{
				if ((m-1) >= 0)
				{
					output.successtext = swallowinfo[(m-1)].replace(/(\r\n|\n|\r)/gm,"").split("#");
				} else {
					output.successtext = swallowinfo[0].replace(/(\r\n|\n|\r)/gm,"").split("#");				
				}
				output.finishtext = swallowinfo[(swallowinfo.length-1)].replace(/(\r\n|\n|\r)/gm,"").split("#");
			} else {
				output.successtext = swallowinfo[m].replace(/(\r\n|\n|\r)/gm,"").split("#");
				output.finishtext = swallowinfo[(swallowinfo.length-1)].replace(/(\r\n|\n|\r)/gm,"").split("#");
			}			
			
			output.rubtext = "";
			output.nextstagetext = "";
			output.prevstagetext = "";
			
			textoutput = "";
		
			for(var n = 0; n < output.successtext.length; n++)
			{
				//alert(output.successtext[n].length);
				if (output.successtext[n].length <= 0)
				{
					// bad
					output.successtext.splice(n,1);
				} else {
					textoutput += output.successtext[n];
				}
			}
		
			for(var n = 0; n < output.finishtext.length; n++)
			{
				//alert(output.finishtext[n].length);
				if (output.finishtext[n].length <= 0)
				{
					// bad
					output.finishtext.splice(n,1);
				} else {
					textoutput += output.finishtext[n];
				}
			}
		
			if (output.successtext.length > 0 && Object.prototype.toString.call( output.successtext ) === '[object Array]')
			{
				new swallowmove(output);

				if (commands["art1"] == undefined)
				{
					// vore art...
					output.addtexture = true;
					output.name = output.referenceid + "_swallow";
					output.referenceid = output.referenceid + "_swallow";
					output.isanimation = false;
					output.animtype = 0; // filmstrip
					output.scale = true;
					output.texturefiles = commands["art"][1];
					output.clipx = Number(commands["art"][2] * (m+1));
					output.clipy = 0;
					output.clipwidth = "./" + commands["art"][2];
					output.clipheight = 32;
				} else {
					// vore art...
					output.addtexture = true;
					output.name = output.referenceid + "_swallow";
					output.referenceid = output.referenceid + "_swallow";
					output.isanimation = false;
					output.animtype = 2; // whole file
					output.scale = true;
					output.texturefiles = (commands[("art" + (m+1))])?"./" + (commands[("art" + (m+1))][1]):"./" + (commands[("art" + (lastart))][1]);
					output.clipx = 0;
					output.clipy = 0;
					//output.clipwidth = commands[("art" + (m))][2];
					//output.clipheight = 32;
				}
	
				new textureobject( output );

				try {
				
					var oneimg;
					var thetexture = getobjectreference(output.referenceid);

					if (thetexture.referenceid != undefined)
					{
						if (imagesrcs[thetexture.texturefiles] == undefined)
						{
							oneimg = new Image();
							oneimg.src = thetexture.display();
							imagesrcs[thetexture.texturefiles] = oneimg;
						} else {
							thetexture.display();
							oneimg = imagesrcs[thetexture.texturefiles];
						}
					} else {
						alert("Unable to load image: " + output.name);
					}
					oneimg = null;
					thetexture = null;
				}
				catch(err) {
					alert(err);
				}
				
			}
		}
	}
		
}

function loadXMLfiles(filename)
{
	var oReq = new XMLHttpRequest();
	oReq.filename = filename;
	oReq.onload = fileparser;
	oReq.crossOrigin="*";
	oReq.open("GET", filename, false);
	//oReq.setRequestHeader("Content-Type", "text/plain");
	try {
		oReq.send();
	}
	catch(err) {
		alert(err);
	}
}

function versioncheck(url)
{
	return;
}

function handleFileSelect(evt) 
{
	evt.stopPropagation();
	evt.preventDefault();

	var files = evt.dataTransfer.files; // FileList object.

	var output = "Loading files: \n";
	for (var i = 0, f; f = files[i]; i++)
	{	
		output += f.name + "\n";	
		var reader = new FileReader();
		customfiles = true;
		reader.onload = fileparser;
		reader.readAsText(f);
    }
	alert(output);
}

function handleDragOver(evt) 
{
	evt.stopPropagation();
	evt.preventDefault();
	evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
}

function setSaveFile(contents, file_name, mime_type) 
{
	var a;
	if (document.getElementById("savefile") == null)
	{
		a = document.createElement('a');
	} else {
		a = document.getElementById('savefile');
	}
	a.id = "savefile";
	
	mime_type = mime_type || 'application/octet-stream'; // text/html, image/png, et c
	if (file_name) 
	{
		a.setAttribute('download', file_name);
	}
	a.href = 'data:'+ mime_type +';base64,'+ btoa(contents || '');
}
