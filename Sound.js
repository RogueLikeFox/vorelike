/*

Vorelike Copyright (c) 2013-2014 RogueLikeFox

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/


var internalsoundarray = new Array();

function getrandomsoundfromcategory(category)
{
	if (sounds[category] == undefined || category == undefined)
	{
		return false;
	}

	var number = Math.round(Math.random() * ((sounds[category].length)-1) );
	var sound = sounds[category][number];

	return sound;
}

function playsound(sound)
{
	if (sound == false || playgamesounds == false || sound == undefined)
	{
		return;
	}

	try {
		var soundefx01 = new Audio();
	}
	catch(err) {
		alert("Sound: " + err);
		return;
	}

	if (internalsoundarray[sound] == undefined)
	{
		if (sound.substring)
		{
			soundefx01.src = sound;
			soundefx01.load();
			internalsoundarray[sound] = soundefx01;
		} else {
			var i = 0;
			while (i < sound.length)
			{
				soundefx01.src = sound[i];
				soundefx01.load();
				internalsoundarray[sound[i]] = soundefx01;
			}
		}
	}

	if (sound.substring)
	{
		// quack.
		try {
			internalsoundarray[sound].play();
		}
		catch(err) {
			alert("Sound: " + err);
		}
	} else {
		// array?
		var i = 0;
		while(i < sound.length)
		{
			internalsoundarray[sound[i]].play();
			internalsoundarray[sound[i]].addEventListener('ended', function() {
				this.currentTime = 0; i = i + 1;
			}, false);
		}
	}
}

function playsoundloop(sound)
{
	// mostly used for music type things.

	if (sound == false || playgamesounds == false)
	{
		return;
	}
	
	try {
		var soundefx01 = new Audio();
	}
	catch(err) {
		alert("Sound: " + err);
		return;
	}	
	
	if (internalsoundarray[sound] == undefined)
	{
		if (sound.substring)
		{
			soundefx01.src = sound;
			soundefx01.load();
			internalsoundarray[sound] = soundefx01;
		} else {
			var i = 0;
			while (i < sound.length)
			{
				soundefx01.src = sound[i];
				soundefx01.load();
				internalsoundarray[sound[i]] = soundefx01;
			}
		}
	}
	
	if (internalsoundarray[sound].addedlistener == undefined)
	{
		internalsoundarray[sound].addEventListener('ended', function() {
			playsoundloop(this.src);
		}, false);
		internalsoundarray[sound].addedlistener = true;		
	}
	
	internalsoundarray[sound].play();
}

function playrandomsound(sound)
{
	if (sound == false || playgamesounds == false)
	{
		return;
	}

	if (sound.substring)
	{
		// just one sound? Ok.
		playsound(sound);
	} else {
		var randomint = Math.floor(Math.random() * 100) - (100 - sound.length);
		while (randomint >= sound.length)
		{
			randomint = Math.floor(Math.random() * 100) - (100 - sound.length);
		}
		playsound(sound[randomint]);
	}
}