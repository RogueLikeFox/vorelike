/*

Vorelike Copyright (c) 2013-2014 RogueLikeFox

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

// global vars used to adjust the algorithm
// not necessary for real use

var allowDiagonals, diagonalCost, dotTiebreaker;

allowDiagonals = false;
diagonalCost = 999;
dotTiebreaker = true; // ?

// insert sort for better priority queue performance

function insertSorted(inputarray, v, sortFn) 
{
	if (inputarray.length < 1 || sortFn(v, inputarray[inputarray.length-1]) >= 0) 
	{
		inputarray.push(v);
		return inputarray;
	}

	for (var i=inputarray.length-2; i>=0; --i) 
	{
		if(sortFn(v, inputarray[i]) >= 0) 
		{
			inputarray.splice(i+1, 0, v);
			return inputarray;
		}
	}

	inputarray.splice(0, 0, v);
	return inputarray;
}

function h(a, b) 
{
	var cross = 0;

//	if (dotTiebreaker) 
//	{
//		var dx1 = a.x - b.x
//		var dy1 = a.y - b.y
//		var dx2 = start.x - b.x
//		var dy2 = start.y - b.y
//		var cross = Math.abs(dx1*dy2 - dx2*dy1)
//	}
	
	if (allowDiagonals) 
	{
		var straight = Math.abs(Math.abs(a.x-b.x) - Math.abs(a.y-b.y));
		var diagonal = Math.max(Math.abs(a.x-b.x), Math.abs(a.y-b.y)) - straight;
		return straight + diagonalCost*diagonal + cross*0.001;
		//return Math.max(Math.abs(a.x-b.x), Math.abs(a.y-b.y)); simple version
	}
	return Math.abs(a.x-b.x)+Math.abs(a.y-b.y) + cross*0.001;
}

function makePath(x, y, x2, y2) 
{
	var start = allfloors[currentlevel].tiles[x][y];
	var goal = allfloors[currentlevel].tiles[x2][y2];
	
	var closed = {};
	var open = [start];
	
	var g_score = {}; // distance from start along optimal path
	var f_score = {}; // estimated distance from start to goal through node
	
	g_score[start.coord] = 0;
	f_score[start.coord] = h(start, goal);
	
	var cameFrom = {};
	
	var sortFn = function(b, a) 
	{
		return f_score[a.coord] - f_score[b.coord];
	};
	
	while(open.length > 0) 
	{
		var node = open.pop(); // node with lowest f score

		if (node == goal) 
		{
			var path = [goal];
			while (cameFrom[path[path.length-1].coord]) 
			{
				path.push(cameFrom[path[path.length-1].coord])
			}
			return path;
		}
		closed[node.coord] = true;
		
		var neighbours = node.makepathneighbours(goal);

		var c = neighbours.length;

		for(var i=0; i < c; ++i) 
		{
			var next = neighbours[i];
			
			if(closed[next.coord])
			{
				continue;
			}
			
			var diagonal = next.x != node.x && next.y != node.y;
			
			var temp_g_score = g_score[node.coord] + (diagonal?diagonalCost:1);
			var isBetter = false;
			
			var idx = open.indexOf(next);

			if(idx < 0) 
			{
				isBetter = true;
				//nodesSearched++;
			} else if (temp_g_score < g_score[next.coord]) 
			{
				open.splice(idx, 1); // remove old node
				isBetter = true;
			}
			
			if (isBetter) 
			{
				cameFrom[next.coord] = node;
				g_score[next.coord] = temp_g_score;
				f_score[next.coord] = g_score[next.coord] + h(next, goal);
				
				// add the new node or reinsert the old node
				open = insertSorted(open, next, sortFn);
										    
				// drawing
				//var s = Math.floor(g_score[next.coord]*4);
				//ctx.fillStyle = 'rgb('+(255-s)+',255,'+s+')';
				//ctx.fillRect(next.x*10, next.y*10, 10, 10);
			}
		}
	}
	// fail
	return [];
}

function pathFind(x, y, x2, y2) 
{
	var start = allfloors[currentlevel].tiles[x][y];
	var goal = allfloors[currentlevel].tiles[x2][y2];
	
	var closed = {};
	var open = [start];
	
	var g_score = {}; // distance from start along optimal path
	var f_score = {}; // estimated distance from start to goal through node
	
	g_score[start.coord] = 0;
	f_score[start.coord] = h(start, goal);
	
	var cameFrom = {};
	
	var sortFn = function(b, a) 
	{
		return f_score[a.coord] - f_score[b.coord];
	};
	
	while(open.length > 0) 
	{
		var node = open.pop(); // node with lowest f score

		if (node == goal) 
		{
			var path = [goal];
			while (cameFrom[path[path.length-1].coord]) 
			{
				path.push(cameFrom[path[path.length-1].coord])
			}
			return path;
		}
		closed[node.coord] = true;
		
		var neighbours = node.neighbours(goal);

		var c = neighbours.length;

		for(var i=0; i < c; ++i) 
		{
			var next = neighbours[i];
			
			if(closed[next.coord])
			{
				continue;
			}
			
			var diagonal = next.x != node.x && next.y != node.y;
			
			var temp_g_score = g_score[node.coord] + (diagonal?diagonalCost:1);
			var isBetter = false;
			
			var idx = open.indexOf(next);

			if(idx < 0) 
			{
				isBetter = true;
				//nodesSearched++;
			} else if (temp_g_score < g_score[next.coord]) 
			{
				open.splice(idx, 1); // remove old node
				isBetter = true;
			}
			
			if (isBetter) 
			{
				cameFrom[next.coord] = node;
				g_score[next.coord] = temp_g_score;
				f_score[next.coord] = g_score[next.coord] + h(next, goal);
				
				// add the new node or reinsert the old node
				open = insertSorted(open, next, sortFn);
										    
				// drawing
				//var s = Math.floor(g_score[next.coord]*4);
				//ctx.fillStyle = 'rgb('+(255-s)+',255,'+s+')';
				//ctx.fillRect(next.x*10, next.y*10, 10, 10);
			}
		}
	}
	// fail
	return [];
}