/*

Vorelike Copyright (c) 2013-2014 RogueLikeFox

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

var inputobj = new Object();

function setupinput()
{
	// need to set up actions:
	
	inputobj.move_up		= "w";
	inputobj.move_down		= "s";
	inputobj.move_left		= "a";
	inputobj.move_right		= "d";
	inputobj.open_inventory	= "v";
	inputobj.pleasure		= "r";
	inputobj.worldtime		= "u";
	inputobj.shadows		= "b";
	inputobj.viewmode		= "t";
	inputobj.musictoggle	= "m";
	inputobj.fullscreen		= "n";
	inputobj.lookaround		= "l";
	inputobj.skipturn		= "f";
	inputobj.zoomin			= "pageup";
	inputobj.zoomout		= "pagedown";
	inputobj.debugmode		= "tilde";
}

function assign_key(event, action)
{	
	var evtobj = window.event ? event : e; //distinguish between IE's explicit event object (window.event) and Firefox's implicit.
	var unicode = evtobj.charCode ? evtobj.charCode : evtobj.keyCode;
	var actualkey = String.fromCharCode(unicode).toLowerCase();
	
	if (unicode == 13)
	{
		actualkey = "return";
	} else if (unicode == 32) {
		actualkey = "space";
	} else if (unicode == 33) {
		actualkey = "pageup";
	} else if (unicode == 34) {
		actualkey = "pagedown";
	} else if (unicode == 192) {
		actualkey = "tilde";
	}	
	
	inputobj.action = actualkey;
}