art art/sprite.png
art art/spritevore.png
name sprite
strength 11
dexterity 36
endurance 6
perk fleety
perk damageup
lexicon
The sprite throws glowing dust on you for DMG damage
#
The sprite distracts you and causes you to trip for DMG damage
#
NEXTSTAGE
The sprite hurls a small rock at your eye causing DMG damage
#
A telekinetic spell from the sprite throws you against the ceiling for DMG damage
#
NEXTSTAGE
You wave off the sprite as it gets in your face
#
The sprite attempts to lead you into one of its traps but you are wiser
#
NEXTSTAGE 
You sidestep a pebble fired from the sprite's slingshot
#
The sprite attempts to lift you with magic but you resist the spell
#
END

The sprite swallows you
#
The sprite swallows you
#
The sprite swallows you
#
NEXTSTAGE
The sprite's stomach churns
#
The sprite's stomach churns
#
The sprite's stomach churns
#
NEXTSTAGE
The sprite digests you completely
#
The sprite digests you completely
#
The sprite digests you completely
#
END