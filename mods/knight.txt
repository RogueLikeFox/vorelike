art art/knight.png
art art/knightvore.png
name knight
strength 20
dexterity 28
endurance 20
perk tough
lexicon
The knight attacks you for DMG damage
#
The knight attacks you for DMG damage
#
NEXTSTAGE
The knight attacks you for DMG damage
#
The knight attacks you for DMG damage
#
NEXTSTAGE
The knight attacks but you are unharmed
#
The knight attacks but you are unharmed
#
NEXTSTAGE 
You manage to evade the knight's attacks
#
You manage to evade the knight's attacks
#
END

The knight swallows you
#
The knight swallows you
#
The knight swallows you
#
NEXTSTAGE
The knight's stomach churns
#
The knight's stomach churns
#
The knight's stomach churns
#
NEXTSTAGE
The knight digests you completely
#
The knight digests you completely
#
The knight digests you completely
#
END