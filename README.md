README

Welcome to Vorelike! This is an open-source, community-moddable Roguelike written in Javascript / HTML5 and designed to work on modern web browsers. This project was designed from the beginning to easily support modifications, or mods, and allow for anyone to create new monsters, items, or vore types using simple text files. 

The file download contains documentation that details how to create your own content. Further, it is possible for programmers to look at the code and improve upon it, if they so wish. While I am currently the only maintainer on this project, anyone is welcome (and encouraged!) to add or submit content to create a better game so that everyone can enjoy it.

Current included vore types:
Absorption vore (Slime)
Oral Vore (Wolf)
Unbirth / insertion (Succubus)
Many different vore types from OVRL monster files.

Here is the rundown of features:
* Interface is by default graphics based, though ASCII art is also implemented.
* Monsters are of different types: prey, predator, and apex predator. Prey types will flee from predators and apex predators, while predators will flee from apex predators.
* Not all monsters can eat the player - some, like rats, simply can not manage it.
* The player, currently, is prey-only, though there are plans to implement vore moves for the player in the future.
* There is a rudimentary inventory system in place, though only potions inhabit the dungeon right now, though future versions will undoubtedly include more items and even weapons.
* Moving between floors of the dungeon is easy, find some stairs and use the activate button (spacebar) to select it.
* Note that there is sound - swallowing, digestion sounds, etc. These can be turned off at any time by using the M key.

Controls:
* WASD moves your character around the dungeon.
* R key will "rub" or pleasure a creature that has swallowed you.
* Spacebar selects items on the floor or activates stairs.
* V key will open the player's inventory.
* W and S move the select bar within the inventory.
* Enter key selects options within the inventory.
* Spacebar moves dialog forward.
* Page Up / Page Down keys will zoom in or out on the player.
* T key switches between ASCII mode and graphics mode.
* B key switches the shadow system on/off.
* F key skips your turn.
* N key changes to/from fullscreen mode.
* U key toggles worldtime mode (whether monsters move on their own without player interaction).
* Walking into an enemy will attack them.
* Walking into a friendly will allow you to speak with them or "switch places" within the dungeon.

Running the game:
* Unzip the contents to a folder on your hard drive.
* Open the index.html file in your favorite browser (I test extensively on Firefox).
* Play.

If you use Chrome, you will need to follow a different procedure:
* Unzip the contents to a folder on your hard drive.
* Open the index.html file in Chrome.
* Drag and drop all the files that have a .TXT extension from the "mods" and "textures" folder onto the messagebox, the game should tell you that they are loaded (see why Chrome is a pain here?)
* Play.
-- OR --
Run Chrome with the --allow-file-access-from-files command-line flag.

Modding:
As mentioned above, I designed this game with the express intent of making it easy for people to add new monsters, new items and other things to the game. While I feel that simply looking through the game files will provide a good starting point for creating mods, I will endeavor to create a tutorial / documentation on how to add a new creature to the game and I will be working to making the creation and use of mods even easier in future releases.

Donations:
If you appreciate the work that I've done, you can send some Bitcoin my way: 1Q2d3iNWoN31NRv4BUE39msZGAn9oZy5YH