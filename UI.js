/*

Vorelike Copyright (c) 2013-2014 RogueLikeFox

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

// character generation

function startchargen()
{
	
}

function endchargen()
{

}

// end character generation

var testinterface = new interfaceui({name: "TestInterface", width:500, height:500, x:0, y:0, background: "#FFFFFF"});
testinterface.addbutton({name:"Save Game",x:1,y:1,width:100,height:100, background: "#ff0000"});
testinterface.addbutton({name:"Load Game",x:301,y:1,width:100,height:100, background: "#ff0000"});

function interfaceui(input)
{
	this.input = input;
	
	this.offsetx = 0;
	this.offsety = 0;
	
	if (document.getElementById("interfacecanvas") == null)
	{
		this.mycanvas = document.createElement("canvas");
		this.mycanvas.id = "interfacecanvas";
	} else {
		this.mycanvas = document.getElementById("interfacecanvas");
	}
	
	this.mousedown;		
	
	this.display = function()
	{
		this.mycanvas.style.position = "absolute";
		this.mycanvas.style.display = "block";
		this.mycanvas.style.zIndex = (Number(document.getElementById("GameBox").style.zIndex) + 1);	
		this.mycanvas.style.background = this.background;
		this.mycanvas.style.width = this.width;
		this.mycanvas.style.height = this.height;
		this.mycanvas.style.left = this.x;	
		this.mycanvas.style.top = this.y;
		this.mycanvas.width = this.width;
		this.mycanvas.height = this.height;
		
		if (document.getElementById("interfacecanvas") == null)
		{
			document.getElementById("GameBox").appendChild(this.mycanvas);
		}
		
		this.mycanvas.style.opacity = 0.0;
		
		gamestatus = "interfaceui";
		currentelement = this;
	};

	this.close = function()
	{
		this.mycanvas.style.zIndex = (Number(document.getElementById("GameBox").style.zIndex) - 1);
		gamestatus = "playing";
		currentelement = undefined;
	};
	
	this.takeinput = function(event)
	{	
		var element;	
	
		var mousex = 0;
		var mousey = 0;		
	
		this.input.e = event;
		if (event.type == "mousedown")
		{
			if (mousex < this.x || mousey < this.y || mousex > (this.x+this.width) || mousey > (this.y+this.height))
			{
				return;
			}		
		
			this.mousedown = event.button;
		}
		if (event.type == "mouseup")
		{
			this.mousedown = -1;
		
			mousex = (event.clientX - this.x);
			mousey = (event.clientY - this.y);	
			if (mousex < this.x || mousey < this.y || mousex > (this.x+this.width) || mousey > (this.y+this.height))
			{
				return;
			}
			
			for(element in this.elements)
			{
				element = this.elements[element];
				
				if (this.dragged != undefined && this.dragged == true)
				{
					this.dragged = false;
					element.click();
				}
				
				if ( mousex >= element.x && mousex <= (element.x+element.width) && mousey >= element.y && mousey <= (element.y+element.height) )
				{
					element.click();				
				}
			}
		} else if (event.type == "mousemove")
		{
			mousex = (event.clientX - this.x);
			mousey = (event.clientY - this.y);				
			
			if (this.mousedown != 0)
			{
				return;
			}
			
			for(element in this.elements)
			{
				element = this.elements[element];
				
				if ( mousex >= element.x && mousex <= (element.x+element.width) && mousey >= element.y && mousey <= (element.y+element.height) )
				{
					if (element.drag != undefined)
					{
						element.drag({mousex: mousex, mousey: mousey});		
					}
				}
			}			
		} else {
			//alert(event.type);
		}
	};

	this.resize = function()
	{

	}
	
	this.elementpositions = function(scrollbarw_x, scrollbarh_y)
	{
		if (scrollbarw_x != 0 && scrollbarw_x != undefined)
		{
			this.offsetx = scrollbarw_x;
		}
		if (scrollbarh_y != 0 && scrollbarh_y != undefined)
		{
			this.offsety = scrollbarh_y;
		}
	}.bind(this); // not sure why the bind is needed?
	
	this.addscrollbars = function(input)
	{
		var scrollbarw = new Object();
		
		var scrollbarh = new Object();
		
		scrollbarw.x = this.x;
		scrollbarw.y = this.height - 25;
		scrollbarw.interfacex = this.x;
		scrollbarw.interfacewidth = this.width - 25;
		scrollbarw.width = this.width - this.elementswidth - 50;
		scrollbarw.elementswidth = this.width + this.elementswidth;
		scrollbarw.height = 25;
		scrollbarw.color = input.scrollbarwcolor ? input.scrollbarwcolor : "#00FF00";
		scrollbarw.dragged = false;
		
		scrollbarw.parent = this;
		
		scrollbarh.x = this.width - 25;
		scrollbarh.y = this.y;		
		scrollbarh.interfacey = this.y;		
		scrollbarh.interfaceheight = this.height - 25;		
		scrollbarh.width = 25;		
		scrollbarh.height = this.height - this.elementsheight - 50;
		scrollbarh.elementsheight = this.height + this.elementsheight;
		scrollbarh.color = input.scrollbarhcolor ? input.scrollbarhcolor : "#00FF00";		
		scrollbarh.dragged = false;		
		
		scrollbarh.parent = this;		
		
		scrollbarw.dragfunc = this.elementpositions;		
		scrollbarh.dragfunc = this.elementpositions;				
		
		scrollbarw.draw = function()
		{
			var ctx = this.parent.mycanvas.getContext('2d');
			ctx.fillStyle = this.color;
			ctx.fillRect((this.x), (this.y), this.width, this.height);
		};
		scrollbarh.draw = scrollbarw.draw;
		
		scrollbarw.click = function()
		{
			this.startdragx = undefined;
			this.startdragy = undefined;
		};
		scrollbarh.click = function()
		{
			this.startdragx = undefined;
			this.startdragy = undefined;
		};				
		
		scrollbarw.drag = function(input)
		{
			if (this.startdragx == undefined)
			{
				this.startdragx = input.mousex;
			}
			if (this.startdragy == undefined)
			{
				this.startdragy = input.mousey;
			}
			
			//alert((this.width + this.x <= this.elementswidth + this.x) + ", " + this.width + " + " + this.x + " <= " + this.elementswidth + " + " + this.x);
			
			if (this.width < this.elementswidth)
			{
				this.x = this.x - (this.startdragx - input.mousex);
			}
			while (this.interfacewidth < this.x + this.width)
			{
				this.x = this.x - 1;
			}
			while (this.interfacex > this.x)
			{
				this.x = this.x + 1;
			}
			this.dragged = true;
			
			this.dragfunc((this.x - this.interfacex), 0);
		};
		
		scrollbarh.drag = function(input)
		{
			if (this.startdragx == undefined)
			{
				this.startdragx = input.mousex;
			}
			if (this.startdragy == undefined)
			{
				this.startdragy = input.mousey;
			}
			
			if (this.height < this.elementsheight)
			{
				this.y = this.y - (this.startdragy - input.mousey);
			}
			while (this.interfaceheight < this.y + this.height)
			{
				this.y = this.y - 1;
			}			
			while (this.interfacey > this.y)
			{
				this.y = this.y + 1;
			}						
			this.dragged = true;
			
			this.dragfunc(0, (this.y - this.interfacey));			
		};		
		
		if (input.width == true)
		{
			this.elements.push(scrollbarw);
		}
		if (input.height == true)
		{
			this.elements.push(scrollbarh);		
		}
	}
	
	this.addbutton = function(input)
	{
		var button = new Object();
		button.name = input.name ? input.name : "Button";
		button.x = input.x ? (input.x + this.x) : this.x;
		button.y = input.y ? (input.y + this.y) : this.y;
		button.width = input.width ? input.width : 50;
		button.height = input.height ? input.height : 50;
		button.background = input.background ? input.background : "#ff0000";
		button.text = input.text ? input.text : button.name;

		button.canvaswidth = this.width;
		button.canvasheight = this.height;

		button.parent = this;
		
		button.draw = function()
		{
			var ctx = this.parent.mycanvas.getContext('2d');
			ctx.fillStyle = this.background;
			
			if (this.canvaswidth < this.width)
			{
				this.width = this.canvaswidth - this.x;
			}
			if (this.canvasheight < this.height)
			{
				this.height = this.canvasheight - this.y;
			}	
			
			ctx.fillRect((this.x - this.parent.offsetx), (this.y - this.parent.offsety), this.width, this.height);
			ctx.fillStyle = "#000000";
			
			var textheight = ctx.measureText("M").width;
			
			while (ctx.measureText(this.text).width >= this.width)
			{
				this.text = this.text.slice(0, -1);
			}
			ctx.fillText( this.text, (this.x - this.parent.offsetx + (this.width/2) - (ctx.measureText(this.text).width/2)), (this.y - this.parent.offsety + (this.height/2) + (textheight/4)) );
			//alert((this.x) + " " + (this.y) + " " + this.width + " " + this.height + " " + this.background);
		};
		button.click = function()
		{
			// do stuff!
		};
		this.elements.push(button);
		
		if ((button.y + button.height) > this.height)
		{
			this.elementsheight = ((button.y + button.height) - this.height);
			this.addscrollbars({height:true});
		} 
		if ((button.x + button.width) > this.width)
		{
			this.elementswidth = ((button.x + button.width) - this.width);
			this.addscrollbars({width:true});
		}		
		// need to make a this.addscrollbar function
		// something like:
		// scrollbar width = this.width - (this.elementswidth - this.width);
		// if (scollbar x + scroll width >= this.width) { scrollbar.x = scrollbar.x );
	};

	if (input.e != undefined)
	{
		currentelement.input.e = input.e;
		currentelement.takeinput(currentelement.input);
		return;
	}

	this.name = input.name ? input.name : "Interface";
	this.width = input.width ? input.width : 200;
	this.height = input.height ? input.height : 200;
	
	this.elementswidth = 0;
	this.elementsheight = 0;
	
	this.x = input.x ? input.x : 0;
	this.y = input.y ? input.y : 0;
	this.background = input.background ? input.background : "#ffffff";
	this.elements = new Array();
	
	return this;
}

function create_logo(input)
{
	gamestatus = "logofull";
	
	createnotification({width: GetPageWidth()*0.98, height: GetPageHeight()*0.98, top: 0, left: 0, image: input.image, imagetext: "Click to Begin"});
}

function checkmenuarray()
{
	// not used.
	if (menuarray.length > 0)
	{
		var newbox = menuarray.pop();
		if (newbox.ischoice != undefined && newbox.ischoice == true)
		{
			newbox.displaychoice(newbox.talker);
		} else if (newbox.istextscene != undefined && newbox.istextscene == true)
		{
			newbox.doscene
		}
	}
}

function removenotification()
{
	var canvas = document.getElementById("Notification");
	
	canvas.width = 0;
	canvas.height = 0;
	canvas.style.border = "0px";
	canvas.style.zIndex = "-1";

	log(gamestatus);
	
	if (gamestatus == "logofull")
	{
		currentelement = undefined;	
	
		gamestatus = "modload";
	
		createnotification({text: "Please drag and drop the mods you wish to use here or simply click the mouse to load defaults.\n\nControls:\nWASD - Move / struggle / select choices\nR - Pleasure enemy (while in stomach)\nSpacebar - Activate items / release from friendly stomach\nReturn - Select option in menu\nV - Open inventory\nF - Skip turn\nM - Toggle sounds\nT - Switch graphics mode\nB - Toggle shadows\nU - Worldtime on/off"});

		printouttext("TextCanvas", true, "Using seed: " + gamerandomseed);	
		printouttext("TextCanvas", false, "You find yourself in a dungeon. The walls smell of mildew.");	
		
		return;
	}	
	
	if (gamestatus == "modload")
	{
		log("Loading default files and creating floor...");
		
		if (customfiles == false)
		{
			loadXMLfiles("./mods/DefaultItems.txt");
		}
		
		if (allfloors[currentlevel] != undefined && allfloors[currentlevel].tmx == true)
		{
			// loaded a TMX file.
		} else {
			var startfloor = new floor(0, 100, 100, gamerandomseed);				
		}
		
		// and add stuff to the floor.
		populatecurrentfloor(gamerandomseed);
		
		gamestatus = "playing";		
		
		viewmode = 1;
		
		currentelement = undefined;		
		
		getobjectreference("genderchoice").displaychoice("");
		
		return;
	}
	
	currentelement = undefined;				
}

function removenotificationiframe()
{
	// unused and old, look to removenotification for updates
	var iframe = document.getElementById("NotificationIframe");

	iframe.width = 0;
	iframe.height = 0;
	iframe.style.border = "0px";
	iframe.style.zIndex = "-1";
	iframe.style.display = "none";

	log(gamestatus);

	if (gamestatus == "logofull")
	{
		currentelement = undefined;	
	
		gamestatus = "modload";	
	
		createnotification({text: "Please drag and drop the mods you wish to use here or simply click the mouse to load defaults.\n\nControls:\nWASD - Move / struggle / select choices\nR - Pleasure enemy (while in stomach)\nSpacebar - Activate items / release from friendly stomach\nReturn - Select option in menu\nV - Open inventory\nF - Skip turn\nM - Toggle sounds\nT - Switch graphics mode\nB - Toggle shadows"});

		printouttext("TextCanvas", true, "Using seed: " + gamerandomseed);	
		printouttext("TextCanvas", false, "You find yourself in a dungeon. The walls smell of mildew.");	
		return;
	}
	
	if (gamestatus == "modload")
	{
		log("Loading default files and creating floor...");

		if (customfiles == false)
		{
			loadXMLfiles("./mods/DefaultItems.txt");
		}

		var startfloor = new floor(0, 100, 100, gamerandomseed);	
		
		// and add stuff to the floor.
		populatecurrentfloor(gamerandomseed);

		gamestatus = "playing";
		
		currentelement = undefined;					
		
		viewmode = 1;
		
		getobjectreference("genderchoice").displaychoice("");		
		return;
	}
	
	currentelement = undefined;			
}

function createnotification(input)
{
	this.resize = function()
	{
		if (gamestatus == "notification")
		{
			var canvas = document.getElementById("Notification");
			canvas.id = "Notification";
			canvas.width = this.width ? this.width : document.getElementById("MapCanvas").width * 0.90;
			canvas.height = this.height ? this.height : document.getElementById("MapCanvas").height * 0.25;
			canvas.style.zIndex = "1";
			canvas.style.position = "absolute";
			canvas.style.top = (document.getElementById("MapCanvas").height * 0.75).toString() + "px";
			canvas.style.left = (document.getElementById("MapCanvas").width * 0.05).toString() + "px";
			canvas.style.border = "1px solid";
			canvas.style.height = canvas.height + "px";
			canvas.style.width = canvas.width + "px";
			canvas.tabIndex = "4";		
		
			var output = new Object();
	
			output.canvasname = canvas.id;
			output.clear = true;
			output.intext = this.text;
	
			printoutcanvastext(output);		
		}
		
		return;
	};
	this.takeinput = function(event)
	{
		if (event.type == "mouseup" || event.type == "keyup")
		{	
			this.input.e = event;
			createnotification(this.input);
		}
	};

	this.input = input;
	currentelement = this;
	//currentelement = createnotification;
	//currentelement.input = input;	
	
	var canvas;
	
	if (document.getElementById("Notification") == null)
	{
		canvas = document.createElement('canvas');
	} else {
		canvas = document.getElementById("Notification");
	}	
	
	// mouse event or key...
	if (input.e != undefined) 
	{
		var evtobj = input.e;
		var mb = evtobj.button;
		var unicode = evtobj.charCode ? evtobj.charCode : evtobj.keyCode;
		var actualkey = String.fromCharCode(unicode).toLowerCase();	
	
		if (unicode == 13)
		{
			actualkey = "return";
		} else if (unicode == 32) {
			actualkey = "space";
		} else if (unicode == 33) {
			actualkey = "pageup";
		} else if (unicode == 34) {
			actualkey = "pagedown";
		} else if (unicode == 192) {
			actualkey = "tilde";
		}
	
		if (gamestatus == "logofull")
		{
			if (mb >= 0)	
			{				
				removenotification();
				return;
			}			
		}			
	
		if (gamestatus == "textscene")
		{
			if (actualkey == "space" || actualkey == "return" || mb >= 0)
			{
				input.currentscene.nextscene();
				return;
			}
		} else {
			if (gamestatus == "modload")
			{
				if (mb >= 0)
				{
					canvas.removeEventListener('dragover', handleDragOver, false);
					canvas.removeEventListener('drop', handleFileSelect, false);				
					removenotification();
					return;
				}
			} else {
				if (actualkey == "space" || actualkey == "return" || mb >= 0)	
				{				
					removenotification();
					gamestatus = "playing";
					return;
				}
			}
		}
		return;
	}

	canvas.id = "Notification";
	canvas.width = input.width ? input.width : document.getElementById("MapCanvas").width * 0.90;
	canvas.height = input.height ? input.height : document.getElementById("MapCanvas").height * 0.25;
	canvas.style.zIndex = "1";
	canvas.style.position = "absolute";
	canvas.style.top = (input.top != undefined) ? input.top.toString() + "px" : (document.getElementById("MapCanvas").height * 0.75).toString() + "px";
	canvas.style.left = (input.left != undefined) ? input.left.toString() + "px" : (document.getElementById("MapCanvas").width * 0.05).toString() + "px";
	canvas.style.border = "1px solid";
	canvas.style.height = canvas.height + "px";
	canvas.style.width = canvas.width + "px";
	canvas.tabIndex = "4";
	
	if (document.getElementById("Notification") == null)
	{
		document.getElementById("GameBox").appendChild(canvas);
	}	

	if (gamestatus == "modload")
	{
		canvas.addEventListener('dragover', handleDragOver, false);
		canvas.addEventListener('drop', handleFileSelect, false);
	}
	
	if (gamestatus != "textscene" && gamestatus != "choicebox" && gamestatus != "modload" && gamestatus != "logofull")
	{
		gamestatus = "notification";
	}
	
	var output = new Object();
	
	output.canvasname = canvas.id;
	output.clear = true;
	output.intext = input.text;

	this.text = input.text;
	
	if (input.image == undefined)
	{
		printoutcanvastext(output);
	} else {
		var logoimage = new Image();
		logoimage.src = input.image;
		var ctx = canvas.getContext("2d");
		ctx.fillRect(0, 0, canvas.width, canvas.height);
		logoimage.onload = function() {
			ctx.drawImage(logoimage, (canvas.width/4), (canvas.height/16));
			if (input.imagetext != undefined)
			{
				ctx.fillStyle = "#FFFFFF";
				ctx.font = "30px Arial";
				ctx.fillText(input.imagetext, (canvas.width/4)+(this.width/2)-(ctx.measureText(input.imagetext).width/2), (canvas.height/16)+this.height+30);
			}			
		}
	}
	return this;
}

function printouttext(textbox, clear, text)
{
	if (text == false || text == undefined || text == "")
	{
		return;
	}

	var c = document.getElementById(textbox);

	if (textbuffer[textbox] && clear == false)
	{
		textbuffer[textbox] = textbuffer[textbox] + text + "\n";
		c.value = c.value + text + "\n";
	} else {
		textbuffer[textbox] = text + "\n";
		c.value = text + "\n";
	}
	c.scrollTop = c.scrollHeight;
}

createnotification.prototype.takeinput = function(event)
{
	this.input.e = event;
	createnotification(this.input);
}

function printoutcanvastext(input)
{
	var canvasname = input.canvasname;
	var clear = input.clear;
	var intext = input.intext;

	var c = document.getElementById(canvasname);
	
	var ctx = c.getContext("2d");
	ctx.font = FONT_SIZE_MESSAGEBOX + "px Arial";

	var distancelines = 20;

	clearcanvas(c);

	if (textbuffer[canvasname] && clear == false)
	{
		textbuffer[canvasname] = textbuffer[canvasname] + intext + "\n";
	} else {
		textbuffer[canvasname] = intext + "\n";
	}

	var lines = textbuffer[canvasname].split("\n");
	
	var measuretextlength = c.width;
	
	if (isArray(lines) && lines.length > 2)
	{
		var line;

		for(line = 0; line <= (lines.length-1); line++)
		{
			if (ctx.measureText(lines[line]).width >= measuretextlength)
			{
				c.width = ctx.measureText(lines[line]).width + distancelines*2;
				measuretextlength = c.width;
			}
		}
	} else {
		if (ctx.measureText(textbuffer[canvasname]).width >= measuretextlength)
		{
			c.width = ctx.measureText(textbuffer[canvasname]).width + distancelines*2;
			measuretextlength = c.width;
		}
	}

	while ((lines.length * distancelines) >= (c.height))
	{
		c.height += distancelines;
		//lines.shift();
	}

	ctx.font = FONT_SIZE_MESSAGEBOX + "px Arial";

	if (viewmode == 0)
	{
		ctx.fillStyle = "#FFFFFF";
		ctx.fillRect(0, 0, c.width, c.height);
		ctx.fillStyle = "#000000";		
		for (var i = 0; i < lines.length; i++)
		{
			ctx.fillText(lines[i], distancelines, (i+1)*distancelines);
		}				
	} else {
		var backgroundimage;

		if (imagesrcs["notificationbackground"] == undefined)
		{
			backgroundimage = new Image();
			backgroundimage.src = "./textures/scroll_background.jpg";
			backgroundimage.onload = function() {
				ctx.drawImage(backgroundimage, 0, 0);			
				ctx.fillStyle = "#000000";

				for (var i = 0; i < lines.length; i++)
				{
					ctx.fillText(lines[i], distancelines, (i+1)*distancelines);
				}				
			};
			imagesrcs["notificationbackground"] = backgroundimage;
		} else {
			backgroundimage = imagesrcs["notificationbackground"];
			ctx.drawImage(backgroundimage, 0, 0);		
			ctx.fillStyle = "#000000";

			for (var i = 0; i < lines.length; i++)
			{
				ctx.fillText(lines[i], distancelines, (i+1)*distancelines);
			}			
		}	
	}	
}

function getchoice()
{
	return chosenchoice;
	chosenchoice = 0;
}

function removechoicebox(input)
{
	var canvas = document.getElementById("Choicebox");

	canvas.width = 0;
	canvas.height = 0;
	canvas.style.border = "0px";
	canvas.style.zIndex = "-1";

	if (gamestatus != "modload")
	{
		gamestatus = "playing";
	}
	
	input.chosenchoice = 0;
	input.chosenchoicefinal = -1;
	input.e = null;
	
	currentelement = undefined;
	
	if (input.nextchoice != "")
	{
		if (getobjectreference(input.nextchoice) != false && getobjectreference(input.nextchoice) != undefined)
		{
			getobjectreference(input.nextchoice).displaychoice("");
		}
	}
}

function choicebox(input)
{
	this.resize = function()
	{
		var canvas = document.getElementById("Choicebox");
		canvas.id = "Choicebox";
		canvas.width = document.getElementById("MapCanvas").width * 0.90;
		canvas.height = document.getElementById("MapCanvas").height * 0.25;
		canvas.style.zIndex = "1";
		canvas.style.position = "absolute";
		canvas.style.top = (document.getElementById("MapCanvas").height * 0.75).toString() + "px";
		canvas.style.left = (document.getElementById("MapCanvas").width * 0.05).toString() + "px";
		canvas.style.border = "1px solid";
		canvas.style.height = canvas.height + "px";
		canvas.style.width = canvas.width + "px";
		canvas.tabIndex = "4";			
		
		var output = new Object();
	
		output.canvasname = canvas.id;
		output.clear = true;
		output.intext = (this.questiontext + this.choicetext);
	
		printoutcanvastext(output);		
		return;
	};

	this.takeinput = function(event)
	{
		if (event.type == "mouseup" || event.type == "keyup")
		{
			this.input.e = event;
			choicebox(this.input);
		}
	};

	gamestatus = "choicebox";
	this.input = input;
	currentelement = this;
	
	if (input.e != undefined && input.e.type != undefined)
	{
		var evtobj = window.event ? input.e : input.e; //distinguish between IE's explicit event object (window.event) and Firefox's implicit.
		var unicode = evtobj.charCode ? evtobj.charCode : evtobj.keyCode;
		var actualkey = String.fromCharCode(unicode).toLowerCase();
	
		if (unicode == 13)
		{
			actualkey = "return";
		} else if (unicode == 32) {
			actualkey = "space";
		} else if (unicode == 33) {
			actualkey = "pageup";
		} else if (unicode == 34) {
			actualkey = "pagedown";
		} else if (unicode == 192) {
			actualkey = "tilde";
		}	
	
		if (actualkey == inputobj.move_up && input.chosenchoice > 0)
		{
			input.chosenchoice = input.chosenchoice - 1;
		} else if (actualkey == inputobj.move_down && input.chosenchoice < (input.choices.length)-1) {
			input.chosenchoice = input.chosenchoice + 1;
		}

		var i = 0;
		
		if (isArray(input.choices) == true && isArray(input.outcomes) == true)
		{		
			for (i = 0; i < input.choices.length; i++)
			{
				if (i == 0)
				{
					input.choicetext = "";
				}
				if (i == input.chosenchoice)
				{
					input.choicetext = input.choicetext + "[" + input.choices[i] + "]\n";
				} else {
					input.choicetext = input.choicetext + " " + input.choices[i] + " \n";
				}
			}
		} else {
			input.chosenchoice = 0;
			input.choicetext = "[" + input.choices + "]\n";
			var outcome = input.outcomes;
			input.outcomes = new Array();
			input.outcomes.push(outcome);
		}
		
		if (actualkey != "return") 
		{
			var output = new Object();
			output.canvasname = input.choicecanvas.id;
			output.clear = true;
			output.intext = input.questiontext + input.choicetext
			printoutcanvastext(output);
		} else {
			// hide the box

			if (input.choices[input.chosenchoice] == "Cancel")
			{
				input.chosenchoice = 0;
				input.chosenchoicefinal = -1;
				gamestatus = "playing";
				removechoicebox(input);
				return;
			}
			
			input.chosenchoicefinal = input.chosenchoice;
			
			if (input.outcomes[input.chosenchoice].indexOf("switchplaces()") == 0)
			{
				var characterid = input.talker;

				characterid.switchplaces(getobjectreference("player"));	
				removechoicebox(input);
				return;			
			}

			if (input.outcomes[input.chosenchoice].indexOf("setvariable(") == 0)
			{
				var choiceid = input.outcomes[input.chosenchoice].replace("setvariable(","");
				choiceid = choiceid.split(",");
				choiceid[1] = choiceid[1].replace(")","");
				
				choiceid[0] = choiceid[0].trim();
				choiceid[1] = choiceid[1].trim();
				
				// choiceid = command, choiceid[1] = new value

				var tempfunc = new Function( "" + choiceid[0] + "=" + choiceid[1] + ";" );
				tempfunc();
				// yay for no eval!
				
				removechoicebox(input);
				return;
			}
			
			if (input.outcomes[input.chosenchoice].indexOf("concatvariable(") == 0)
			{
				var choiceid = input.outcomes[input.chosenchoice].replace("concatvariable(","");
				choiceid = choiceid.split(",");
				choiceid[1] = choiceid[1].replace(")","");
				
				// choiceid = command, choiceid[1] = new value
				
				choiceid[0] = choiceid[0].trim();
				choiceid[1] = choiceid[1].trim();
				choiceid[1] = choiceid[1].replace("'","");
				choiceid[1] = choiceid[1].replace("'","");		
				
				if (choiceid[1] != "")
				{
					var tempfunc = new Function( "" + choiceid[0] + "=" + choiceid[0] + "+\"" + choiceid[1] + ",\";" );
					tempfunc();
					// yay for no eval!
				}
				removechoicebox(input);				
				return;
			}			
			
			if (input.outcomes[input.chosenchoice].indexOf("anyswallow(") == 0 || input.outcomes[input.chosenchoice].indexOf("digest()") == 0)
			{
				//var characterid = currentchoice.outcomes[chosenchoice].replace("anyswallow(","");
				//characterid = characterid.replace(")","");

				var characterid = input.talker;

				if (input.outcomes[input.chosenchoice].indexOf("digest()") == 0)
				{
					getobjectreference("player").hp = 0;
				}

				log("Utilities: Talker = " + characterid);

				var i = 0;
				var finali = -1;
				var tries = 10;

				while (finali == -1 && tries > 0)
				{
					for (i = 0; i < characterid.swallowmoves.length; i++)
					{
						if (characterid.swallowmoves[i].isstart == true)
						{
							// got a starting move.
							var randomnumber = Math.floor((Math.random()*10)+1);

							if (randomnumber > 5)
							{
								characterid.swallow(getobjectreference("player"));
								i = characterid.swallowmoves.length;
								finali == i;
								removechoicebox(input);								
								return;
							}
						}
					}
					// if we are here, we didn't find a swallowmove?
					tries = tries - 1;
				}
				removechoicebox(input);
				return;
			}

			if (input.outcomes[input.chosenchoice].indexOf("swallow(") == 0 || input.outcomes[input.chosenchoice].indexOf("digest(") == 0)
			{
				// chosen swallow move...
				var characterid = input.talker;

				if (input.outcomes[input.chosenchoice].indexOf("swallow(") == 0)
				{
					var choiceid = input.outcomes[input.chosenchoice].replace("swallow(","");
					choiceid = choiceid.replace(")","");
				} else {
					var choiceid = input.outcomes[input.chosenchoice].replace("digest(","");
					choiceid = choiceid.replace(")","");				
					getobjectreference("player").hp = 0;					
				}

				var i = 0;

				for (i = 0; i < characterid.swallowmoves.length; i++)
				{
					if (characterid.swallowmoves[i].referenceid == choiceid)
					{
						characterid.swallow(getobjectreference("player"), choiceid);
					}
				}
				removechoicebox(input);
				return;
			}

			if (getobjectreference(input.outcomes[input.chosenchoice]))
			{
				if (getobjectreference(input.outcomes[input.chosenchoice]).ischoice == true)
				{
					getobjectreference(input.outcomes[input.chosenchoice]).displaychoice(input.talker);
				} else if (getobjectreference(input.outcomes[input.chosenchoice]).istextscene == true)
				{
					getobjectreference(input.outcomes[input.chosenchoice]).doscene(input.talker);
				}
				removechoicebox(input);
				return;
			} else {
				if (input.outcomes[input.chosenchoice] == "godownlevel")
				{
					log(allfloors[currentlevel].level);
					allfloors[currentlevel].godownlevel();
				} else if (input.outcomes[input.chosenchoice] == "gouplevel") 
				{
					log(allfloors[currentlevel].level);
					allfloors[currentlevel].gouplevel();
				} else if (input.outcomes[input.chosenchoice] == "activate")
				{
					// this is likely an in-game item.
					if (input.canactivate == true && input.deleted == false)
					{
						input.activate(getobjectreference("player"));
					}
				} else if (input.outcomes[input.chosenchoice] == "objectselect")
				{
					// selecting an object in an inventory
					// either use or drop it.
					
					var objchoice = ("Use " + input.objindx[input.chosenchoice].name + "|Drop " + input.objindx[input.chosenchoice].name).split("|");
					var objcommands = "useitem|dropitem".split("|");

					removechoicebox(input);

					var choiceinv = new choiceobject( { name: "InvChoice", referenceid: -1, choices: objchoice, outcomes: objcommands, objindx: input.objindx[input.chosenchoice] } );
					
					choiceinv.displaychoice(getobjectreference("player"));					
				} else if (input.outcomes[input.chosenchoice] == "dropitem")
				{
					input.objindx.drop(getobjectreference("player"));
				} else if (input.outcomes[input.chosenchoice] == "useitem")
				{
					input.objindx[input.chosenchoice].use(getobjectreference("player"));
				}
				removechoicebox(input);
				return;				
			}
		}
		return;
	}	
	
	if (input.allowcancel == true)
	{
		if (input.textchoices.indexOf("|Cancel") == -1)
		{
			input.textchoices = input.textchoices + "|Cancel";
		}
	} else {
		if (input.textchoices.indexOf("|Cancel") != -1)
		{
			input.textchoices = input.textchoices.replace("|Cancel", "");
		}	
	}
	
	input.chosenchoice = 0;
	
	if (input.textchoices.indexOf("|") >= 0)
	{
		input.choices = input.textchoices.split("|");
	} else {
		input.choices = input.textchoices;
	}
	
	input.questiontext = input.questiontext?input.questiontext+"\n":"";
	
	input.choicetext = "";	
	
	if (isArray(input.choices) == true)
	{
		input.totalchoices = input.choices.length;
	
		for (var i = 0; i < input.choices.length; i++)
		{
			if (i == input.chosenchoice)
			{
				input.choicetext = input.choicetext + "[" + input.choices[i] + "]\n";
			} else {
				input.choicetext = input.choicetext + " " + input.choices[i] + " \n";
			}
		}
	} else {
		input.chosenchoice = 0;
		input.totalchoices = 1;
		input.choicetext = "[" + input.choices + "]\n";
	}

	var canvas;

	if (document.getElementById("Choicebox") == null)
	{
		canvas = document.createElement('canvas');
	} else {
		canvas = document.getElementById("Choicebox");
	}

	canvas.id = "Choicebox";
	canvas.width = document.getElementById("MapCanvas").width * 0.90;
	canvas.height = document.getElementById("MapCanvas").height * 0.25;
	canvas.style.zIndex = "1";
	canvas.style.position = "absolute";
	canvas.style.top = (document.getElementById("MapCanvas").height * 0.75).toString() + "px";
	canvas.style.left = (document.getElementById("MapCanvas").width * 0.05).toString() + "px";
	canvas.style.border = "1px solid";
	canvas.style.height = canvas.height + "px";
	canvas.style.width = canvas.width + "px";
	canvas.tabIndex = "4";
	
	input.choicecanvas = canvas;
	
	if (document.getElementById("Choicebox") == null)
	{
		document.getElementById("GameBox").appendChild(canvas);
	}
	
	var output = new Object();
	
	this.questiontext = input.questiontext;
	this.choicetext = input.choicetext;
	
	output.canvasname = canvas.id;
	output.clear = true;
	output.intext = (input.questiontext + input.choicetext);
	
	printoutcanvastext(output);
	return this;
}