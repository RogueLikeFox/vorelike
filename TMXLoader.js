/*

Vorelike Copyright (c) 2013-2014 RogueLikeFox

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

// This file loads and sets up a TMX XML Map, shimming into the Map.js system.

var mapxmldoc = "";

var tileSets = new Array();
var totalTileSets = 0;

var TMXMap = new Object();

var mapWidth;
var mapHeight;
var tileWidth;
var tileHeight;

function TileSet(firstgid, name, intileWidth, intileHeight, source, imageWidth, imageHeight)
{
	this.firstgid = firstgid;
	this.name = name;
	this.tileWidth = intileWidth;
	this.tileHeight = intileHeight;
	this.source = source;
	this.imageWidth = imageWidth;
	this.imageHeight = imageHeight;
	this.tileAmountWidth	= 0;
	this.tileAmountWidth = Number(Math.floor(imageWidth / tileWidth));
	this.lastgid = 0;
	this.lastgid = this.tileAmountWidth * Math.floor(imageHeight / tileHeight) + (this.firstgid - 1);
	return this;
}

function loadTMX(fileurl)
{
	if (window.XMLHttpRequest)
	{
		// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	}
	xmlhttp.crossOrigin="*";	
	xmlhttp.open("GET", fileurl, false);
	xmlhttp.onload = function() {
		mapxmldoc = this.responseXML;
		parseTMX(mapxmldoc);
	};
	//alert("Sending TMX file request...");
	xmlhttp.send();
}

function parseTMX(xmldoc)
{
	//alert("Parsing TMX file...");
	var xml = xmldoc;
	mapWidth = xml.getElementsByTagName("map")[0].getAttribute("width");
	mapHeight = xml.getElementsByTagName("map")[0].getAttribute("height");
	tileWidth = xml.getElementsByTagName("map")[0].getAttribute("tilewidth");
	tileHeight = xml.getElementsByTagName("map")[0].getAttribute("tileheight");
	var xmlCounter = 0;
 
	var mapsize = 0;
 
	if (mapWidth > mapHeight)
	{
		mapsize = mapWith;
	} else {
		mapsize = mapHeight;
	}
	
	TMXMap.tiles = new Array(new Array());
	
	for (var x = 0; x < mapsize; x++)
	{
		TMXMap.tiles[x] = new Array();
		for (var y = 0; y < mapsize; y++)
		{
			TMXMap.tiles[x][y] = new tile( { walkable: true, blocked: false, type: -1, texture: 'transparent', char: '', color: '#000000', x: x, y: y } );
			TMXMap.tiles[x][y].layers = new Array();
		}	
	}
	TMXMap.size = mapsize;
 
	var output = "Created tilesets: \n";
 
	for (var input in xml.getElementsByTagName("tileset")) 
	{
		if (!(!isNaN(parseFloat(input)) && isFinite(input)))
		{
			continue;
		}
		var tileset = xml.getElementsByTagName("tileset")[input];
		var image = tileset.getElementsByTagName("image")[0];
		
		var imageWidth = image.getAttribute("width");
		var imageHeight = image.getAttribute("height");
		var firstGid = tileset.getAttribute("firstgid");
		var tilesetName = tileset.getAttribute("name");
		var tilesetTileWidth = tileset.getAttribute("tilewidth");
		var tilesetTileHeight = tileset.getAttribute("tileheight");
		var tilesetImagePath = "./maps/" + image.getAttribute("source");
		var newtile = new TileSet(firstGid, tilesetName, tilesetTileWidth, tilesetTileHeight, tilesetImagePath, imageWidth, imageHeight);
		tileSets.push(newtile);
		output = output + tilesetName + "(" + firstGid + ")\n";
		xmlCounter++;
	}
	totalTileSets = xmlCounter;
	
	addTileBitmapData();
}

function addTileBitmapData()
{
	// load each layer
	var layernum = 0;
	
	var numberoflayers = 0;
	var layerMap = 0;
	
	var tileX;
	var tileY;	
	
	for (var numlayer in mapxmldoc.getElementsByTagName("layer")) 
	{
		var thelayer = mapxmldoc.getElementsByTagName("layer")[numlayer];
		if (thelayer.getAttribute == undefined || thelayer.getAttribute("name") == undefined)
		{
			continue;
		}
		
		//alert("Creating layer: " + numberoflayers + ", " + thelayer.getAttribute("name"));		
		
		var tiles = new Array();
		var tileLength = 0;
		// assign the gid to each location in the layer
		for (var tilenum in thelayer.childNodes[1].childNodes) 
		{
			var tilebit = thelayer.childNodes[1].childNodes[tilenum];
			if (tilebit.nodeName == "tile")
			{
				var gid = tilebit.getAttribute("gid");
				// if gid > 0
				if (gid > 0) {
					tiles[tileLength] = gid;
				}
				tileLength++;
			}
		}
	
		var layerName = thelayer.getAttribute("name");
	
		// decide where we're going to put the layer
		layerMap = numberoflayers;
	
		// store the gid into a 2d array
		
		var tileCoordinates = new Array();
		
		for (tileX = 0; tileX < mapWidth; tileX++) 
		{
			tileCoordinates[tileX] = new Array();
			for (tileY = 0; tileY < mapHeight; tileY++) 
			{
				tileCoordinates[tileX][tileY] = tiles[(tileX+(tileY*mapWidth))];
			}
		}		
		
		for (tileX = 0; tileX < mapWidth; tileX++) 
		{
			for (tileY = 0; tileY < mapHeight; tileY++) 
			{
				if (tileCoordinates[tileX][tileY] == undefined)
				{
					continue;
				}
				var tileGid = tileCoordinates[tileX][tileY];
				var currentTileset;
				//alert("Tilegid: " + tileGid + ", " + tileSets.length);
				// only use tiles from this tileset (we get the source image from here)
				
				for (var tilesetnum in tileSets) 
				{
					var tilesetone = tileSets[tilesetnum];
					if (tileGid >= (tilesetone.firstgid-1) && tileGid <= tilesetone.lastgid) 
					{
						//alert(tilesetone.firstgid + ", " + tileGid + ", " + tilesetone.lastgid);					
						// we found the right tileset for this gid!
						currentTileset = tilesetone;
						break;
					}
				}
				
				if (currentTileset == undefined)
				{
					//continue;
				}
				
				var destY = Math.round(tileY * currentTileset.tileHeight);
				var destX = Math.round(tileX * currentTileset.tileWidth);
				// basic math to find out where the tile is coming from on the source image
				tileGid -= (currentTileset.firstgid - 1);				
				var sourceY = Math.ceil(tileGid/currentTileset.tileAmountWidth)-1;
				var sourceX = tileGid - (currentTileset.tileAmountWidth * sourceY) - 1;				
				
				if (TMXMap.tiles[tileX][tileY].layers == undefined)
				{
					TMXMap.tiles[tileX][tileY].layers = new Array();
				}
				TMXMap.tiles[tileX][tileY].layers[numberoflayers] = new tile( { gid: tileGid, walkable: true, blocked: false, type: 0, texture: (currentTileset.name + layerMap + tileGid), char: '', color: '#000000', x: tileX, y: tileY, layer:  (numberoflayers)} );
				
				// load images for tileset
				var tileimage = new textureobject({texturefiles: (currentTileset.source), referenceid: (currentTileset.name + layerMap + tileGid), animtype: 1});			
				
				var spritesheetstring = "" + Math.round(sourceX * currentTileset.tileWidth) + ", " + Math.round(sourceY * currentTileset.tileHeight) + ", " + Math.round(currentTileset.tileWidth) + ", " + Math.round(currentTileset.tileHeight);
				
				if (spritesheetstring.indexOf("NaN") > -1)
				{
					alert("Problem with TMX: " + tileGid);
				}
				
				getobjectreference(currentTileset.name + layerMap + tileGid).spritesheetpoints = new Array();
				getobjectreference(currentTileset.name + layerMap + tileGid).spritesheetpoints.push(spritesheetstring);
				getobjectreference(currentTileset.name + layerMap + tileGid).canvaswidth = Math.round(currentTileset.tileWidth);
				getobjectreference(currentTileset.name + layerMap + tileGid).canvasheight = Math.round(currentTileset.tileHeight);
			}
		}
		
		numberoflayers = numberoflayers + 1;		
		
		if (currentTileset.source.indexOf("tree") > -1)
		{
			//alert("Creating a tree at layer: " + layerMap + "\n" + TMXMap.tiles[tileX][tileY].layer[layerMap].texture);
		}		
	}
	allfloors[currentlevel] = TMXMap;
	allfloors[currentlevel].tmx = true;
	allfloors[currentlevel].layers = numberoflayers;
	// need to figure out here how to populate TMX map. Probably using tile properties?
	//alert("Finished loading TMX file");
}