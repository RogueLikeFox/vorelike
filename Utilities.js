/*

Vorelike Copyright (c) 2013-2014 RogueLikeFox

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

var textbuffer = new Array();

var player_took_turn = false; // whether the player did an action / took his turn
var movedbool = true; // whether player moved successfully

var debuglevel = 0;

var consolemode = false;

var customfiles = false;

/** Function count the occurrences of substring in a string;
 * @param {String} string   Required. The string;
 * @param {String} subString    Required. The string to search for;
 * @param {Boolean} allowOverlapping    Optional. Default: false;
 */
function occurrences(string, subString, allowOverlapping)
{

    string+=""; subString+="";
    if (subString.length<=0) return string.length+1;

    var n=0, pos=0;
    var step = (allowOverlapping)?(1):(subString.length);

    while(true)
	{
        pos=string.indexOf(subString,pos);
        if(pos>=0) 
		{ 
			n++; pos+=step; 
		} else { break; }
    }
    return(n);
}

function log(text)
{
	if (debuglevel >= 1)
	{
		alert(text);
	}
}

function getrandomelement(inputarray)
{	
	if (inputarray == undefined)
	{
		return false;
	}

	if (Object.prototype.toString.call( inputarray ) === '[object Array]')
	{
		var randnum = Math.round(Math.random() * (inputarray.length-1));
		var output = inputarray[randnum];
		return output;
	} else {
		return inputarray;
	}
	return false;
}

function GetPageWidth()
{
	var x = 0;
	if (self.innerHeight)
	{
		x = self.innerWidth;
	}
	else if (document.documentElement && document.documentElement.clientHeight)
	{
		x = document.documentElement.clientWidth;
	}
	else if (document.body)
	{
		x = document.body.clientWidth;
	}
	return Math.round(x);
}

function GetPageHeight()
{
	var y = 0;
	if (self.innerHeight)
	{
		y = self.innerHeight;
	}
	else if (document.documentElement && document.documentElement.clientHeight)
	{
		y = document.documentElement.clientHeight;
	}
	else if (document.body)
	{
		y = document.body.clientHeight;
	}
	return Math.round(y);
}

function centermapcanvasx()
{
	return Math.round(Number(document.getElementById("MapCanvas").width)*.5);
}

function centermapcanvasy()
{
	return Math.round(Number(document.getElementById("MapCanvas").height)*.5);
}

function replacetext(input)
{
	if (input.intext == undefined || input.intext == false)
	{
		alert("Replacetext error: " + input.intext);
		return "";
	}

	var returntext = "";
	returntext = input.intext;
	
	try {
		returntext = returntext.replace("DMG", input.damage);
	}
	catch(err) {
		alert("Error: " + input.intext);
	}
	
	returntext = returntext.replace("GND", input.gender);
	returntext = returntext.replace("CRT", input.creaturename);
	returntext = returntext.replace("HP", input.hp);
	returntext = returntext.replace("THP", input.totalhp);
	if (input.gender == "Male")
	{
		returntext = returntext.replace("GNDA", "his");
	} else if (input.gender == "Female")
	{
		returntext = returntext.replace("GNDA", "her");
	} else {
		returntext = returntext.replace("GNDA", "their");
	}
	return returntext;
}

function handlemouseclick(e)
{
	// mouse time!

	if (currentelement != undefined)
	{
		if (e.type == "mousedown")
		{
			if (this.last_type != "mousedown")
			{
				this.last_type = e.type;
				currentelement.takeinput(e);
			}
		} else if (e.type == "mouseup") {
			if (this.last_type != "mouseup")
			{
				this.last_type = e.type;
				currentelement.takeinput(e);
			}
		} else if (e.type == "mousemove") {
			//if (this.last_type != "mousemove")
			//{
				this.last_type = e.type;
				currentelement.takeinput(e);
			//}			
		} else {
			this.last_type = undefined;
		}
		return;
	}
	
	if (e.type == "mousemove")
	{
		return;
	}
	
	player_took_turn = false;
	movedbool = true;
	
	if (gamestatus == "notification" || gamestatus == "modload")
	{
		//removenotification();
		return;
	}

	if (gamestatus != "playing")
	{
		return;
	}

	if (getobjectreference("player").dead == true)
	{
		return;
	}

	if (consolemode)
	{
		var mousex = Math.floor(screenx + e.clientX/TILE_SIZE);
		var mousey = Math.floor(screeny + e.clientY/TILE_SIZE);
		
		if (getobjectat(mousex, mousey).objectindex != undefined)
		{
			alert(getobjectat(mousex, mousey).objectindex);
		}
		
		return;
	}
	
	if (e.clientX < (centermapcanvasx() + centermapcanvasx() * 0.5) && e.clientX > (centermapcanvasx() - centermapcanvasx() * 0.5))
	{
		// handle y now
		if (e.clientY > (centermapcanvasy() + centermapcanvasy() * 0.5))
		{
			// like hitting w key
			movedbool = getobjectreference("player").move(0, 1);
			player_took_turn = true;
		} else {
			// like hitting s key
			movedbool = getobjectreference("player").move(0, -1);
			player_took_turn = true;
		}
	}

	if (e.clientX > centermapcanvasx() && player_took_turn == false)
	{
		if (e.clientX > (centermapcanvasx() + centermapcanvasx() * 0.5))
		{
			// like hitting d key			
			movedbool = getobjectreference("player").move(1, 0);
			player_took_turn = true;
		}
	} else {
		if (e.clientX < (centermapcanvasx() - centermapcanvasx() * 0.5))
		{
			// like hitting a key
			movedbool = getobjectreference("player").move(-1, 0);
			player_took_turn = true;
		}
	}

	if (player_took_turn == true && movedbool == true)
	{
		renderall({handleactors: true});
	}
}

function handlekeyboard(e)
{
	if (currentelement != undefined)
	{
		currentelement.takeinput(e);
		return;
	}

	var evtobj = window.event ? event : e; //distinguish between IE's explicit event object (window.event) and Firefox's implicit.
	var unicode = evtobj.charCode ? evtobj.charCode : evtobj.keyCode;
	var actualkey = String.fromCharCode(unicode).toLowerCase();
	
	if (unicode == 13)
	{
		actualkey = "return";
	} else if (unicode == 32) {
		actualkey = "space";
	} else if (unicode == 33) {
		actualkey = "pageup";
	} else if (unicode == 34) {
		actualkey = "pagedown";
	} else if (unicode == 192) {
		actualkey = "tilde";
	}

	if (actualkey == inputobj.debugmode)
	{
		// debug mode.
		if (consolemode)
		{
			consolemode = false;
		} else {
			consolemode = true;
		}
	}
	
	if (consolemode)
	{
		return;
	}
	
	if (getobjectreference("player").dead == true)
	{
		if (actualkey == "return")
		{
			bigredresetbutton();
			return;
		}
		return;
	}

	if (actualkey == inputobj.worldtime)
	{
		if (worldtimetoggle == true)
		{
			worldtime(false);
		} else {
			worldtime(true);
		}
	}
	
	if (actualkey == inputobj.shadows) 
	{
		if (engine.useshadows == true)
		{
			engine.useshadows = false;
		} else {
			engine.useshadows = true;
		}
	} else if (actualkey == inputobj.viewmode) {
		viewmode = viewmode + 1;

		if (viewmode > 1)
		{
			viewmode = 0;
		}
	}		
	
	if (actualkey == inputobj.musictoggle)
	{
		if (playgamesounds == true)
		{
			playgamesounds = false;
		} else {
			playgamesounds = true;
		}
	}

	if (actualkey == inputobj.fullscreen)
	{
		if (
			document.fullscreenEnabled ||
			document.webkitFullscreenEnabled ||
			document.mozFullScreenEnabled ||
			document.msFullscreenEnabled
		) {
			// supported
			if (
				document.fullscreenElement ||
				document.webkitFullscreenElement ||
				document.mozFullScreenElement ||
				document.msFullscreenElement
			) {
				// we are already full screen!
				// so exit full-screen
				if (document.exitFullscreen) {
					document.exitFullscreen();
				} else if (document.webkitExitFullscreen) {
					document.webkitExitFullscreen();
				} else if (document.mozCancelFullScreen) {
					document.mozCancelFullScreen();
				} else if (document.msExitFullscreen) {
					document.msExitFullscreen();
				}
			} else {
				// go full screen!
				var i = document.getElementById("GameBox");
 
				if (i.requestFullscreen) {
					i.requestFullscreen();
				} else if (i.webkitRequestFullscreen) {
					i.webkitRequestFullscreen();
				} else if (i.mozRequestFullScreen) {
					i.mozRequestFullScreen();
				} else if (i.msRequestFullscreen) {
					i.msRequestFullscreen();
				}				
			}
		}
	}
	
	if (actualkey == inputobj.lookaround)
	{
		if (gamestatus != "lookaround")
		{
			gamestatus = "lookaround";
			alert("Lookaround mode");
		} else {
			gamestatus = "playing";
		}
	}

	player_took_turn = false;
	movedbool = true;

	if (gamestatus == "lookaround")
	{
		if (actualkey == inputobj.move_up)
		{
			lookaroundoffsety = lookaroundoffsety - 1;
		} else if (actualkey == inputobj.move_down) {
			lookaroundoffsety = lookaroundoffsety + 1;
		} else if (actualkey == inputobj.move_left) {
			lookaroundoffsetx = lookaroundoffsetx - 1;
		} else if (actualkey == inputobj.move_right) {
			lookaroundoffsetx = lookaroundoffsetx + 1;
		}
		return;
	} else {
		lookaroundoffsetx = 0;
		lookaroundoffsety = 0;	
	}

	if (gamestatus != "playing")
	{
		return;
	}

	if (getobjectreference("player").dead == true)
	{
		return;
	}

	if (getobjectreference("player").contents.length > 0)
	{
		// player has swalloed something?
		for (var i = 0; i < getobjectreference("player").contents.length; i++)
		{
			getobjectreference("player").attack(getobjectreference("player").contents[i], undefined);
			player_took_turn = true;
		}
	}
	
	if (getobjectreference("player").swallowed == true || getobjectreference("player").canmove == false || getobjectreference("player").trapped == true)
	{
		//log("Player is swallowed by: " + getobjectreference("player").swallower.name);
		if (actualkey == inputobj.move_up || actualkey == inputobj.move_down || actualkey == inputobj.move_left || actualkey == inputobj.move_right)
		{
			getobjectreference("player").attack(getobjectreference("player").swallower);
			player_took_turn = true;
		} else if (actualkey == inputobj.pleasure)
		{
			getobjectreference("player").pleasureactor(getobjectreference("player").swallower);
			player_took_turn = true;
		} else if (actualkey == inputobj.skipturn) {
			player_took_turn = true;
		} else if (actualkey == "space")
		{
			if (getobjectreference("player").swallower.ismyfriend(getobjectreference("player")) == true)
			{
				getobjectreference("player").swallower.release(getobjectreference("player"));
			}
		}
	} else {
		if (actualkey == inputobj.move_up)
		{
			movedbool = getobjectreference("player").move(0, -1);
			player_took_turn = true;
		} else if (actualkey == inputobj.move_down) {
			movedbool = getobjectreference("player").move(0, 1);
			player_took_turn = true;
		} else if (actualkey == inputobj.move_left) {
			movedbool = getobjectreference("player").move(-1, 0);
			player_took_turn = true;
		} else if (actualkey == inputobj.move_right) {
			movedbool = getobjectreference("player").move(1, 0);
			player_took_turn = true;
		} else if (actualkey == inputobj.pleasure) {
			getobjectreference("player").rest();
			player_took_turn = true;
		} else if (actualkey == inputobj.skipturn) {
			player_took_turn = true;			
		} else if (actualkey == "space") {
			//log(getobjectat(getobjectreference("player").x, getobjectreference("player").y).name);

			var theobject = getobjectat(getobjectreference("player").x, getobjectreference("player").y);

			if (theobject != false)
			{
				currentchoice = theobject;
				theobject.textchoices = theobject.choices.join("|");
				theobject.allowcancel = (theobject.allowcancel !== undefined)?theobject.allowcancel:true;
				choicebox(theobject);
			}
		} else if (actualkey == inputobj.open_inventory)
		{
			getobjectreference("player").openinventory(getobjectreference("player"));
		} else if (actualkey == inputobj.zoomin)
		{
			increaseenginezoom();
		} else if (actualkey == inputobj.zoomout)
		{
			decreaseenginezoom();
		}
	}
}