/*

Vorelike Copyright (c) 2013-2014 RogueLikeFox

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

var TILE_SIZE_ASCII = 16;
var TILE_SIZE_GPHS = 32;
var HEIGHT_OFF = 0;
var WIDTH_OFF = 0;
var TILE_SIZE = 0;

var FONT_SIZE_ASCII = 16;
var FONT_SIZE_MESSAGEBOX = 16;
var FONT_SIZE_TEXTCONSOLE = 16;

engine.screenx = 0;
engine.screeny = 0;

engine.zoomlevel = 0;

engine.doonce;

var fpstimer = 1;
var prevfps = 1;
var timeprev = new Date().getTime();
var timenow = new Date().getTime();

engine.lookaroundoffsetx = 0;
engine.lookaroundoffsetx = 0;

var imagesrcs = new Array();

engine.tileoffsetx;
engine.tileoffsety;		

engine.useshadows = true;

engine.worldtimeinterval;
engine.worldtimetoggle;

engine.layertotal = 5;
engine.layerset = 0;

var off = document.createElement("canvas");
off.id = "offcanvas";

function increaseenginezoom()
{
	engine.zoomlevel = engine.zoomlevel + 1;

	if (engine.zoomlevel > 3)
	{
		engine.zoomlevel = 3;
	}
}

function decreaseenginezoom()
{
	engine.zoomlevel = engine.zoomlevel - 1;

	if (engine.zoomlevel <= 0)
	{
		engine.zoomlevel = 0;
	}
}

function resizecanvas()
{
	var mapcanvas = document.getElementById("MapCanvas");
	mapcanvas.width = GetPageWidth()*.80;
	mapcanvas.height = GetPageHeight()*.75;

	var statscanvas = document.getElementById("StatsCanvas");
	statscanvas.width = GetPageWidth()*.17;
	statscanvas.height = GetPageHeight()*.75;

	var textcanvas = document.getElementById("TextCanvas");
	textcanvas.width = GetPageWidth()*0.97;
	textcanvas.height = GetPageHeight()*.20;
	textcanvas.style = "border:1px solid #000000; z-index:0; box-sizing: border-box; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; width:" + textcanvas.width + "px; height:" + textcanvas.height + "px; font-size:" + FONT_SIZE_TEXTCONSOLE + "pt;";	
	
	if (currentelement != undefined)
	{
		currentelement.resize();
	}
}

function printcanvas()
{
	var contentdiv = document.createElement("div");
	var mapcanvas = document.createElement("canvas");
	var statscanvas = document.createElement("canvas");
	var textcanvas = document.createElement("textarea");
	
	contentdiv.id = "GameBox";
	contentdiv.width = "98%";
	contentdiv.height = "98%";
	contentdiv.style = "background: #000000; z-index:1;";
	
	mapcanvas.id = "MapCanvas";
	mapcanvas.tabIndex = 1;
	mapcanvas.width = GetPageWidth()*.80;
	mapcanvas.height = GetPageHeight()*.75;
	mapcanvas.style = 'border:1px solid #000000; z-index:1; background: #000000';
	
	statscanvas.id = "StatsCanvas";
	statscanvas.tabIndex = 2;	
	statscanvas.width = GetPageWidth()*.17;
	statscanvas.height = GetPageHeight()*.75;
	statscanvas.style = 'border:1px solid #000000; z-index:1;';

	textcanvas.id = "TextCanvas";
	textcanvas.tabIndex = 3;
	textcanvas.readOnly = true;
	textcanvas.width = GetPageWidth()*0.97;
	textcanvas.height = GetPageHeight()*0.20;
	textcanvas.style.width = GetPageWidth() * 0.97 + "px";
	textcanvas.style.height = GetPageHeight() * 0.20 + "px";	
	textcanvas.style = "border:1px solid #000000; z-index:1; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; width:" + textcanvas.width + "px; height:" + textcanvas.height + "px; font-size:" + FONT_SIZE_TEXTCONSOLE + "pt;";
	
	document.body.appendChild(contentdiv);
	
	document.getElementById("GameBox").appendChild(mapcanvas);
	document.getElementById("GameBox").appendChild(statscanvas);
	document.getElementById("GameBox").appendChild(textcanvas);
	
	/*
	document.write("<canvas id='MapCanvas' width=" + GetPageWidth()*.80 + " height=" + GetPageHeight()*.75 + " style='border:1px solid #000000; z-index:0; background: #000000'></canvas>" +
	"<canvas id='StatsCanvas' width=" + GetPageWidth()*.15 + " height=" + GetPageHeight()*.75 + " style='border:1px solid #000000; z-index:0'></canvas>" +
	"<br>" +
	"<textarea readonly id='TextCanvas' style='border:1px solid #000000; z-index:0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; width:" + GetPageWidth()*0.95 + "px; height:" + GetPageHeight()*.20 + "px; font-size:" + FONT_SIZE_TEXTCONSOLE + "pt;'></textarea>");
	*/
}

function clearcanvas(canvas)
{
	var ctx = canvas.getContext("2d");
	// Store the current transformation matrix
	//ctx.save();

	// Use the identity matrix while clearing the canvas
	//ctx.setTransform(1, 0, 0, 1, 0, 0);

	ctx.clearRect(0, 0, canvas.width, canvas.height);

	// Restore the transform
	//ctx.restore();
}

function renderLoop(time)
{
	// Put another iteration of renderLoop in the animation queue
	window.requestAnimationFrame(renderLoop);
 
	// Actually calculate and draw a frame
	renderall({handleactors: false, time: time});
}

function bigredresetbutton()
{
	// this resets everything, except for loaded mods.

	if (customseed == false)
	{
		gamerandomseed = Math.seedrandom(new Date().getTime());
	} else {
		gamerandomseed = Math.seedrandom(customseed);
	}

	defaultplayer.gender = getobjectreference("player").gender;
	
	var theplayer = defaultplayer;
	
	actors = new Array();

	actors.push(theplayer);
	
	getobjectreference("player").hp = getobjectreference("player").totalhp;
	getobjectreference("player").dead = false;
	getobjectreference("player").status = "Normal";
	getobjectreference("player").powerlevel = 1;
	getobjectreference("player").swallowed = false;
	getobjectreference("player").trapped = false;	
	getobjectreference("player").canmove = true;	
	getobjectreference("player").swallower = false;
	getobjectreference("player").friends = new Array();
	
	
	getobjectreference("player").prev_x = 0;
	getobjectreference("player").prev_y = 0;		
	
	objects = new Array();

	allfloors = new Array();

	lastusedobjectindex = getobjectreference("player").objectindex;
	currentlevel = 0;
	getobjectreference("player").level = currentlevel;
	currentscene = ""; // current textscene
	currentchoice = ""; 

	//gamestatus = "modload";

	//createnotification("Please drag and drop the mods you wish to use here or simply click the mouse to load defaults.\n\nControls:\nWASD - Move / struggle / select choices\nR - Pleasure enemy (while in stomach)\nSpacebar - Activate items / release from friendly stomach\nReturn - Select option in menu\nV - Open inventory\nM - Toggle sounds");

	var startfloor = new floor(0, 100, 100, gamerandomseed);

	// and add stuff to the floor.
	populatecurrentfloor(gamerandomseed);

	gamestatus = "playing";

	printouttext("TextCanvas", true, "Using seed: " + gamerandomseed);
	printouttext("TextCanvas", false, "You find yourself in a dungeon. The walls smell of mildew.");	
}

function clearmap() 
{
	var c = document.getElementById("MapCanvas");
	var ctx = c.getContext("2d");
	ctx.font = FONT_SIZE_ASCII + "px Arial";

	//log("Clearmap");
	renderall({handleactors: false});
}

function worldtime(input)
{
	if (input == true)
	{
		if (engine.worldspeed > 0 && player_took_turn == false)
		{
			// update actors according to worldspeed
			engine.worldtimeinterval = setInterval(handle_actors, engine.worldspeed);
		}
		engine.worldtimetoggle = true;
	} else {
		clearInterval(engine.worldtimeinterval);
		engine.worldtimetoggle = false;		
	}
}

function renderall(input)
{
	var i = 0;
	var j = 0;
	
	if (gamestatus != "playing" && gamestatus != "lookaround" && gamestatus != "interfaceui")
	{
		return;
	}

	//log("Player x: " + player.x + ", player.y: " + player.y);

	if (viewmode == 0)
	{
		TILE_SIZE = Math.round(TILE_SIZE_ASCII + engine.zoomlevel*TILE_SIZE_ASCII);
	} else {
		TILE_SIZE = Math.round(TILE_SIZE_GPHS + engine.zoomlevel*TILE_SIZE_GPHS);
	}

	//engine.screenx = Math.round(getobjectreference("player").x - Math.round((centermapcanvasx()-TILE_SIZE) / TILE_SIZE));
	//engine.screeny = Math.round(getobjectreference("player").y - Math.round((centermapcanvasy()-TILE_SIZE) / TILE_SIZE));

	engine.screenx = Math.round(getobjectreference("player").x - (centermapcanvasx()/TILE_SIZE));
	engine.screeny = Math.round(getobjectreference("player").y - (centermapcanvasy()/TILE_SIZE));
	
	engine.screenx = engine.screenx + engine.lookaroundoffsetx;
	engine.screeny = engine.screeny + engine.lookaroundoffsetx;
	
	if (engine.screenx < 0)
	{
		engine.screenx = 0;
		if (engine.lookaroundoffsetx != 0)
		{
			engine.lookaroundoffsetx = engine.lookaroundoffsetx + 1;
		}
	}
	if (engine.screeny < 0)
	{
		engine.screeny = 0;
		if (engine.lookaroundoffsetx != 0)
		{
			engine.lookaroundoffsetx = engine.lookaroundoffsetx + 1;
		}		
	}

	var screenwidth = 0;
	var screenheight = 0;

	screenwidth = Math.round(getobjectreference("player").x + (centermapcanvasx()/TILE_SIZE));
	screenheight = Math.round(getobjectreference("player").y + (centermapcanvasy()/TILE_SIZE));

	screenwidth = screenwidth + engine.lookaroundoffsetx;
	screenheight = screenheight + engine.lookaroundoffsetx;
	
	if (screenwidth < ((centermapcanvasx()*2)/TILE_SIZE))
	{
		screenwidth = Math.round((centermapcanvasx()*2)/TILE_SIZE);
	}
	if (screenheight < ((centermapcanvasy()*2)/TILE_SIZE))
	{
		screenheight = Math.round((centermapcanvasy()*2)/TILE_SIZE);
	}
	
	if (screenwidth >= allfloors[currentlevel].tiles.length)
	{
		screenwidth = ((allfloors[currentlevel].tiles.length)-1);
		if (engine.lookaroundoffsetx != 0)
		{
			engine.lookaroundoffsetx = engine.lookaroundoffsetx - 1;
			return;
		}
	}
	if (screenheight >= allfloors[currentlevel].tiles.length)
	{
		screenheight = ((allfloors[currentlevel].tiles.length)-1);
		if (engine.lookaroundoffsetx != 0)
		{
			engine.lookaroundoffsetx = engine.lookaroundoffsetx - 1;
			return;
		}		
	}
	
	var screenoffx = 0;
	var screenoffy = 0;

	screenoffx = Math.round(engine.screenx * TILE_SIZE);
	screenoffy = Math.round(engine.screeny * TILE_SIZE);

	//var off = document.createElement('canvas'); -- see above
	off.width = document.getElementById("MapCanvas").width;
	off.height = document.getElementById("MapCanvas").height;

	var ctx = off.getContext('2d');

	ctx.font = FONT_SIZE_ASCII + "px Arial";

	clearcanvas(off);
	
	//printouttext("TextCanvas", true, engine.screenx + ", " + engine.screeny + ", " + screenwidth + ", " + screenheight + ": " + allfloors[currentlevel].size);
	
	//ctx.fillStyle = "#000000";
	var output = new Object()
	output.canvasname = "StatsCanvas";
	output.clear = true;
	output.intext = getobjectreference("player").name + "\n" + "HP: " + getobjectreference("player").hp + "/" + getobjectreference("player").totalhp + "\nStatus: " + getobjectreference("player").status + "\nPower level: " + getobjectreference("player").powerlevel + "\nDungeon level: " + (getobjectreference("player").level+1);
	
	printoutcanvastext(output);

	if (input.handleactors == true || player_took_turn == true)
	{
		player_took_turn = false;
		handle_actors();
	}
	
	var prevstyle;

	var totaltiles = 0;

	var oneimg;

	var thetexture;	
	var texturereferenceid;
	
	var floortime;
	var floortimeprev;
	
	var objecttime;
	var objecttimeprev;
	
	var actortime;
	var actortimeprev;
	
	floortimeprev = new Date().getTime();
	
	engine.layertotal = 5;
	if (allfloors[currentlevel].tmx == true)
	{
		engine.layertotal = (allfloors[currentlevel].layers-1);
	}
	
	for (engine.layerset = 0; engine.layerset <= engine.layertotal; engine.layerset++)
	{
	
		try {

		for (i = engine.screenx; i <= screenwidth; i++)
		{
			for (j = engine.screeny; j <= screenheight; j++)
			{
				if (allfloors[currentlevel].tiles[i] == undefined || allfloors[currentlevel].tiles[i][j] == undefined || (allfloors[currentlevel].tiles[i][j].layer != engine.layerset && allfloors[currentlevel].tmx != true))
				{
					continue;
				}		
				if (viewmode == 0)
				{
					document.getElementById("MapCanvas").style.backgroundColor = "#FFFFFF";
					if (allfloors[currentlevel].tiles[i][j].char != "" && allfloors[currentlevel].tiles[i][j].type == 1 && (allfloors[currentlevel].tiles[i][j].discovered == true || engine.useshadows == false))
					{
						ctx.textAlign = 'center';

						if (prevstyle != allfloors[currentlevel].tiles[i][j].color)
						{
							ctx.fillStyle = allfloors[currentlevel].tiles[i][j].color;
							prevstyle = allfloors[currentlevel].tiles[i][j].color;
						}
		
						ctx.fillText(allfloors[currentlevel].tiles[i][j].char,(WIDTH_OFF + i*TILE_SIZE)-screenoffx,(HEIGHT_OFF + j*TILE_SIZE)-screenoffy)
					}
				} else {
					document.getElementById("MapCanvas").style.backgroundColor = "#000000";
					if (allfloors[currentlevel].tiles[i][j] != undefined)
					{
						thetexture = getobjectreference(allfloors[currentlevel].tiles[i][j].texture);
						
						if (allfloors[currentlevel].tmx == true)
						{
							if (allfloors[currentlevel].tiles[i][j].layers[engine.layerset] == undefined)
							{
								continue;
							}
							thetexture = getobjectreference(allfloors[currentlevel].tiles[i][j].layers[engine.layerset].texture);					
						}
						
						if (thetexture.referenceid != undefined)
						{
							if (imagesrcs[thetexture.display()] == undefined)
							{
								oneimg = new Image();
								oneimg.src = thetexture.display();
								imagesrcs[thetexture.display()] = oneimg;
							} else {
								thetexture.display();
								oneimg = imagesrcs[thetexture.display()];
							}
						} else {
							continue;
						}						
						
						// we have a texture						
						
						if (oneimg == null)
						{
							continue;
						}
						
						if (oneimg.src != "" && oneimg.src != undefined)
						{
							if (thetexture.imagewidth == 0 || thetexture.imageheight == 0)
							{
								if (oneimg.width != 0 && oneimg.height != 0)
								{
									thetexture.imagewidth = oneimg.width;
									thetexture.imageheight = oneimg.height;
								}
							}
							
							//alert(allfloors[currentlevel].tiles[i][j].gid);

							ctx.drawImage(oneimg, thetexture.clipx, thetexture.clipy, thetexture.clipwidth, thetexture.clipheight, ((i*TILE_SIZE)-screenoffx), ((j*TILE_SIZE)-screenoffy), Math.round(thetexture.canvaswidth + (thetexture.canvaswidth * engine.zoomlevel)), Math.round(thetexture.canvasheight + (thetexture.canvasheight * engine.zoomlevel)));
							//  + ((thetexture.canvasheight * engine.zoomlevel) / thetexture.clipheight)
						} else {
							//alerrt(oneimg.src);
							if (prevstyle != allfloors[currentlevel].tiles[i][j].color)
							{
								ctx.fillStyle = allfloors[currentlevel].tiles[i][j].color;
								prevstyle = allfloors[currentlevel].tiles[i][j].color;
							}
							ctx.fillRect( (WIDTH_OFF + i*TILE_SIZE)-screenoffx, (HEIGHT_OFF + j*TILE_SIZE)-screenoffy, TILE_SIZE, TILE_SIZE);
						}
						oneimg = null;
						thetexture = null;
					} else {
						if (prevstyle != allfloors[currentlevel].tiles[i][j].color)
						{
							//ctx.fillStyle = allfloors[currentlevel].tiles[i][j].color;
							//prevstyle = allfloors[currentlevel].tiles[i][j].color;
						}
						//ctx.fillRect( (WIDTH_OFF + i*TILE_SIZE)-screenoffx, (HEIGHT_OFF + j*TILE_SIZE)-screenoffy, TILE_SIZE, TILE_SIZE);
					}
				}
				totaltiles = totaltiles + 1;
			}		
		}

		}
		catch (err) {
			if (debuglevel >= 1)
			{
				alert(err + ", i: " + i + ", j: " + j);
			}
			alert(err + ", i: " + i + ", j: " + j + "\nengine.layerset: " + engine.layerset);			
		}

		floortime = new Date().getTime();	
		
		floortime = floortime - floortimeprev;
		
		objecttimeprev = new Date().getTime();
		
		for (i = 0; i < allfloors[currentlevel].objects.length; i++)
		{
			var object = allfloors[currentlevel].objects[i];

			if (object.level != currentlevel || object.isbaseobject == true || object.render == false)
			{
				continue;
			}

			if (object.isactor == true || object.isbaseobject == true || object.layer != engine.layerset)
			{
				continue;
			}

			if (viewmode == 0)
			{
				if (allfloors[currentlevel].tiles[object.x][object.y].char == " " || allfloors[currentlevel].tiles[object.x][object.y].char == object.char && (allfloors[currentlevel].tiles[object.x][object.y].discovered == true || engine.useshadows == false))
				{
					allfloors[currentlevel].tiles[object.x][object.y].char = object.char;
					allfloors[currentlevel].tiles[object.x][object.y].blocked = object.blocks;

					ctx.fillStyle = object.color;
					ctx.textAlign = 'center';

					ctx.fillText(object.char,(WIDTH_OFF + object.x*TILE_SIZE)-screenoffx,(HEIGHT_OFF + object.y*TILE_SIZE)-screenoffy);
				}
			} else {
				thetexture = getobjectreference(object.texture);
				texturereferenceid = getobjectreference(object.texture).referenceid;

				if (texturereferenceid != undefined)
				{
					if (imagesrcs[getobjectreference(object.texture).display()] == undefined)
					{
						oneimg = new Image();
						oneimg.src = getobjectreference(object.texture).display();
						imagesrcs[getobjectreference(object.texture).display()] = oneimg;
					} else {
						getobjectreference(object.texture).display();
						oneimg = imagesrcs[getobjectreference(object.texture).display()];
					}
				}

				// we have a texture

				//allfloors[currentlevel].tiles[object.x][object.y].char = object.char;
				//allfloors[currentlevel].tiles[object.x][object.y].blocked = object.blocks;

				engine.tileoffsetx = 0;
				engine.tileoffsety = 0;			
				
				var canvaswidth = thetexture.canvaswidth;
				var canvasheight = thetexture.canvasheight;			
				
				if (oneimg.src != "" && oneimg.src != undefined)
				{

					if (thetexture.scale == true)
					{
						if (thetexture.canvaswidth > thetexture.clipwidth || thetexture.canvasheight > thetexture.clipheight)
						{
							if (thetexture.canvaswidth > thetexture.clipwidth)
							{
								engine.tileoffsetx = ((TILE_SIZE/2)-(thetexture.clipwidth/2));	
								canvaswidth = Number(thetexture.clipwidth);
							}

							if (thetexture.canvasheight > thetexture.clipheight)
							{
								engine.tileoffsety = ((TILE_SIZE/2)-(thetexture.clipheight/2));							
								canvasheight = Number(thetexture.clipheight);
							}
						}
					}
					if (allfloors[currentlevel].tiles[object.x][object.y].playernear == true || allfloors[currentlevel].tiles[object.x][object.y].lightlevel <= 88 || engine.useshadows == false)
					{
						if (thetexture.imagewidth == 0 || thetexture.imageheight == 0)
						{
							if (oneimg.width != 0 && oneimg.height != 0)
							{
								thetexture.imagewidth = oneimg.width;
								thetexture.imageheight = oneimg.height;
							}
						}
						ctx.drawImage(oneimg, thetexture.clipx, thetexture.clipy, thetexture.clipwidth, thetexture.clipheight, (object.x*TILE_SIZE)-screenoffx+engine.tileoffsetx, (object.y*TILE_SIZE)-screenoffy+engine.tileoffsety, Math.round(canvaswidth + (canvaswidth * engine.zoomlevel)), Math.round(canvasheight + (canvasheight * engine.zoomlevel)));
					}
				} else {
					ctx.fillStyle = object.color;
					ctx.fillRect( (WIDTH_OFF + object.x*TILE_SIZE)-screenoffx, (HEIGHT_OFF + object.y*TILE_SIZE)-screenoffy, TILE_SIZE, TILE_SIZE);
				}
				oneimg = null;
				thetexture = null;						
			}
		}

		objecttime = new Date().getTime();	
		
		objecttime = objecttime - objecttimeprev;
		
		var playerlightx = 0;
		var playerlighty = 0
		
		actortimeprev = new Date().getTime();	
		
		for (i = 0; i < allfloors[currentlevel].actors.length; i++)
		{
			var object = allfloors[currentlevel].actors[i];

			if (object.level != currentlevel || object.isbaseobject == true || object.render == false || object.layer != engine.layerset)
			{
				continue;
			}

			if (object.isactor == true)
			{
				if (object.dead == true || object.swallowed == true)
				{
					continue;
				}
			}

			allfloors[currentlevel].tiles[object.x][object.y].char = object.char;
			allfloors[currentlevel].tiles[object.x][object.y].blocked = object.blocks;

			if (viewmode == 0)
			{
				if (allfloors[currentlevel].tiles[object.x][object.y].discovered == true || engine.useshadows == false)
				{
					ctx.fillStyle = object.color;
					ctx.textAlign = 'center';
					ctx.fillText(object.char,(WIDTH_OFF + object.x*TILE_SIZE)-screenoffx,(HEIGHT_OFF + object.y*TILE_SIZE)-screenoffy);
				}
			} else {
				thetexture = null;
				
				thetexture = getobjectreference(object.texture);
				texturereferenceid = getobjectreference(object.texture).referenceid;
				
				if (texturereferenceid != undefined)
				{
					if (imagesrcs[getobjectreference(object.texture).display()] == undefined)
					{
						oneimg = new Image();
						oneimg.src = getobjectreference(object.texture).display();
						imagesrcs[getobjectreference(object.texture).display()] = oneimg;
					} else {
						getobjectreference(object.texture).display();				
						oneimg = imagesrcs[getobjectreference(object.texture).display()];
					}
				} 

				//printouttext("TextCanvas", true, "Object.texture: " + getobjectreference(object.texture).referenceid + ", Oneimg: " + oneimg + ", Oneimg.src: " + oneimg.src + " 3");			
				
				// we have a texture
				
				engine.tileoffsetx = 0;
				engine.tileoffsety = 0;			
				
				var canvaswidth = thetexture.canvaswidth * object.tilesizex;
				var canvasheight = thetexture.canvasheight * object.tilesizey;
				
				if (oneimg.src != "" && oneimg.src != undefined && oneimg != undefined)
				{
					// image, clipx, clipy, clipwidth, clipheight, canvasx, canvasy, canvaswidth, canvasheight
					
					if (thetexture.scale == true)
					{
						if (thetexture.canvaswidth > thetexture.clipwidth || thetexture.canvasheight > thetexture.clipheight)
						{
							if (thetexture.canvaswidth > thetexture.clipwidth)
							{
								engine.tileoffsetx = ((TILE_SIZE/2)-(thetexture.clipwidth/2));	
								canvaswidth = Number(thetexture.clipwidth);
							}

							if (thetexture.canvasheight > thetexture.clipheight)
							{
								engine.tileoffsety = ((TILE_SIZE/2)-(thetexture.clipheight/2));
								canvasheight = Number(thetexture.clipheight);
							}						
						}					
					}
					try {
						if (allfloors[currentlevel].tiles[object.x][object.y].playernear == true || allfloors[currentlevel].tiles[object.x][object.y].lightlevel <= 88 || engine.useshadows == false)
						{
							if (thetexture.imagewidth == 0 || thetexture.imageheight == 0)
							{
								if (oneimg.width != 0 && oneimg.height != 0)
								{
									thetexture.imagewidth = oneimg.width;
									thetexture.imageheight = oneimg.height;
									if (thetexture.clipwidth == 0 || thetexture.clipheight == 0)
									{
										thetexture.clipwidth = thetexture.imagewidth;
										thetexture.clipheight = thetexture.imageheight;
									}
								}
							}
							ctx.drawImage(oneimg, thetexture.clipx, thetexture.clipy, thetexture.clipwidth, thetexture.clipheight, ((object.x*TILE_SIZE)-screenoffx+engine.tileoffsetx), ((object.y*TILE_SIZE)-screenoffy+engine.tileoffsety), Math.round(canvaswidth + (canvaswidth * engine.zoomlevel)), Math.round(canvasheight + (canvasheight * engine.zoomlevel)));				
						}
					}
					catch(err)
					{
						viewmode = 0;
						alert(err + "\n" + "Oneimg: " + oneimg + "\nOneimg.src: " + oneimg.src + ", Refid: " + thetexture.referenceid + ", files: " + thetexture.texturefiles + "\n" + thetexture.clipy + ", " + thetexture.clipy + ", " + thetexture.clipwidth + ", " + thetexture.clipheight);
					}
				} else {
					ctx.fillStyle = object.color;
					ctx.fillRect( (WIDTH_OFF + object.x*TILE_SIZE)-screenoffx+engine.tileoffsetx, (HEIGHT_OFF + object.y*TILE_SIZE)-screenoffy+engine.tileoffsety, TILE_SIZE, TILE_SIZE);
				}		
				oneimg = null;
				thetexture = null;			
			}
		}
		
		actortime = new Date().getTime();
		
		actortime = actortime - actortimeprev;	
	}
	
	// lighting...
	
	prevstyle = "";
	
	if (engine.useshadows)
	{
		for (i = engine.screenx; i <= screenwidth; i++)
		{
			for (j = engine.screeny; j <= screenheight; j++)
			{
				if (allfloors[currentlevel].tiles[i][j] == undefined)
				{
					continue;
				}
				if (viewmode != 0)
				{
					if (get_distance(getobjectreference("player"), allfloors[currentlevel].tiles[i][j]).distance > 5)
					{
						allfloors[currentlevel].tiles[i][j].playernear = false;
					}
				
					if (allfloors[currentlevel].tiles[i][j].discovered == true && allfloors[currentlevel].tiles[i][j].playernear == false && allfloors[currentlevel].tiles[i][j].lightlevel <= 89)
					{
						allfloors[currentlevel].tiles[i][j].lightlevel = Math.round(allfloors[currentlevel].tiles[i][j].lightlevel + 1);
					}
					
					ctx.save();
						if (allfloors[currentlevel].tiles[i][j].discovered == false && allfloors[currentlevel].tiles[i][j].playernear == false)
						{
							ctx.fillStyle = "rgba(0, 0, 0, 1.0)";
						} else {
							ctx.fillStyle = "rgba(0, 0, 0, " + (allfloors[currentlevel].tiles[i][j].lightlevel * 0.01) + ")";
						}
					ctx.fillRect( (WIDTH_OFF + i*TILE_SIZE)-screenoffx, (HEIGHT_OFF + j*TILE_SIZE)-screenoffy, TILE_SIZE, TILE_SIZE);
					ctx.restore();
					
				}
			}		
		}
	}
	
	// and now we render to the real canvas...

	var realcanvas = document.getElementById("MapCanvas");
	var ctx2 = realcanvas.getContext('2d');

	clearcanvas(realcanvas);

	// img, sx, sy, swidth, sheight, x, y, width, height
	// img, x, y
	
	ctx2.drawImage(off, 0, 0, realcanvas.width, realcanvas.height);
	
	// interface time!
	
	if (gamestatus == "interfaceui")
	{
		// draw background...
		var interface = document.getElementById(currentelement.mycanvas.id).getContext("2d");
		
		// opacity
		if (document.getElementById(currentelement.mycanvas.id).style.opacity <= 1.0)
		{
			document.getElementById(currentelement.mycanvas.id).style.opacity = Number(document.getElementById(currentelement.mycanvas.id).style.opacity) + (1/(1*60));
		}
		
		clearcanvas(document.getElementById(currentelement.mycanvas.id));
		interface.fillStyle = currentelement.background;
		interface.fillRect(currentelement.x, currentelement.y, currentelement.width, currentelement.height);
		for(var x = 0; x < currentelement.elements.length; x++)
		{
			currentelement.elements[x].draw();
		}
	}	
	
	timenow = input.time;

	fpstimer += Math.round(1000/(timenow-timeprev))?Math.round(1000/(timenow-timeprev)):1;
	
	if (fpstimer >= 1000)
	{
		fpstimer = 0;
		var output = new Object();
		output.canvasname = "StatsCanvas";
		output.clear = false;
		output.intext = "\n" + Math.round(1000/(timenow-timeprev)) + " FPS";
		printoutcanvastext(output);
		prevfps = Math.round(1000/(timenow-timeprev));
	} else {
		var output = new Object();
		output.canvasname = "StatsCanvas";
		output.clear = false;
		output.intext = "\n" + prevfps + " FPS";
		printoutcanvastext(output);	
	}
	
	timeprev = timenow;
}