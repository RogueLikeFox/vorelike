/*

Vorelike Copyright (c) 2013-2014 RogueLikeFox

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

/*

This file contains floor creation code.

*/

var ROOM_MAX_SIZE = 10;
var ROOM_MIN_SIZE = 5;

function tile(input)
{
	// this is a tile
	// walkable is obvious
	// blocked means the tile cannot be seen past
	// type is "bedrock" (0), wall (1), floor (2), etc
	// char is character for ASCII

	this.walkable = input.walkable;
	this.blocked = input.blocked;
	this.texture = input.texture;
	this.type = input.type;
	this.char = input.char;
	this.color = input.color;
	this.prevchar = input.char;

	this.discovered = false;
	
	this.playernear = false;
	
	this.x = input.x;
	this.y = input.y;
	this.layer = input.layer ? input.layer : 0;

	this.threex = input.threex ? input.threex : input.x;
	this.threey = input.threey ? input.threey : input.y;	
	this.threez = input.threez ? input.three : 0;	
	this.threew = input.threew ? input.threew : 1;
	this.threeh = input.threeh ? input.threeh : 1;	
	this.threel = input.threel ? input.threel : 1;		

	this.coord = this.x+":"+this.y;
	
	this.lightlevel = 90;
	
	for (i in input)
	{
		this[i] = input[i];
	}	
	
	return this;
}

tile.prototype.neighbours = function(goal) 
{
	var n = [];
		
	var allowDiagonals = false;

	var dir = [[0,1],[0,-1],[1,0],[-1,0], [1,1],[-1,1],[1,-1],[-1,-1]];

	for (var i=0; i < (allowDiagonals?8:4); ++i) 
	{
		if ((this.x+dir[i][0] < 0 || this.x+dir[i][0] > 19) || (this.y+dir[i][1] < 0 || this.y+dir[i][1] > 19))
		{
			//continue;
		}
			
		var p = allfloors[currentlevel].tiles[this.x+dir[i][0]][this.y+dir[i][1]];
			
		if (p.walkable == false && p != goal) 
		{
			continue;
		}
			
		n.push(p);
	}
	return n;
};

tile.prototype.makepathneighbours = function(goal) 
{
	var n = [];
		
	var allowDiagonals = false;

	var dir = [[0,1],[0,-1],[1,0],[-1,0], [1,1],[-1,1],[1,-1],[-1,-1]];

	for (var i=0; i < (allowDiagonals?8:4); ++i) 
	{
		if ((this.x+dir[i][0] < 0 || this.x+dir[i][0] > 19) || (this.y+dir[i][1] < 0 || this.y+dir[i][1] > 19))
		{
			//continue;
		}
			
		var p = allfloors[currentlevel].tiles[this.x+dir[i][0]][this.y+dir[i][1]];
		
		if (p.walkable == false && p != goal) 
		{			
			allfloors[currentlevel].tiles[this.x+dir[i][0]][this.y+dir[i][1]].walkable = true;
			allfloors[currentlevel].tiles[this.x+dir[i][0]][this.y+dir[i][1]].blocked = false;
			allfloors[currentlevel].tiles[this.x+dir[i][0]][this.y+dir[i][1]].texture = "floor";
			allfloors[currentlevel].tiles[this.x+dir[i][0]][this.y+dir[i][1]].char = " ";
			allfloors[currentlevel].tiles[this.x+dir[i][0]][this.y+dir[i][1]].type = 2;
			allfloors[currentlevel].tiles[this.x+dir[i][0]][this.y+dir[i][1]].layer = 0;
			allfloors[currentlevel].tiles[this.x+dir[i][0]][this.y+dir[i][1]].color = "#FFFFFF";			
		}
			
		n.push(p);
	}
	return n;
};

function discover_tiles(tilex, tiley, distance)
{
	// generally will be player's X and Y
	
	var x = 0, y = 0;
	
	var start_lights = false;
	
	if (allfloors[currentlevel].tiles[getobjectreference("player").x][getobjectreference("player").y].discovered == false || allfloors[currentlevel].tiles[getobjectreference("player").x][getobjectreference("player").y].playernear == false)
	{
		// startup...
		start_lights = true;
	}
	
	for ((x = tilex-distance); (x <= tilex+distance); x++)
	{
		for ((y = tiley-distance); (y <= tiley+distance); y++)
		{	
			if (allfloors[currentlevel].tiles[x] == undefined || allfloors[currentlevel].tiles[x][y] == undefined)
			{
				continue;
			}
			if (allfloors[currentlevel].tiles[x][y].discovered == false)
			{
				allfloors[currentlevel].tiles[x][y].lightlevel = 90;					
			}
		
			if (get_distance(getobjectreference("player"), allfloors[currentlevel].tiles[x][y]).realdistance <= distance)
			{
				if (allfloors[currentlevel].tiles[x][y].playernear == true || start_lights == true)
				{
					allfloors[currentlevel].tiles[x][y].lightlevel = 10 * (get_distance(getobjectreference("player"), allfloors[currentlevel].tiles[x][y]).realdistance);
				}
				allfloors[currentlevel].tiles[x][y].playernear = true;
			}		
			allfloors[currentlevel].tiles[x][y].discovered = true;						
		}
	}
}

function rectangle(x, y, w, h)
{
	this.x1 = x;
	this.y1 = y;
	this.x2 = x + w;
	this.y2 = y + h;
	this.w = w;
	this.h = h;

	return this;
}

rectangle.prototype.centerx = function() 
{
	var center_x = (this.x1 + this.x2) / 2;
	var center_y = (this.y1 + this.y2) / 2;
	return Math.round(center_x);
};

rectangle.prototype.centery = function() 
{
	var center_x = (this.x1 + this.x2) / 2;
	var center_y = (this.y1 + this.y2) / 2;
	return Math.round(center_y);
};
 
rectangle.prototype.intersect = function(otherrect)
{
	var otherrectone = otherrect;

	return (this.x1 <= otherrectone.x2 && this.x2 >= otherrectone.x1 && this.y1 <= otherrectone.y2 && this.y2 >= otherrectone.y1);	
};

function create_h_tunnel(x1, x2, y)
{
	var i = 0;
	var x1 = x1;
	var x2 = x2;
	var y = y;
	if (x1 > x2)
	{
		for (i = x2; i <= x1; i++)
		{
			allfloors[currentlevel].tiles[i][y].walkable = true;
			allfloors[currentlevel].tiles[i][y].blocked = false;
			allfloors[currentlevel].tiles[i][y].texture = "floor";
			allfloors[currentlevel].tiles[i][y].char = " ";
			allfloors[currentlevel].tiles[i][y].type = 2;
			allfloors[currentlevel].tiles[i][y].color = "#FFFFFF";
			allfloors[currentlevel].tiles[i][y].layer = 0;

			if (allfloors[currentlevel].tiles[i][y+1].walkable == false)
			{
				allfloors[currentlevel].tiles[i][y+1].char = "─";
				allfloors[currentlevel].tiles[i][y+1].type = 1;
				allfloors[currentlevel].tiles[i][y+1].texture = "hwall";
				allfloors[currentlevel].tiles[i][y+1].layer = 0;
			}

			if (allfloors[currentlevel].tiles[i][y-1].walkable == false)
			{
				allfloors[currentlevel].tiles[i][y-1].char = "─";
				allfloors[currentlevel].tiles[i][y-1].type = 1;
				allfloors[currentlevel].tiles[i][y-1].texture = "hwall";
				allfloors[currentlevel].tiles[i][y-1].layer = 0;				
			}
		}
	} else if (x1 < x2) {
		for (i = x1; i <= x2; i++)
		{
			allfloors[currentlevel].tiles[i][y].walkable = true;
			allfloors[currentlevel].tiles[i][y].blocked = false;
			allfloors[currentlevel].tiles[i][y].texture = "floor";
			allfloors[currentlevel].tiles[i][y].char = " ";
			allfloors[currentlevel].tiles[i][y].type = 2;
			allfloors[currentlevel].tiles[i][y].color = "#FFFFFF";
			allfloors[currentlevel].tiles[i][y].layer = 0;			

			if (allfloors[currentlevel].tiles[i][y+1].walkable == false)
			{
				allfloors[currentlevel].tiles[i][y+1].char = "─";
				allfloors[currentlevel].tiles[i][y+1].type = 1;
				allfloors[currentlevel].tiles[i][y+1].texture = "hwall";
				allfloors[currentlevel].tiles[i][y+1].layer = 0;
			}

			if (allfloors[currentlevel].tiles[i][y-1].walkable == false)
			{
				allfloors[currentlevel].tiles[i][y-1].char = "─";
				allfloors[currentlevel].tiles[i][y-1].type = 1;
				allfloors[currentlevel].tiles[i][y-1].texture = "hwall";
				allfloors[currentlevel].tiles[i][y-1].layer = 0;				
			}
		}
	}
}

function create_v_tunnel(y1, y2, x)
{
	var i = 0;
	var y1 = y1;
	var y2 = y2;
	var x = x;
	if (y1 > y2)
	{
		for (i = y2; i <= y1; i++)
		{
			allfloors[currentlevel].tiles[x][i].walkable = true;
			allfloors[currentlevel].tiles[x][i].blocked = false;
			allfloors[currentlevel].tiles[x][i].texture = "floor";
			allfloors[currentlevel].tiles[x][i].char = " ";
			allfloors[currentlevel].tiles[x][i].type = 2;
			allfloors[currentlevel].tiles[x][i].color = "#FFFFFF";
			allfloors[currentlevel].tiles[x][i].layer = 0;			

			if (allfloors[currentlevel].tiles[x+1][i].walkable == false)
			{
				allfloors[currentlevel].tiles[x+1][i].char = "│";
				allfloors[currentlevel].tiles[x+1][i].type = 1;
				allfloors[currentlevel].tiles[x+1][i].texture = "vwall";
				allfloors[currentlevel].tiles[x+1][i].layer = 0;				
			}

			if (allfloors[currentlevel].tiles[x-1][i].walkable == false)
			{
				allfloors[currentlevel].tiles[x-1][i].char = "│";
				allfloors[currentlevel].tiles[x-1][i].type = 1;
				allfloors[currentlevel].tiles[x-1][i].texture = "vwall";
				allfloors[currentlevel].tiles[x-1][i].layer = 0;
			}
		}
	} else if (y1 < y2) {
		for (i = y1; i <= y2; i++)
		{
			allfloors[currentlevel].tiles[x][i].walkable = true;
			allfloors[currentlevel].tiles[x][i].blocked = false;
			allfloors[currentlevel].tiles[x][i].texture = "floor";
			allfloors[currentlevel].tiles[x][i].char = " ";
			allfloors[currentlevel].tiles[x][i].type = 2;
			allfloors[currentlevel].tiles[x][i].color = "#FFFFFF";
			allfloors[currentlevel].tiles[x][i].layer = 0;			

			if (allfloors[currentlevel].tiles[x+1][i].walkable == false)
			{
				allfloors[currentlevel].tiles[x+1][i].char = "│";
				allfloors[currentlevel].tiles[x+1][i].type = 1;
				allfloors[currentlevel].tiles[x+1][i].texture = "vwall";
				allfloors[currentlevel].tiles[x+1][i].layer = 0;				
			}

			if (allfloors[currentlevel].tiles[x-1][i].walkable == false)
			{
				allfloors[currentlevel].tiles[x-1][i].char = "│";
				allfloors[currentlevel].tiles[x-1][i].type = 1;
				allfloors[currentlevel].tiles[x-1][i].texture = "vwall";
				allfloors[currentlevel].tiles[x-1][i].layer = 0;				
			}
		}
	}
}

function check_room(room)
{
	// This function checks to see if the room is open to the others.
	// It uses the AI Pathfinding
	// pathFind(x, y, x2, y2)
	// which returns a array filled with tile coords
	
	if (allfloors[currentlevel].rooms.length <= 1)
	{
		return true;
	}
	
	var path = pathFind(Math.round(room.centerx()), Math.round(room.centery()), Math.round(getobjectreference("player").x), Math.round(getobjectreference("player").y))

	var pathback = pathFind(Math.round(getobjectreference("player").x), Math.round(getobjectreference("player").y), Math.round(room.centerx()), Math.round(room.centery()))
	
	var info = "";	
	
	for (var x in path)
	{
		info = info + x + ": " + path[x] + "\n";
	}
	
	if (path.length > 0 && path.length > room.w && path.length > room.h && pathback.length > 0 && pathback.length > room.w && pathback.length > room.h)
	{	
		return true;
	} else {
		return false
	}
	return false;
}

function delete_room(room)
{
	// This function removes a room on the current floor
	
	var x = 0;
	var y = 0;

	y = room.y1;

	for (x = room.x1; x <= room.x2; x++)
	{
		allfloors[currentlevel].tiles[x][y] = new tile( { walkable: false, blocked: false, type: -1, texture: 'transparent', char: '', color: '#000000', x: x, y: y } );
	}

	y = room.y2;

	for (x = room.x1; x <= room.x2; x++)
	{
		allfloors[currentlevel].tiles[x][y] = new tile( { walkable: false, blocked: false, type: -1, texture: 'transparent', char: '', color: '#000000', x: x, y: y } );
	}

	x = room.x1;

	for (y = room.y1; y <= room.y2; y++)
	{
		allfloors[currentlevel].tiles[x][y] = new tile( { walkable: false, blocked: false, type: -1, texture: 'transparent', char: '', color: '#000000', x: x, y: y } );
	}

	x = room.x2;

	for (y = room.y1; y <= room.y2; y++)
	{
		allfloors[currentlevel].tiles[x][y] = new tile( { walkable: false, blocked: false, type: -1, texture: 'transparent', char: '', color: '#000000', x: x, y: y } );
	}

	for (x = room.x1 + 1; x < room.x2; x++)
	{
		allfloors[currentlevel].tiles[x][y] = new tile( { walkable: false, blocked: false, type: -1, texture: 'transparent', char: '', color: '#000000', x: x, y: y } );
	}	
}

function create_room(room)
{
	// This function creates a room on the current floor

	var x = 0;
	var y = 0;

	y = room.y1;

	for (x = room.x1; x <= room.x2; x++)
	{
		if (allfloors[currentlevel].tiles[x][y].walkable == false)
		{
			allfloors[currentlevel].tiles[x][y].walkable = false;
			allfloors[currentlevel].tiles[x][y].blocked = true;
			allfloors[currentlevel].tiles[x][y].texture = "hwall";
			allfloors[currentlevel].tiles[x][y].char = "─";	
			allfloors[currentlevel].tiles[x][y].type = 1;	
			allfloors[currentlevel].tiles[x][y].layer = 0;			
		}
	}

	y = room.y2;

	for (x = room.x1; x <= room.x2; x++)
	{
		if (allfloors[currentlevel].tiles[x][y].walkable == false)
		{
			allfloors[currentlevel].tiles[x][y].walkable = false;
			allfloors[currentlevel].tiles[x][y].blocked = true;
			allfloors[currentlevel].tiles[x][y].texture = "hwall";
			allfloors[currentlevel].tiles[x][y].char = "─";	
			allfloors[currentlevel].tiles[x][y].type = 1;
			allfloors[currentlevel].tiles[x][y].layer = 0;			
		}
	}

	x = room.x1;

	for (y = room.y1; y <= room.y2; y++)
	{
		if (allfloors[currentlevel].tiles[x][y].walkable == false)
		{
			allfloors[currentlevel].tiles[x][y].walkable = false;
			allfloors[currentlevel].tiles[x][y].blocked = true;
			allfloors[currentlevel].tiles[x][y].texture = "vwall";
			allfloors[currentlevel].tiles[x][y].char = "│";	
			allfloors[currentlevel].tiles[x][y].type = 1;		
			allfloors[currentlevel].tiles[x][y].layer = 0;			
		}
	}

	x = room.x2;

	for (y = room.y1; y <= room.y2; y++)
	{
		if (allfloors[currentlevel].tiles[x][y].walkable == false)
		{
			allfloors[currentlevel].tiles[x][y].walkable = false;
			allfloors[currentlevel].tiles[x][y].blocked = true;
			allfloors[currentlevel].tiles[x][y].texture = "vwall";
			allfloors[currentlevel].tiles[x][y].char = "│";	
			allfloors[currentlevel].tiles[x][y].type = 1;		
			allfloors[currentlevel].tiles[x][y].layer = 0;			
		}
	}

	for (x = room.x1 + 1; x < room.x2; x++)
	{
		for (y = room.y1 + 1; y < room.y2; y++)
		{
			allfloors[currentlevel].tiles[x][y].walkable = true;
			allfloors[currentlevel].tiles[x][y].block_sight = false;
			allfloors[currentlevel].tiles[x][y].texture = "floor";
			allfloors[currentlevel].tiles[x][y].type = 2;
			allfloors[currentlevel].tiles[x][y].color = "#FFFFFF";
			allfloors[currentlevel].tiles[x][y].layer = 0;			
		}
	}
	//log("Created a room at: " + room.x1 + ", " + room.y1);
}

function floor(level, size, roomsnumber, seednumber)
{
	this.seednumber = seednumber;
	
	Math.seedrandom(this.seednumber);
	
	this.level = level; 
	// levels start at 0/1, larger numbers = lower down in dungeon

	this.tiles = new Array(new Array());

	this.rooms = new Array();

	this.roomsnumber = roomsnumber;

	this.size = size;

	var x = 0;
	var y = 0;

	for (x = 0; x <= size; x++)
	{
		this.tiles[x] = [];
		for (y = 0; y <= size; y++)
		{
			this.tiles[x][y] = new tile( { walkable: false, blocked: false, type: -1, texture: 'transparent', char: '', color: '#000000', x: x, y: y, layer: 0 } );
		}	
	}

	allfloors[level] = this;

	// create rooms...

	var r = 0;
	var numrooms = 0;

	for (r = 0; r <= roomsnumber; r++)
	{
		var roomwidth = Math.round(Math.random() * (ROOM_MAX_SIZE-ROOM_MIN_SIZE)+ROOM_MIN_SIZE);
		var roomheight = Math.round(Math.random() * (ROOM_MAX_SIZE-ROOM_MIN_SIZE)+ROOM_MIN_SIZE);
		var roomx = Math.round(Math.random() * 0.9 * (this.size - roomwidth - 1) );
		var roomy = Math.round(Math.random() * 0.9 * (this.size - roomheight - 1) );

		while ((roomx - (roomwidth) <= 0) || (roomy - (roomheight) <= 0) || (roomx + (roomwidth) >= size) || (roomy + (roomheight) >= size))
		{
			roomx = Math.round(Math.random() * 0.9 * (this.size - roomwidth - 1) );
			roomy = Math.round(Math.random() * 0.9 * (this.size - roomheight - 1) );
		}

		var new_room = new rectangle(roomx, roomy, roomwidth, roomheight);

		var boolfailed = false;

		var x = 0;

		for (x = 0; x < this.rooms.length; x++)
		{
			if (this.rooms[x])
			{
				if (new_room.intersect(this.rooms[x]))
				{
					boolfailed = true;
					break;
				}
			}
		}

		if (boolfailed == false)
		{ 
			create_room(new_room); 		
		
           	//center coordinates of new room, will be useful later
			var new_x = Math.round(new_room.centerx());
			var new_y = Math.round(new_room.centery());
 
			if (numrooms == 0)
			{
				// this is the first room, where the player starts at

				getobjectreference("player").x = Math.round(new_x);
				getobjectreference("player").y = Math.round(new_y);
				getobjectreference("player").layer = 1;
			} else {
				var prev_x = Math.round(this.rooms[numrooms-1].centerx());
				var prev_y = Math.round(this.rooms[numrooms-1].centery());

				if (Math.random() > 0.5)
				{
					create_h_tunnel(prev_x, new_x, prev_y);
					create_v_tunnel(prev_y, new_y, new_x);
				} else {
					create_v_tunnel(prev_x, new_x, prev_y);
					create_h_tunnel(prev_y, new_y, new_x);
				}	

				if (check_room(new_room) == false)
				{
					if (Math.random() > 0.5)
					{
						create_h_tunnel(new_x, prev_x, prev_y);
						create_v_tunnel(new_y, prev_y, new_x);
					} else {
						create_v_tunnel(new_x, prev_x, prev_y);
						create_h_tunnel(new_y, prev_y, new_x);
					}
				}
			}

			if (check_room(new_room) == false)
			{
				delete_room(new_room);
				continue;
			}
			
			new_room.roomnumber = numrooms;
			this.rooms.push(new_room);
			numrooms += 1;
		} else {
			//delete_room(new_room);
			continue;
		}
	}
	
	// clean up...
	for (x = 0; x <= size; x++)
	{
		for (y = 0; y <= size; y++)
		{
			if (this.tiles[x][y].walkable == true)
			{
				this.tiles[x][y].char = " ";
				this.tiles[x][y].texture = "floor";
				this.tiles[x][y].layer = 0;
			}
		}	
	}
	
	for (var roomx = 0; roomx < this.rooms.length; roomx++)
	{
		makePath(this.rooms[roomx].centerx(), this.rooms[roomx].centery(), getobjectreference("player").x, getobjectreference("player").y);
	}
	
	this.addwalls();	
	this.deleteunusedtiles();	
	
	while (this.tiles[getobjectreference("player").x][getobjectreference("player").y] == undefined || this.tiles[getobjectreference("player").x][getobjectreference("player").y].walkable == false || this.tiles[getobjectreference("player").x][getobjectreference("player").y].type == -1)
	{
		var randroom = Math.round(Math.random() * (this.rooms.length-1));

		getobjectreference("player").x = Math.round(this.rooms[randroom].centerx());
		getobjectreference("player").y = Math.round(this.rooms[randroom].centery());
	}	
	
	allfloors[level] = this;
}

floor.prototype.deleteunusedtiles = function()
{
	var totaltiles = 0;
	var deletedtiles = 0;
	for (var x = 0; x < this.size; x++)
	{
		for (var y = 0; y < this.size; y++)
		{
			totaltiles = totaltiles + 1;
			if (this.tiles[x][y].type == -1 && x != getobjectreference("player").x && y != getobjectreference("player").y)
			{
				// delete!
				this.tiles[x][y] = undefined;
				deletedtiles = deletedtiles + 1;
				totaltiles = totaltiles - 1;
			}
		}	
	}
	return totaltiles + " (" + deletedtiles + ")";
}

floor.prototype.addwalls = function()
{
	for (var x = 0; x < this.size; x++)
	{
		for (var y = 0; y < this.size; y++)
		{
			if (this.tiles[x][y].walkable == true)
			{
				if (this.tiles[x][y+1].walkable == false && this.tiles[x][y+1].type != 1)
				{
					allfloors[currentlevel].tiles[x][y+1].walkable = false;
					allfloors[currentlevel].tiles[x][y+1].blocked = true;
					allfloors[currentlevel].tiles[x][y+1].texture = "hwall";
					allfloors[currentlevel].tiles[x][y+1].char = "─";	
					allfloors[currentlevel].tiles[x][y+1].type = 1;					
					allfloors[currentlevel].tiles[x][y+1].layer = 0;					
				}			
				if (this.tiles[x][y-1].walkable == false && this.tiles[x][y-1].type != 1)
				{
					allfloors[currentlevel].tiles[x][y-1].walkable = false;
					allfloors[currentlevel].tiles[x][y-1].blocked = true;
					allfloors[currentlevel].tiles[x][y-1].texture = "hwall";
					allfloors[currentlevel].tiles[x][y-1].char = "─";	
					allfloors[currentlevel].tiles[x][y-1].type = 1;					
					allfloors[currentlevel].tiles[x][y-1].layer = 0;					
				}							
				if (this.tiles[x-1][y].walkable == false && this.tiles[x-1][y].type != 1)
				{
					allfloors[currentlevel].tiles[x-1][y].walkable = false;
					allfloors[currentlevel].tiles[x-1][y].blocked = true;
					allfloors[currentlevel].tiles[x-1][y].texture = "hwall";
					allfloors[currentlevel].tiles[x-1][y].char = "─";	
					allfloors[currentlevel].tiles[x-1][y].type = 1;		
					allfloors[currentlevel].tiles[x-1][y].layer = 0;
				}
				if (this.tiles[x+1][y].walkable == false && this.tiles[x+1][y].type != 1)
				{
					allfloors[currentlevel].tiles[x+1][y].walkable = false;
					allfloors[currentlevel].tiles[x+1][y].blocked = true;
					allfloors[currentlevel].tiles[x+1][y].texture = "hwall";
					allfloors[currentlevel].tiles[x+1][y].char = "─";	
					allfloors[currentlevel].tiles[x+1][y].type = 1;					
					allfloors[currentlevel].tiles[x+1][y].layer = 0;					
				}				
				if (this.tiles[x-1][y-1].walkable == false && this.tiles[x-1][y-1].type != 1)
				{
					allfloors[currentlevel].tiles[x-1][y-1].walkable = false;
					allfloors[currentlevel].tiles[x-1][y-1].blocked = true;
					allfloors[currentlevel].tiles[x-1][y-1].texture = "hwall";
					allfloors[currentlevel].tiles[x-1][y-1].char = "─";	
					allfloors[currentlevel].tiles[x-1][y-1].type = 1;					
					allfloors[currentlevel].tiles[x-1][y-1].layer = 0;					
				}								
				if (this.tiles[x+1][y-1].walkable == false && this.tiles[x+1][y-1].type != 1)
				{
					allfloors[currentlevel].tiles[x+1][y-1].walkable = false;
					allfloors[currentlevel].tiles[x+1][y-1].blocked = true;
					allfloors[currentlevel].tiles[x+1][y-1].texture = "hwall";
					allfloors[currentlevel].tiles[x+1][y-1].char = "─";	
					allfloors[currentlevel].tiles[x+1][y-1].type = 1;					
					allfloors[currentlevel].tiles[x+1][y-1].layer = 0;					
				}	
				if (this.tiles[x-1][y+1].walkable == false && this.tiles[x-1][y+1].type != 1)
				{
					allfloors[currentlevel].tiles[x-1][y+1].walkable = false;
					allfloors[currentlevel].tiles[x-1][y+1].blocked = true;
					allfloors[currentlevel].tiles[x-1][y+1].texture = "hwall";
					allfloors[currentlevel].tiles[x-1][y+1].char = "─";	
					allfloors[currentlevel].tiles[x-1][y+1].type = 1;					
					allfloors[currentlevel].tiles[x-1][y+1].layer = 0;					
				}				
				if (this.tiles[x+1][y+1].walkable == false && this.tiles[x+1][y+1].type != 1)
				{
					allfloors[currentlevel].tiles[x+1][y+1].walkable = false;
					allfloors[currentlevel].tiles[x+1][y+1].blocked = true;
					allfloors[currentlevel].tiles[x+1][y+1].texture = "hwall";
					allfloors[currentlevel].tiles[x+1][y+1].char = "─";	
					allfloors[currentlevel].tiles[x+1][y+1].type = 1;					
					allfloors[currentlevel].tiles[x+1][y+1].layer = 0;					
				}																				
			}
		}	
	}	
}

floor.prototype.godownlevel = function()
{
	if (getobjectreference("player") == false || getobjectreference("player") == undefined)
	{
		return;
	}

	if (allfloors[currentlevel+1] == undefined)
	{
		var lastlevel = currentlevel;
	
		currentlevel = currentlevel + 1;
		var floorseed = (parseInt(allfloors[currentlevel-1].seednumber) + parseInt(currentlevel));	
		var newfloor = new floor(currentlevel, allfloors[currentlevel-1].size, allfloors[currentlevel-1].roomsnumber, floorseed);
		getobjectreference("player").level = currentlevel;

		// and add stuff to the floor.
		populatecurrentfloor(floorseed);		
		
		var upstairs = findrandomobjonfloor({floornumber: currentlevel, referenceid: getobjectreference("stairsup").referenceid});
		
		if (upstairs != false)
		{
			getobjectreference("player").x = upstairs.x;
			getobjectreference("player").y = upstairs.y;			
		} else {
			while (allfloors[currentlevel].tiles[getobjectreference("player").x][getobjectreference("player").y].walkable == false)
			{
				getobjectreference("player").x = allfloors[currentlevel].rooms[0].x1 + Math.round(allfloors[currentlevel].rooms[0].w * Math.random());
				getobjectreference("player").y = allfloors[currentlevel].rooms[0].y1 + Math.round(allfloors[currentlevel].rooms[0].h * Math.random());

				getobjectreference("player").x = Math.round(getobjectreference("player").x);
				getobjectreference("player").y = Math.round(getobjectreference("player").y);
			}
		}
		discover_tiles(getobjectreference("player").x, getobjectreference("player").y, 5);
		
		for(property in allfloors[lastlevel])
		{
			delete allfloors[lastlevel][property];
		}
		allfloors[lastlevel] = null;
	} else if (allfloors[currentlevel+1] != undefined) {
		var lastlevel = currentlevel;
	
		currentlevel = currentlevel + 1;
		getobjectreference("player").level = currentlevel;

		var upstairs = findrandomobjonfloor({floornumber: currentlevel, referenceid: getobjectreference("stairsup").referenceid});
		
		if (upstairs != false)
		{
			getobjectreference("player").x = upstairs.x;
			getobjectreference("player").y = upstairs.y;			
		} else {
			while (allfloors[currentlevel].tiles[getobjectreference("player").x][getobjectreference("player").y].walkable == false)
			{
				getobjectreference("player").x = allfloors[currentlevel].rooms[0].x1 + Math.round(allfloors[currentlevel].rooms[0].w * Math.random());
				getobjectreference("player").y = allfloors[currentlevel].rooms[0].y1 + Math.round(allfloors[currentlevel].rooms[0].h * Math.random());

				getobjectreference("player").x = Math.round(getobjectreference("player").x);
				getobjectreference("player").y = Math.round(getobjectreference("player").y);
			}
		}
		discover_tiles(getobjectreference("player").x, getobjectreference("player").y, 5);
		
		for(property in allfloors[lastlevel])
		{
			delete allfloors[lastlevel][property];
		}
		allfloors[lastlevel] = null;
	}
	//renderall({handleactors: false});
};

floor.prototype.gouplevel = function()
{
	if (getobjectreference("player") == false || getobjectreference("player") == undefined)
	{
		return;
	}

	if (allfloors[currentlevel-1] == undefined)
	{
		var lastlevel = currentlevel;
		
		currentlevel = currentlevel - 1;
		
		if (currentlevel < 0)
		{
			currentlevel = 0;
		}
		
		var floorseed = (parseInt(allfloors[currentlevel+1].seednumber) - parseInt(currentlevel+1));
		var newfloor = new floor((currentlevel), allfloors[currentlevel+1].size, allfloors[currentlevel+1].roomsnumber, floorseed);
		getobjectreference("player").level = currentlevel;

		// and add stuff to the floor.
		populatecurrentfloor(floorseed);		
		
		var downstairs = findrandomobjonfloor({floornumber: currentlevel, referenceid: getobjectreference("stairsdown").referenceid});
		
		if (downstairs != false)
		{
			getobjectreference("player").x = downstairs.x;
			getobjectreference("player").y = downstairs.y;			
		} else {
			while (allfloors[currentlevel].tiles[getobjectreference("player").x][getobjectreference("player").y].walkable == false)
			{
				getobjectreference("player").x = allfloors[currentlevel].rooms[0].x1 + Math.round(allfloors[currentlevel].rooms[0].w * Math.random());
				getobjectreference("player").y = allfloors[currentlevel].rooms[0].y1 + Math.round(allfloors[currentlevel].rooms[0].h * Math.random());

				getobjectreference("player").x = Math.round(getobjectreference("player").x);
				getobjectreference("player").y = Math.round(getobjectreference("player").y);
			}
		}
		discover_tiles(getobjectreference("player").x, getobjectreference("player").y, 5);		
		
		for(property in allfloors[lastlevel])
		{
			delete allfloors[lastlevel][property];
		}
		allfloors[lastlevel] = null;
	} else if (currentlevel - 1 >= 0 && allfloors[currentlevel-1] != undefined) {
		var lastlevel = currentlevel;

		currentlevel = currentlevel - 1;
		
		if (currentlevel < 0)
		{
			currentlevel = 0;
		}		
		
		getobjectreference("player").level = currentlevel;

		var downstairs = findrandomobjonfloor({floornumber: currentlevel, referenceid: getobjectreference("stairsdown").referenceid});
		
		if (downstairs != false)
		{
			getobjectreference("player").x = downstairs.x;
			getobjectreference("player").y = downstairs.y;			
		} else {
			while (allfloors[currentlevel].tiles[getobjectreference("player").x][getobjectreference("player").y].walkable == false)
			{
				getobjectreference("player").x = allfloors[currentlevel].rooms[0].x1 + Math.round(allfloors[currentlevel].rooms[0].w * Math.random());
				getobjectreference("player").y = allfloors[currentlevel].rooms[0].y1 + Math.round(allfloors[currentlevel].rooms[0].h * Math.random());

				getobjectreference("player").x = Math.round(getobjectreference("player").x);
				getobjectreference("player").y = Math.round(getobjectreference("player").y);
			}
		}
		discover_tiles(getobjectreference("player").x, getobjectreference("player").y, 5);		
		
		for(property in allfloors[lastlevel])
		{
			delete allfloors[lastlevel][property];
		}
		allfloors[lastlevel] = null;
	}
	//renderall({handleactors: false});
};