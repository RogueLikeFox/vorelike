/*

Vorelike Copyright (c) 2013-2014 RogueLikeFox

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

// Simple JavaScript file for creating monsters for VDOS

/*

Example:

char: w;
	color: #670000;
	texture: wolftex;
	name: Wolf;
	gender: Male;
	race: wolf;
	isactor: true;
	isbaseobject: true;
	referenceid: mwolf;
	minfloor: 0;
	maxfloor: 999;
	hp: 5;
	totalhp: 5;
	backpack: [];
	status: Normal;
	actortype: Predator;
	aggrotype: 1;
	sightdistance: 5;
	strength: 1; 
	dexterity: 1;
	constitution: 1;
	intelligence: 1;
	wisdom: 1;
	charisma: 1;
	totalpleasure: 5;
	pleasure: 0;
	talkscenes: mwolftext

addattack: true;
	name: Wolf;
	gender: Male;
	actors: mwolf;
	
	// Arrays (lists) can be either be one item per line,
	// a bunch of items seperated by a pipe ("|"), or
	// a combination of the two.  Blank lines are
	// ignored.
	successtext: [
		The male wolf attacks you for DMG damage.
		The male wolf claws at you for DMG damage.
	];

	failtext: The male wolf's attack does no damage to you.;
	finishtext: The wolf's claws tear into your skin and you fall down, unable to fight. Your vision darkens as the wolf advances on you.

addswallow: true;
	referenceid: mwolforalvore1;
	name: Oral Vore;
	isstart: true;
	actors: mwolf;
	gender: Male;
	sounds: swallow;
	nextstage: mwolforalvore2;
	nextstagepercent: 100;
	movetonextstageimd: true;
	doesswallow: true;
	targetcanmove: false;
	actorcanmove: false;
	status: In stomach;
	successtext: [
		The male wolf lunges forward, jaws open wide, and with surprising quickness gulps you down into his stomach!
		The wolf gobbles you up one quick swallow.
	];
	failtext: [
		The wolf tries to swallow you whole, but you manage to avoid the attack.
		The wolf's jaws clamp down inches in front of your face and you barely avoid becoming his meal.
		];
	rubtext: ;
	nextstagetext: You feel yourself sliding down into the wolf's stomach.;
	prevstagetext: You feel the male wolf spit you out of his mouth.

addtextscene: true;
	referenceid: mwolftext;
	scenes: [The wolf looks at you expectantly. "Hello there, little morsel."
		mwolfchoice]

addchoice: true;
	referenceid: mwolfchoice;
	choices: [Oral Vore
		Digest
		Switch places];
	outcomes: [swallow(mwolforalvore1)
		digest()
		switchplaces()]

*/

// global variables:

var charvars;
var attackvars;
var swallowvars;
var textscenevars;
var choicevars;
var textureinfovars;

var finaloutput = document.getElementById("finaloutputdiv");

var curstage = 0;

charvars = new Array("char", "color", "texture", "name", "gender", "race", "isactor", "isbaseobject", "referenceid", "minfloor", "maxfloor", "hp", "totalhp", "backpack", "status", "actortype",
	"aggrotype", "sightdistance", "strength", "dexterity", "constitution", "intelligence", "wisdom", "charisma", "totalpleasure", "pleasure", "talkscenes");
	
var defaultcharvars = new Array("", "", "", "", "", "", "true", "true", "", "0", "", "", "", "", "Normal", "", "", "", "", "", "", "", "", "", "", "", "");
	
attackvars = new Array("name", "gender", "actors", "successtext", "failtext", "rubtext", "finishtext");

var defaultattackvars;

swallowvars = new Array("referenceid", "name", "isstart", "actors", "gender", "sounds", "nextstage", "nextstagepercent", "movetonextstageimd", "doesswallow", "targetcanmove", "actorcanmove", "status",
	"successtext", "failtext", "rubtext", "nextstagetext", "prevstagetext");
	
var defaultswallowvars;
	
textscenevars = new Array("referenceid", "scenes");

choicevars = new Array("referenceid", "choices", "outcomes" );

textureinfovars = new Array("referenceid", "isanimation", "animtype", "scale", "texturefiles", "spritesheetpoints");

var defaulttextureinfovars;

/** Function count the occurrences of substring in a string;
 * @param {String} string   Required. The string;
 * @param {String} subString    Required. The string to search for;
 * @param {Boolean} allowOverlapping    Optional. Default: false;
 */
function occurrences(string, subString, allowOverlapping)
{
    string+=""; subString+="";
    if (subString.length<=0) return string.length+1;

    var n=0, pos=0;
    var step = (allowOverlapping)?(1):(subString.length);

    while(true)
	{
        pos=string.indexOf(subString,pos);
        if(pos>=0) 
		{ 
			n++; pos+=step; 
		} else { break; }
    }
    return(n);
}

function create_form(stage)
{
	var output = "<form METHOD=POST><table>";
	if (stage == 0 || stage == undefined)
	{
		output = output + "<br>Basic Info<br>";
		for(var i in charvars)
		{
			output = output + "<tr><td>" + charvars[i] + ": </td><td>" + "<input type='text' name='" + charvars[i] + "' value='" + defaultcharvars[i] + "'></td></tr>";
		}
	}
	if (stage == 1)
	{
		output = output + "<br>Attack Variables<br>";	
		output = output + "<input type='hidden' name='addattack' value='true'>";
		for(var i in attackvars)
		{
			if (occurrences(attackvars[i], "text", false) >= 1)
			{
				output = output + "<tr><td>" + attackvars[i] + ": </td><td>" + "<textarea name='" + attackvars[i] + "'></textarea></td></tr>";						
			} else {
				output = output + "<tr><td>" + attackvars[i] + ": </td><td>" + "<input type='text' name='" + attackvars[i] + "' value='" + defaultattackvars[i] + "'></td></tr>";
			}
		}		
	}
	if (stage == 2)
	{
		output = output + "<br>Swallow Variables<br>";		
		output = output + "<input type='hidden' name='addswallow' value='true'>";
		for(var i in swallowvars)
		{
			if (occurrences(swallowvars[i], "text", false) >= 1)
			{
				output = output + "<tr><td>" + swallowvars[i] + ": </td><td>" + "<textarea name='" + swallowvars[i] + "'></textarea></td></tr>";			
			} else {
				output = output + "<tr><td>" + swallowvars[i] + ": </td><td>" + "<input type='text' name='" + swallowvars[i] + "' value='" + defaultswallowvars[i] + "'></td></tr>";
			}
		}		
	}
	if (stage == 3)
	{
		output = output + "<br>Text Scene Variables<br>";			
		output = output + "<input type='hidden' name='addtextscene' value='true'>";		
		for(var i in textscenevars)
		{
			if (occurrences(textscenevars[i], "text", false) >= 1)
			{
				output = output + "<tr><td>" + textscenevars[i] + ": </td><td>" + "<textarea name='" + textscenevars[i] + "'></textarea></td></tr>";						
			} else {
				output = output + "<tr><td>" + textscenevars[i] + ": </td><td>" + "<input type='text' name='" + textscenevars[i] + "'></td></tr>";
			}
		}		
	}
	if (stage == 4)
	{
		output = output + "<br>Choicebox Variables<br>";				
		output = output + "<input type='hidden' name='addchoice' value='true'>";				
		for(var i in choicevars)
		{
			output = output + "<tr><td>" + choicevars[i] + ": </td><td>" + "<input type='text' name='" + choicevars[i] + "'></td></tr>";
		}		
	}
	if (stage == 5)
	{
		// texture files
		output = output + "<br>Texture / Sprite Variables<br>";						
		output = output + "<input type='hidden' name='addtexture' value='true'>";
		for(var i in textureinfovars)
		{
			if (textureinfovars[i] == "texturefiles")
			{
				output = output + "<tr><td>" + textureinfovars[i] + ": </td><td>" + "<input type='file' name='" + textureinfovars[i] + "' multiple></td></tr>";				
			} else {
				output = output + "<tr><td>" + textureinfovars[i] + ": </td><td>" + "<input type='text' name='" + textureinfovars[i] + "' value='" + defaulttextureinfovars[i] + "'></td></tr>";
			}
		}	
	}
	if (stage >= 6)
	{
		return false;
	}
	output = output + "</table>";
	if (stage >= 2 && stage < 4)
	{
		output = output + "<input type='Submit' name='Submit' value='Add Another' onclick=make_new(this.form);>";
	}
	output = output + "<input type='Submit' name='Submit' value='Next' onclick=next_input(this.form);></form>";
	return output;
}

function make_new(input)
{
	for (var i = 0; i < input.elements.length; i++)
	{
		if (input.elements[i].name == "Submit" || input.elements[i].value == undefined)
		{
			continue;
		}
		if (i == 0)
		{
			document.getElementById("finaloutputdiv").innerHTML = document.getElementById("finaloutputdiv").innerHTML + input.elements[i].name + ": " + input.elements[i].value + ";\n";
		} else {
			if (input.elements[i].value.indexOf("\n") > -1 || input.elements[i].type == "textarea")
			{
				document.getElementById("finaloutputdiv").innerHTML = document.getElementById("finaloutputdiv").innerHTML + "\t" + input.elements[i].name + ": [ " + input.elements[i].value.replace("\\n", "\\n\\t") + " ];\n";							
			} else {
				document.getElementById("finaloutputdiv").innerHTML = document.getElementById("finaloutputdiv").innerHTML + "\t" + input.elements[i].name + ": " + input.elements[i].value + ";\n";			
			}
		}
	}
	document.getElementById("finaloutputdiv").innerHTML = document.getElementById("finaloutputdiv").innerHTML + "\n";
	
	document.getElementById('monsterform').innerHTML = create_form(curstage);
}

function next_input(input)
{
	var encodefile = false;

	if (input != undefined)
	{
		for (var i = 0; i < input.elements.length; i++)
		{
			if (input.elements[i].name == "Submit" || input.elements[i].value == undefined)
			{
				continue;
			}
			if (i == 0)
			{
				document.getElementById("finaloutputdiv").innerHTML = document.getElementById("finaloutputdiv").innerHTML + input.elements[i].name + ": " + input.elements[i].value + ";\n";
			} else {
				if (input.elements[i].type == "file")
				{
					encodefile = true;
					for (var j = 0; j < input.elements[i].files.length; j++)
					{
						var reader = new FileReader();	
						
						reader.onloadend = (function(name) 
						{
							return function(e) 
							{
								if (reader.readyState == FileReader.DONE) 
								{
									document.getElementById("finaloutputdiv").innerHTML = document.getElementById("finaloutputdiv").innerHTML + "\t" + name + ": [ " + e.target.result.replace(/\n/g, '') + " ];\n\n";
								}
							};
						})(input.elements[i].name);
						reader.readAsDataURL(input.elements[i].files[j]);
					}
				} else {
					if (input.elements[i].value.indexOf("\n") > -1 || input.elements[i].type == "textarea")
					{
						document.getElementById("finaloutputdiv").innerHTML = document.getElementById("finaloutputdiv").innerHTML + "\t" + input.elements[i].name + ": [ " + input.elements[i].value.replace("\n", "\n\t") + " ];\n";							
					} else {
						document.getElementById("finaloutputdiv").innerHTML = document.getElementById("finaloutputdiv").innerHTML + "\t" + input.elements[i].name + ": " + input.elements[i].value + ";\n";			
					}
				}
			}
		}
		if (encodefile == false)
		{
			document.getElementById("finaloutputdiv").innerHTML = document.getElementById("finaloutputdiv").innerHTML + "\n";		
		}
	}
	
	if (curstage == 0)
	{
		defaultattackvars = new Array(input.elements[3].value, input.elements[4].value, input.elements[8].value, "successtext", "failtext", "rubtext", "finishtext");		
		defaultswallowvars = new Array("", "", "", input.elements[8].value, input.elements[4].value, "", "", "", "", "", "", "", "", "", "", "", "", "");		
		defaulttextureinfovars = new Array(input.elements[2].value, "", "", "", "", "");
	}
	
	if (create_form(curstage + 1) != false)
	{
		curstage = curstage + 1;
		document.getElementById('monsterform').innerHTML = create_form(curstage);
	} else {
		document.getElementById("finaloutputdiv").style.display = "block";
		document.getElementById('monsterform').innerHTML = "And you are done!<br><br>Output:<br>";
	}
}

var readyStateCheckInterval = setInterval(function() {
    if (document.readyState === "complete") {
		document.getElementById("monsterform").innerHTML = create_form(curstage);
        clearInterval(readyStateCheckInterval);
    }
}, 10);